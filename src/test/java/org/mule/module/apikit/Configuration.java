package org.mule.module.apikit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.construct.Flow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration
extends AbstractConfiguration {
    public static final String DEFAULT_CONSOLE_PATH = "console";
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    private boolean consoleEnabled = true;
    private String consolePath = "console";
    private List<FlowMapping> flowMappings = new ArrayList<FlowMapping>();

    public boolean isConsoleEnabled() {
        return this.consoleEnabled;
    }

    public void setConsoleEnabled(boolean consoleEnabled) {
        this.consoleEnabled = consoleEnabled;
    }

    public String getConsolePath() {
        return this.consolePath;
    }

    public void setConsolePath(String consolePath) {
        this.consolePath = consolePath;
    }

    public List<FlowMapping> getFlowMappings() {
        return this.flowMappings;
    }

    public void setFlowMappings(List<FlowMapping> flowMappings) {
        this.flowMappings = flowMappings;
    }

    @Override
    public void initialise() throws InitialisationException {
        log.warn("initialize");
    }

    @Override
    protected void initializeRestFlowMap() {
        log.warn("initializeRestFlowMap");
    }

    @Override
    protected HttpRestRequest getHttpRestRequest(MuleEvent var1) {
        log.warn("getHttpRestRequest");
        return null;
    }

    @Override
    public Set<String> getFlowActionRefs(Flow flow) {
        return new HashSet<String>();
    }

    public Map<String, Flow> getRawRestFlowMap() {
        return new HashMap<String, Flow>();
    }

    @Override
    protected FlowResolver getFlowResolver(AbstractConfiguration abstractConfiguration, String key) {
        return new RouterFlowResolver((Configuration)abstractConfiguration, key);
    }

    @Override
    protected Map<String, FlowResolver> populateFlowMapWrapper() {
        return new HashMap<String, FlowResolver>();
    }

    @Override
    public void start() throws MuleException {
        log.warn("start");
    }

    private static class RouterFlowResolver
    implements FlowResolver {
        RouterFlowResolver(Configuration configuration, String key) {
            // 
        }

        @Override
        public Flow getFlow() {
            System.err.println("start");
            return null;
        }
    }
}