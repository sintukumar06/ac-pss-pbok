### Host

Several environments are available for use.  The API's are available at

|Env|Host|
|---|----|
|INT| https://pss-int.api.aircanada.com/utilities/... |
|UAT| https://pss-uat.api.aircanada.com/utilities/... |
|STG| https://pss-stg.api.aircanada.com/utilities/... |

*NOTE*: DNS and load balancers are currently being configured.  The above are the long term hosts.

Some of the old hosts are still available if the above hosts cannot be reached.  However, they will be disabled soon after completion of the DNS and load balancers.

### URL Path
The remainder of the URL path must contain a version number followed by the remainder of the path stated in this document.

For example, using *v1* of the API, a call to INT looks like
GET https://pss-int.api.aircanada.com/utilities/v1/localdatetime/YYZ

For example, using *v1* of the API, a call to UAT looks like
POST https://pss-uat.api.aircanada.com/flights/v1/availability/get


### GZIP Request
The HTTP the header *Content-Encoding* must be set to *gzip* when sending a request with a GZip compressed payload.

A GZip request body may improve performance if the request payload is large.

### GZIP Response
Set HTTP the header *Accept-Encoding* to *gzip* on a request to receive a GZip compressed payload as the response.

A GZip response body may improve performance if the response payload is large.