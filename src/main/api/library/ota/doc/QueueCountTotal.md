# Altea error  codes for Queue Count Total
|Error number|Error message|
| ------- | ------------ |
|1|Invalid date|
|360|Invalid PNR file address|
|723|Invalid category|
|727|Invalid amount|
|79A|Invalid office identification|
|79B|Already working another queue|
|79C|Not allowed to access queues for specified office identification|
|79D|Queue identifier has not been assigned for specified office identification|
|79E|Attempting to perform a queue function when not associated with a queue|
|79F|Queue placement or add new queue item is not allowed for the specified office identification and queue identifier|
|911|Unable to process - system error|
|912|Incomplete message - data missing in query|
|913|Item/data not found or data not existing in processing host|
|914|Invalid format/data - data does not match EDIFACT rules|
|915|No action - processing host cannot support function|
|916|EDIFACT version not supported|
|917|EDIFACT message size exceeded|
|918|enter message in remarks|
|919|no PNR in AAA|
|91A|inactive queue bank|
|91B|nickname not found|
|91C|invalid record locator|
|91D|invalid format|
|91F|invalid queue number|
|920|queue/date range empty|
|921|target not specified|
|922|targetted queue has wrong queue type|
|923|invalid time|
|924|invalid date range|
|925|queue number not specified|
|926|queue category empty|
|927|no items exist|
|928|queue category not assigned|
|929|No more items|
|92A|queue category full|

If an errorCategory element is included in the error response, the possible codes returned are the following:

|Value|Description|
| ------- | ------------ |
|EC|Error codes|
|WEC|Warning code|
|WZZ|Mutually defined warning|
|ZZZ|Mutually defined|