%dw 1.0
%output application/java

%var pnrReply = payload.PNR_Reply
%var handlePnrActions = ["Commit","EndTransact"] //PNR actions 

%function removeWhitespaceFromValues( e )
  e match {
    :array  -> $ map removeWhitespaceFromValues( $ ),
    :object -> $ mapObject { ( $$ ): removeWhitespaceFromValues( $ ) },
    default -> trim $ when $ is :string otherwise $
  } 

%function checkOrgDestErrorCategory (pnrReply)
flatten (pnrReply.originDestinationDetails map (orgDest) -> (
    (orgDest.itineraryInfo map (itineraryInfo) -> {
      (status: "400") when itineraryInfo.errorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
      (code: itineraryInfo.errorInfo.errorOrWarningCodeDetails.errorDetails.errorCode) when itineraryInfo.errorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
      (message: removeWhitespaceFromValues(itineraryInfo.errorInfo.errorWarningDescription.freeText) joinBy ', ') when itineraryInfo.errorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC'
    }) when orgDest.itineraryInfo..errorInfo? otherwise {}
)) when pnrReply.originDestinationDetails? otherwise {}

%function checkGeneralErrorInfoErrorCategoryIsEC (pnrReply, action)  
(pnrReply.generalErrorInfo map (generalErrorInfo) -> {
	(status: "400") when generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC' and (handlePnrActions contains action),
    (code: generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCode) when generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
    (message: removeWhitespaceFromValues(generalErrorInfo.errorWarningDescription.freeText) joinBy ', ') when generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC'
}) when pnrReply.generalErrorInfo? otherwise {}

%function checkGeneralErrorInfoErrorCategory (pnrReply)  
(pnrReply.generalErrorInfo map (generalErrorInfo) -> {
    (code: generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCode) when generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'WEC' or 
                                              generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'WA' or 
                                              generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'INF',
    (message: removeWhitespaceFromValues(generalErrorInfo.errorWarningDescription.freeText) joinBy ', ') when generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'WEC' or 
                                         generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'WA' or 
                                           generalErrorInfo.errorOrWarningCodeDetails.errorDetails.errorCategory == 'INF'
}) when pnrReply.generalErrorInfo? otherwise {} 

%function checkTravellerInfoErrorCategory (pnrReply)     
(pnrReply.travellerInfo map (travellerInfo) -> {
    (status: "400") when travellerInfo.nameError.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
    (code: travellerInfo.nameError.errorOrWarningCodeDetails.errorDetails.errorCode) when travellerInfo.nameError.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
    (message: removeWhitespaceFromValues(travellerInfo.nameError.errorWarningDescription.freeText) joinBy ', ') when travellerInfo.nameError.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC'
} when pnrReply.travellerInfo..nameError? otherwise {}) filter ($ != {})

%function checkDataElementsMasterErrorCategory (pnrReply)    
(pnrReply.dataElementsMaster.dataElementsIndiv map (dataElementsIndiv) -> {
    (code: dataElementsIndiv.elementErrorInformation.errorOrWarningCodeDetails.errorDetails.errorCode) when dataElementsIndiv.elementErrorInformation.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC',
    (message: removeWhitespaceFromValues(dataElementsIndiv.elementErrorInformation.errorWarningDescription.freeText) joinBy ', ') when dataElementsIndiv.elementErrorInformation.errorOrWarningCodeDetails.errorDetails.errorCategory == 'EC'
}) when pnrReply.dataElementsMaster.dataElementsIndiv..elementErrorInformation? otherwise {}

          
---
{
  	errorDetails: (checkOrgDestErrorCategory(pnrReply) ++ checkTravellerInfoErrorCategory(pnrReply) ++ (checkGeneralErrorInfoErrorCategoryIsEC(pnrReply, flowVars.PNRAction) when (handlePnrActions contains flowVars.PNRAction) otherwise {})) filter ($ != {}) default [],
	warningDetails: (checkDataElementsMasterErrorCategory(pnrReply) ++ (checkGeneralErrorInfoErrorCategoryIsEC(pnrReply, flowVars.PNRAction) when not (handlePnrActions contains flowVars.PNRAction) otherwise null) ++ checkGeneralErrorInfoErrorCategory(pnrReply)) filter ($ != {}) default []
}