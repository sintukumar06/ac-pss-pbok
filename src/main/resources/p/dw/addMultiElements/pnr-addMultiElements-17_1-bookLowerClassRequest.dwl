%dw 1.0
%output application/json skipNullOn = "everywhere"

%var businessFunctionAirProvider = "1"
%var referenceQualifierSegmentClientRequest= "SR"
%var relatedProductStatusNeedSegment = "NN"
%var optionBasicBooking = "P10"
%var segmentNameAirSegment = "AIR"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")


%var alteaTattoo2otaRPHfltHL = flowVars.alteaTattoo2otaRPHfltHL

%var flightSegmentMap = flowVars.flightSegmentsWithACLowerResBookDesigCode 
%var pnrReplyWaitListedItineraryInfo= flowVars.pnrReplyWaitListedItineraryInfo

---

{
	PNR_AddMultiElements: {
		pnrActions: {
			optionCode: [0]
		},
		
	
	originDestinationDetails: [{
		originDestination: {},
		itineraryInfo: pnrReplyWaitListedItineraryInfo map (( itineraryInfo , indexOfItineraryInfo) -> using (flghtSgmnt = flightSegmentMap[alteaTattoo2otaRPHfltHL[itineraryInfo.elementManagementItinerary.reference.number as :string]][0]) {
			(pnrAddMultiLib.createItineraryInfo( flghtSgmnt ,indexOfItineraryInfo+1,flghtSgmnt.AC_LowerResBookDesigCode))
			
			} )
		
		}] 
		}
		} 		
