%dw 1.0
%output application/json skipNullOn = "everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplyItinLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_itinerary_mapping.dwl")
---
{
	
	AirReservation: [{
		AirItinerary: (
			pnrReplyItinLib.mapAirItinerary(payload.PNR_Reply)
		)
	}]
}