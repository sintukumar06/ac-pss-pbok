%dw 1.0
%output application/json skipNullOn = "everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")
%var pbokLib = readUrl("classpath://p/dw/pbok-common-functions.dwl")

%var elementMgmtPassengerClientRequest = "PR"
%var elementMgmtNameElemenet = "NM"
%var relatedProductStatusNeedSegment = "NN"
%var businessFunctionAirProvider = "1"
%var freeTextDetailQualifierLiteralText = "3" // Literal Text
%var freeTextDetailPhoneType = "6" // Travel Agent Telephone Number
%var freeTextDetailEmailType = "P02" // Travel Agent Email
%var passengerTypeGroup = "G"
%var passengerTypeInfant = "INF"
%var passengerTypeAdult  = "ADT"
%var passengerTypeChild  = "CHD"
%var passengerTypeSenior = "SRC"
%var ticketType = "OK"
%var referenceQualifierSegmentClientRequest = "SR"
%var segmentNameContactElement = "AP"
%var segmentNameAirSegment = "AIR"
%var segmentNameTicket = "TK"
%var optionBasicBooking = "P10"
%var passengerType = "PAX"
//%var passengerTypeCodeInfantNotOccupyingASeat = "INF"
//%var passengerTypeCodeInfantOccupyingASeat = "INS"
%var docaRemarks = ["ResidenceAddress", "DestinationAddress"]


%function isSaveAirPax()
	flowVars.PNRAction == "SaveAirPax"
	
%function isSaveSSRs()
	flowVars.PNRAction == "SaveSSRs"
//endorsement	
%function isSaveEndorsement()
	flowVars.PNRAction == "SaveEndorsement" 

%var PTCFarebreakdowns = flowVars.PTCFareBreakdownsWithEndorsement when isSaveEndorsement() and flowVars.PTCFareBreakdownsWithEndorsement != null otherwise null
%var pnrRetrieveResponse = flowVars.pnrRetrieve17_1Response when isSaveEndorsement() and flowVars.pnrRetrieve17_1Response != null otherwise null
%var itineraryInfo=pnrRetrieveResponse.PNR_Reply.originDestinationDetails[0].itineraryInfo when pnrRetrieveResponse!=null otherwise null
	
%function endorsements(endorsements)
(
	(endorsements.Endorsement.*Content joinBy "//") when commonLib.isArrayNotEmpty(endorsements.Endorsement) otherwise null
)

%var airTravelerBuckets = 
	(payload.TravelerInfo.AirTraveler reduce ( (obj, acc={
	ACC_INF: [],
	INF: [],
	OTH: []}) -> {
	// TBD: Assuming the Child can't have an Infant on Lap
	ACC_INF: acc.ACC_INF + obj when obj.AccompaniedByInfantInd == true and (obj.PassengerTypeCode != passengerTypeInfant or obj.PassengerTypeCode != passengerTypeChild) otherwise acc.ACC_INF,
	INF: acc.INF + obj when obj.PassengerTypeCode == passengerTypeInfant otherwise acc.INF,
	OTH: acc.OTH + obj when obj.PassengerTypeCode != passengerTypeInfant and (obj.AccompaniedByInfantInd == null or obj.AccompaniedByInfantInd == false) otherwise acc.OTH
//	Email: acc.Email + obj when obj.Email? otherwise acc.Email,
//	Phone: acc.Phone + obj when obj.Telephone? otherwise acc.Phone
}) when payload.TravelerInfo? and payload.TravelerInfo.AirTraveler? and isSaveAirPax() otherwise null)

%var ssrObjectsMap = flowVars.otaAirbookMaps.miscObjectsMap when isSaveSSRs() and flowVars.otaAirbookMaps.miscObjectsMap != null otherwise
	pbokLib.getMiscObjs(payload) when isSaveSSRs() otherwise null
				
%var seats = ssrObjectsMap.Seats when ssrObjectsMap != null and commonLib.isArrayNotEmpty(ssrObjectsMap.Seats) otherwise null
%var SSRs = ssrObjectsMap.SSRs 
				 when ssrObjectsMap != null and commonLib.isArrayNotEmpty(ssrObjectsMap.SSRs) otherwise null

%var SKs = ssrObjectsMap.SKs when ssrObjectsMap != null and commonLib.isArrayNotEmpty(ssrObjectsMap.SKs) otherwise null

%var OSInfos = ssrObjectsMap.OSs when ssrObjectsMap != null and commonLib.isArrayNotEmpty(ssrObjectsMap.OSs) otherwise null

%var fltRPHTattooMap = flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap when flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap != null otherwise null
%var paxRPHTattooMap = flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap when flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap != null otherwise null

%var docsRefMap = pbokLib.docsRefMap

%function convert2AlteaDocDate(otaDate)
	(otaDate as :date as :string {format: "ddMMMYY"}) when commonLib.isNotEmpty(otaDate) otherwise null

%function createFreeText(elm, docRefObj, ptc)
	createDOCSFreeText(elm, docRefObj.Type,ptc) replace /-+$/ with "" when docRefObj != null and docRefObj.SSR == "DOCS" 
 	otherwise
 	       createDOCOFreeText(elm, docRefObj.Type) replace  /-+$/ with "" when docRefObj != null and docRefObj.SSR == "DOCO" 
 		otherwise
 			createDOCAFreeText(elm, docRefObj.Type, ptc) replace  /-+$/ with "" when docRefObj != null and  docRefObj.SSR == "DOCA" 
 			otherwise null

%function createDOCSFreeText(docElm, type,ptctype)
using (delimiter = "-")
commonLib.buildStringWithDelimiter([
	type, getISO3CountryCode(docElm.DocIssueCountry), docElm.DocID, 
	getISO3CountryCode(docElm.DocHolderNationality), convert2AlteaDocDate(docElm.BirthDate),
	docsRefMap[(docElm.Gender ++ ptctype) when ptctype=="INF" otherwise docElm.Gender] when docElm.Gender? otherwise null, convert2AlteaDocDate(docElm.ExpireDate),
	docElm.DocHolderName, docElm.AdditionalPersonNames[0], 
	(commonLib.buildStringWithDelimiter(docElm.AdditionalPersonNames[1..-1], " ", false)) when docElm.AdditionalPersonNames[1] != null,
	docsRefMap[docElm.HolderType] when docElm.HolderType? otherwise null
	],  delimiter, true)


%function createDOCOFreeText(docElm, type)
using (delimiter = "-")
commonLib.buildStringWithDelimiter([
	docElm.BirthPlace, type, docElm.DocID, docElm.DocIssueLocation,
	convert2AlteaDocDate(docElm.EffectiveDate), getISO3CountryCode(docElm.DocIssueCountry),
	docsRefMap[docElm.HolderType] when docElm.HolderType? and docElm.HolderType == "Infant" otherwise null
	], delimiter, true)
	
%function createDOCAFreeText(docElm, type, ptc)
using (delimiter = "-")
commonLib.buildStringWithDelimiter([type, getISO3CountryCode(docElm.CountryName.Code), 
	(( docElm.StreetNmbr.Content ++ " " ++ docElm.AddressLine[0] ) when commonLib.isNotEmpty(docElm.StreetNmbr.Content)
		otherwise
		docElm.AddressLine[0]), 
	docElm.CityName, docElm.StateProv.StateCode, docElm.PostalCode,
	docsRefMap[ptc] when ptc == "INF" otherwise null
	], delimiter, true)
	
%function createPCTCFreeText(pctcElm)
commonLib.buildStringWithDelimiter([
	commonLib.buildStringWithDelimiter([
		commonLib.buildStringWithDelimiter([pctcElm.PersonName.GivenName[0], pctcElm.PersonName.Surname], " ", true),
		pctcElm.CountryCode
		], "-", true),
	pctcElm.Telephone.PhoneNumber
	], "", false) replace  /-+$/ with ""
	
%function createPhoneNumber(countryAccessCode, areaCityCode, phoneNumber)
commonLib.buildStringWithDelimiter([countryAccessCode, areaCityCode, phoneNumber], " ", true)

%function createPassengerData (airTraveler, infAirTraveller) 
{
	travellerInformation: {
		traveller: {
			surname: airTraveler.PersonName.Surname!,
			quantity: 2 when infAirTraveller != null otherwise 1
		},
		passenger: [{
			firstName: pnrAddMultiLib.buildAlteaGivenName(airTraveler.PersonName.GivenName[0]!, airTraveler.PersonName.NamePrefix[0], airTraveler.PersonName.NameSuffix[0]),

			type: airTraveler.PassengerTypeCode!,
			((infantIndicator: "3" when infAirTraveller.PersonName.GivenName[0]? and infAirTraveller.PersonName.Surname?
									otherwise
										"2" when infAirTraveler.PersonName.GivenName[0]?
									otherwise "1"
			) unless infAirTraveller == null otherwise {})
		}]
	},
	dateOfBirth: {
		dateAndTimeDetails: {
			date: commonLib.createAlteaDate(null, airTraveler.BirthDate)
		}
	}	
}


%function createTravellerInfo (indexOfAirTraveler, airTraveler, accInfIndicator)
using (infAirTraveller = (airTravelerBuckets.INF[indexOfAirTraveler]) when accInfIndicator == true and airTravelerBuckets != null 
							and airTravelerBuckets.INF != null and indexOfAirTraveler < sizeOf airTravelerBuckets.INF otherwise null)
{
	elementManagementPassenger: {
		reference: {
			qualifier: elementMgmtPassengerClientRequest, // PR
			number: airTraveler.TravelerRefNumber.RPH!
		},
		segmentName: elementMgmtNameElemenet // NM
	},
	passengerData: [(createPassengerData (airTraveler, infAirTraveller))] ++
	[(createPassengerData (infAirTraveller, null) 
	) when infAirTraveller != null ] // TBD: Assuming the number of Infants is equals to the Adults or seniors with accompanying infants.
} when airTraveler.PassengerTypeCode? otherwise null

%function createFreeTextDataElm (segmentName, subjectQualifier, type, longFreeText, companyName, refNumber)
{
	elementManagementData: {
		segmentName: segmentName
	},
	freetextData: {
		freetextDetail: {
			subjectQualifier: subjectQualifier,
			type: type,
			companyId: companyName as :string when companyName != null otherwise null
		},
		longFreetext: longFreeText
	},
	referenceForDataElement:( {
		reference: [{
			qualifier: "PR",
			number: refNumber as :string
		}]
	}) when refNumber!= null otherwise null
}

%function createReferenceForDataElm(travelerRPHs, flightRPHs)
w: {
	referenceForDataElement: {
		reference: 
		(
			(
				(
					travelerRPHs map ((rph, idxRph) -> {
						qualifier: "PT",
						number: paxRPHTattooMap[rph] as :string
					} when paxRPHTattooMap != null and paxRPHTattooMap[rph] != null otherwise {}
				) when commonLib.isArrayNotEmpty(travelerRPHs) otherwise {})	
			)
			++
			(
				(
					flightRPHs map ((rph, idxRph) -> {
						qualifier: "ST",
						number: fltRPHTattooMap[rph] as :string
					} when fltRPHTattooMap != null and fltRPHTattooMap[rph] != null otherwise {}
				) when commonLib.isArrayNotEmpty(flightRPHs) and not isSaveEndorsement() otherwise {})
			)
			++
			(
				(
					referenceDetails: flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap map {
					qualifier: "ST",
					number: $ as :string}
 
				 when (flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap != null and isSaveEndorsement() )
				otherwise [{}])[0]
				
				
			)  //endorsement
		) filter $ != {}
	} when (commonLib.isArrayNotEmpty(travelerRPHs) or commonLib.isArrayNotEmpty(flightRPHs)) and (fltRPHTattooMap != null or  paxRPHTattooMap != null) otherwise null	
	
}

%function createSSRElement (type, freetext, status, companyId, quantity)
w: {
	ssr: {
		type: type,
		status: status when status != null otherwise null,
		quantity: quantity when quantity != null otherwise null,
		companyId: companyId when companyId != null otherwise null,
		freetext: [freetext] when freetext != null otherwise null
	}
}
---

{
	PNR_AddMultiElements: {
		pnrActions: {
			optionCode: [0]
		},
		// Traveler Info element for 1A / Amadeus can only exist for Adults. 
		// Infants are grouped under the adult as a second 'passengerData' structure. 
		// Filter is added before the map to remove any AirTravelers that are not Adults. 

		(travellerInfo: (airTravelerBuckets.ACC_INF map ((airTraveler, indexOfAirTraveler) -> 
			createTravellerInfo (indexOfAirTraveler, airTraveler, true)
		)) ++
		(airTravelerBuckets.OTH map ((airTraveler, indexOfAirTraveler) -> 
			createTravellerInfo (indexOfAirTraveler, airTraveler, false)
		))) when airTravelerBuckets != null,
		
		// originDestinationDetails
		originDestinationDetails : payload.AirItinerary.OriginDestinationOptions map (originDestinationOption , indexOfOriginDestinationOption) -> {
				originDestination: {}, //Empty Element
				itineraryInfo: originDestinationOption.FlightSegment map (flightSegment , indexOfFlightSegment)  -> using (nbrInParty=flightSegment.NumberInParty!) {									
						(pnrAddMultiLib.createItineraryInfo( flightSegment,indexOfFlightSegment+1,flightSegment.ResBookDesigCode))	
				} when originDestinationOption != null otherwise null
		} when flowVars.longSessionId == null and isSaveAirPax() and payload.AirItinerary != null and payload.AirItinerary.OriginDestinationOptions? otherwise null, // End payload.AirItinerary.OriginDestinationOptions map

		dataElementsMaster: {
			marker1: {},
			dataElementsIndiv:
				//AirTraveler ContatElm mappings
				flatten (payload.TravelerInfo.AirTraveler map (traveler, idxTraveler) -> using (address = (traveler.Address filter $.Remark == null or not (docaRemarks contains $.Remark))[0],
					travellerRPH=traveler.TravelerRefNumber.RPH!,
					flightRPHs=traveler.FlightSegmentRPHs) ( 
					 (traveler.Telephone map (phone, idxPhone) -> (
						createFreeTextDataElm (segmentNameContactElement, freeTextDetailQualifierLiteralText, phone.PhoneLocationType default freeTextDetailPhoneType, (createPhoneNumber(phone.CountryAccessCode, phone.AreaCityCode, phone.PhoneNumber) when freeTextDetailQualifierLiteralText == "3" and phone.CountryAccessCode? and phone.AreaCityCode? otherwise phone.PhoneNumber), null, traveler.TravelerRefNumber.RPH) //AP, 3, 3/4/5/6/7/P01
					) when isSaveAirPax() and commonLib.isArrayNotEmpty(traveler.Telephone)  otherwise {}) ++ 
					(traveler.Email map (email, idxEmail) -> (
						createFreeTextDataElm (segmentNameContactElement, freeTextDetailQualifierLiteralText, freeTextDetailEmailType, email.Content, null, traveler.TravelerRefNumber.RPH) //AP, 3, P02
					) when isSaveAirPax() and commonLib.isArrayNotEmpty(traveler.Email)  otherwise {}) ++ 
					([{ //Billing Address
						elementManagementData: {
							segmentName: "AB"
						},
						structuredAddress: {
							informationType: "P08",
							address: {
								optionA1: "A1",
								optionTextA1: address.AddressLine[0]								
							} when address.AddressLine[0]? otherwise null,
							optionalData : [
								{
									option: "A2",
									optionText: address.AddressLine[1]	
								} when address.AddressLine[1]? otherwise null,
								{
									option: "CI",
									optionText: address.CityName
								} when address.CityName? otherwise null,
								{
									option: "CO",
									optionText: address.CountryName.Code
								} when address.CountryName.Code? otherwise null,
								{
									option: "NA",
									optionText: pnrAddMultiLib.buildAlteaGivenName(traveler.PersonName.GivenName[0]!,traveler.PersonName.NamePrefix[0],traveler.PersonName.NameSuffix[0]) ++ traveler.PersonName.Surname!
								},
								{
									option: "ST",
									optionText: address.StateProv.StateCode
								} when address.StateProv? and address.StateProv.StateCode? otherwise null,
								{
									option: "ZP",
									optionText: address.PostalCode
								} when address.PostalCode? otherwise null
							]
						},
						referenceForDataElement: {
							reference: [{
								qualifier: "PR",
								number: traveler.TravelerRefNumber.RPH
							}]
						}
					}] when isSaveAirPax() and address != null otherwise {}) 
					++ 
					(traveler.Document map (doc, idxDoc) -> using (
						docRefObj=docsRefMap[doc.AC_DocType] when docsRefMap[doc.AC_DocType] != null otherwise docsRefMap["NA"]
					) { 
						elementManagementData: { //TBD: TBA question, Not found in IMD
							segmentName: "SSR"
						},
						serviceRequest: {
							(createSSRElement(docRefObj.SSR when docRefObj != null otherwise null, createFreeText(doc, docRefObj,traveler.PassengerTypeCode), "HK", null, 1))
						},
						(createReferenceForDataElm([travellerRPH],null))
					} when isSaveSSRs() and commonLib.isArrayNotEmpty(traveler.Document) otherwise {}) ++ 
					([{
						elementManagementData: { //TBD: TBA question, Not found in IMD
							segmentName: "SSR"
						},						
						serviceRequest: {
							(createSSRElement("PCTC", createPCTCFreeText(traveler.AC_EmergencyContactInfo), "HK", null, null))
						},
						(createReferenceForDataElm([travellerRPH],flightRPHs))						
					}]	when isSaveSSRs() and commonLib.isNotEmpty(traveler.AC_EmergencyContactInfo) otherwise {}) ++
					(traveler.Address map ((doca, idxDoca) ->using (docRefObj=docsRefMap[doca.Remark]) { 
						elementManagementData: { //TBD: TBA question, Not found in IMD
							segmentName: "SSR"
						},
						serviceRequest: {
							(createSSRElement(docRefObj.SSR when docRefObj != null otherwise null, createFreeText(doca, docRefObj, traveler.PassengerTypeCode), "HK", null, 1))
						},
						(createReferenceForDataElm([travellerRPH],flightRPHs))
					} when doca.Remark != null and (docaRemarks contains doca.Remark) otherwise {}
						) when isSaveSSRs() and commonLib.isArrayNotEmpty(traveler.Address) otherwise {})												
				) when payload.TravelerInfo.AirTraveler? otherwise {} ) ++ 
				([{ // Ticket Segment
					elementManagementData: {
						segmentName: segmentNameTicket
					},
					ticketElement: {
						passengerType: passengerType,
						ticket: {
							indicator: ticketType
						}
					}
				}] when isSaveAirPax() otherwise {}) ++ 
				(seats map (seat, idSeat) -> {
					elementManagementData: {
						segmentName: "STR"
					},
					seatGroup: {
						seatRequest: {					  	  
							special: [{
								data: seat.SeatNumber
							}]
						} when seat.SeatNumber? otherwise {
					  	    seat: {
					  	  	 type: "NSST"
					  		},						  	  
							special: [{
								data: seat.SeatPreference[0]
							}]
						} when ["A","W"] contains seat.SeatPreference[0] otherwise null
					},
					(createReferenceForDataElm(seat.TravelerRefNumberRPHList,seat.FlightRefNumberRPHList))											
				} when seats != null otherwise {}) ++
				(SSRs map (ssr, idxSsr) -> { //SSRs
					elementManagementData: {
						segmentName: "SSR"
					},
					serviceRequest: {
						//(createSSRElement(ssr.SSRCode, ssr.Text, "HK", null, 1))
						(createSSRElement(ssr.SSRCode, ssr.Text, null, null, null))						
					},
					(createReferenceForDataElm(ssr.TravelerRefNumberRPHList,ssr.FlightRefNumberRPHList))		
				} when SSRs != null and commonLib.isArrayNotEmpty(SSRs) otherwise {}) ++ 
				(SKs map (sk, idxsk) -> { //SK Segment
					elementManagementData: {
						segmentName: "SK"
					},
					serviceRequest: {
						(createSSRElement(sk.Code, sk.Text, "HK", sk.Airline.Code when sk.Airline? otherwise null, 1))
					},
					(createReferenceForDataElm(sk.TravelerRefNumber.RPH,sk.FlightRefNumber.RPH))						
				} when SKs != null and commonLib.isArrayNotEmpty(SKs) otherwise {}) ++ 
				(OSInfos map using (comanyName= $.Airline.Code when $.Airline? otherwise null) {
				//OS segment
					(w:createFreeTextDataElm ("OS", freeTextDetailQualifierLiteralText, "P27", $.Text, comanyName,null)), //20, 3, P27,										
					(createReferenceForDataElm($.TravelerRefNumber.RPH,$.FlightRefNumber.RPH))										
				} when OSInfos != null and commonLib.isArrayNotEmpty(OSInfos) otherwise {}) ++
				(PTCFarebreakdowns map { //endorsements
					elementManagementData: { 
					segmentName: "FE"
					},
					fareElement:{
						generalIndicator:"E",
						passengerType:"PAX",
						freetextLong:endorsements($.Endorsements)
						},
						(createReferenceForDataElm($.TravelerRefNumber.*RPH,null)) //check null
						
					} when isSaveEndorsement() and PTCFarebreakdowns != null otherwise {}
				 )
				filter $ != {} //filter out empty elements
		} when airTravelerBuckets != null or isSaveSSRs() or isSaveEndorsement() otherwise null // dataElementsMaster		
	}
}