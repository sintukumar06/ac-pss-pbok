%dw 1.0
%output application/json
%var commonConst = readUrl("classpath://p/dw/common-constants.dwl")
%var pos = flowVars.originalOtaCancelRequest.POS.Source
%var vEmails = flowVars.originalOtaCancelRequest.Verification.email default []
%function trimNull(str)
	str default ""
%var refundType = flowVars.originalOtaCancelRequest.UniqueID.RefundType 
	
%var freeText = flowVars.clientAppId when pos==null otherwise 
	trimNull(pos.RequestorID.CompanyName.CompanyShortName) ++ "-" ++ trimNull(pos.RequestorID.ID_Context)
	++ "-" ++ trimNull(pos.RSP_UserID) ++ "-" ++ trimNull(pos.AgentSine)	
%var remarkMaxSize = commonConst.remarkMaxSize
%function addRemarks(remarks)
	remarks map 	
	({
			elementManagementData: {
				segmentName: "RM"
			},
			miscellaneousRemark: {
				remarks: {
					type: "RM",
					freetext: $ when (sizeOf $) <= remarkMaxSize otherwise $[0 to remarkMaxSize-4] ++ "..."
				}
			}
		} when $ != null otherwise null)
		when remarks != null otherwise {}

%function addCancelRemark(vEmails)
	vEmails map {
	//cancel remark
		elementManagementData: {
			segmentName: "AP"
		},
		freetextData: {
			freetextDetail: {
				subjectQualifier: "3",
				type: "N"				
			},
			longFreetext: "AC/E " ++ $ ++ " **NOTIFY"
		}
	} when vEmails !=[] otherwise {}
%function addRefundRemarks(refundResList)
	refundResList map {
		elementManagementData: {
			segmentName: "RI"
		},
		extendedRemark: {
			structuredRemark: {
				type: "RIF",
				freetext: "RFND" ++ "/" ++ $.ticketNbr ++ "/" ++ $.currency ++ " BF:" ++ $.baseAmount ++ " TX:" ++ $.tax ++ " TF:" ++ $.total ++ "/SEGMENT CANCELLATION"
			}
		}
	} when refundResList != null and refundType == "refund" otherwise {}
---

{
	marker1:{},
	dataElementsIndiv:[
	{
		elementManagementData:{
			segmentName: "RF"
		},
		freetextData:{
			freetextDetail:{
				subjectQualifier: "3",
				type: "P22"				
			},
			longFreetext: freeText
		}
		
	}
	,({	//AC2U remark
		elementManagementData: {
			segmentName: "RM"
		},
		extendedRemark: {
			structuredRemark: {
				type: "RM",
				freetext: "PNR CANCELLED BY AC2U CLIENT " ++ trimNull(pos.RequestorID.CompanyName.CompanyShortName) ++ " FOR AGY " ++ trimNull(pos.RequestorID.ID_Context)		
			}
		}
	})  when flowVars.clientAppId == "AC2"
	] 
	++ addCancelRemark(vEmails)
	++ addRefundRemarks(flowVars.refundResList)
	++ addRemarks(flowVars.eotRemarks)
} 
