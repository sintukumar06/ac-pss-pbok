%dw 1.0

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%var businessFunctionAirProvider = "1"
%var referenceQualifierSegmentClientRequest= "SR"
%var relatedProductStatusNeedSegment = "NN"
%var optionBasicBooking = "P10"
%var segmentNameAirSegment = "AIR"

%function getItineraryInfo (input, relatedProductStatus)
	input filter ((relatedProductStatus contains $.relatedProduct.status[0]) == true )
	when commonLib.isArrayNotEmpty(input) otherwise null


%function buildAlteaGivenName (givenName, prefix, suffix) 
using (
	delim=" "
)
	commonLib.buildStringWithDelimiter([givenName, suffix, prefix], delim, false)
	
	
%function createItineraryInfo(flightSegment, index, classOfService)
c:{
			elementManagementItinerary: {
				reference: {
				qualifier: referenceQualifierSegmentClientRequest, // SR
				number: (index  +1) as :string
				},
					segmentName: segmentNameAirSegment
				},
				airAuxItinerary: {
					travelProduct: {
						product: {
							depDate: commonLib.createAlteaDate(flightSegment.DepartureDateTime!, null),
							depTime: commonLib.createAlteaTime(flightSegment.DepartureDateTime!),
							arrDate: commonLib.createAlteaDate(flightSegment.ArrivalDateTime!, null),
							arrTime: commonLib.createAlteaTime(flightSegment.ArrivalDateTime!)
							},
								boardpointDetail: {
									cityCode: flightSegment.DepartureAirport.LocationCode
									
								}
								,
								offpointDetail: {
									cityCode: flightSegment.ArrivalAirport.LocationCode!
								}
								,
								company: {
									identification: flightSegment.MarketingAirline.Code!
								},
								productDetails: {
									identification: flightSegment.FlightNumber!,
									classOfService: classOfService
								}
						}, //travelProduct
						
							messageAction: {
								business: {
									function: businessFunctionAirProvider  //1
								}
							},
							relatedProduct: {
								quantity: flightSegment.NumberInParty!,
								status:  flightSegment.Status
											when (flightSegment.Status? 
												and (flightSegment.Status == "SA")
											) otherwise relatedProductStatusNeedSegment 
							},
							selectionDetailsAir: {
								selection: [{
									option: optionBasicBooking  //P10
								}]
							}
				}
			
}
---
{
	getItineraryInfo: getItineraryInfo,
	//mapAirItinerary: commonpnrreplyLib.mapAirItinerary,  use commonpnrreplyLib.mapAirItinerary instead
	buildAlteaGivenName: buildAlteaGivenName,
	createItineraryInfo: createItineraryInfo
}
