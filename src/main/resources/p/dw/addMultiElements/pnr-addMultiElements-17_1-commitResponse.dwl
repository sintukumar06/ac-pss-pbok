%dw 1.0
%output application/json
skipNullOn = "everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplyItinLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_itinerary_mapping.dwl")
%var pnrReplyTravellerLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_travellerInfo_mapping.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")
%var pnrReplyPriceInfoLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_priceInfo_mapping.dwl")
%var pnrReplyMiscLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_misc_mapping.dwl")

%var fopResponse = flowVars.otaFopCreateFormOfPayment15_4Response when flowVars.otaFopCreateFormOfPayment15_4Response != null otherwise null

//Creating paxDatas by adding passenger tattoo from the parent travellerInfo
//traverllerInfo[].passengerData[]
%var pnrReply = payload.PNR_Reply
%var passengerData = pnrReplycommonLib.getPaxDatas(pnrReply)
//Creating dataElement buckets for different types.
%var dataElmByType = pnrReplycommonLib.createDataElmBuckets(pnrReply)
---

{
  AirReservation: [{
    
    AirItinerary: pnrReplyItinLib.mapAirItinerary(pnrReply),

    PriceInfo: pnrReplyPriceInfoLib.mapPriceInfo(pnrReply, dataElmByType, passengerData),
    
    TravelerInfo: pnrReplyTravellerLib.mapTravelerInfo(passengerData, dataElmByType, pnrReply, getISO2CountryCode),
  
    Fulfillment: fopResponse.AirReservation[0].Fulfillment when fopResponse.AirReservation[0].Fulfillment? otherwise null, //pnrReplyMiscLib.mapFulfillment(dataElmByType),
        
    Ticketing: pnrReplyMiscLib.mapTicketing(dataElmByType),     
        
    BookingReferenceID: pnrReplyMiscLib.mapBookingReference(pnrReply),
        
    LastModified: pnrReplyMiscLib.mapLastModified(pnrReply),
    
    CreateDateTime: pnrReplyMiscLib.mapCreateDateTime(pnrReply)
  }]
}