%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonConst = readUrl("classpath://p/dw/common-constants.dwl")

%var elementMgmtPassengerReceiveFromElement = "RF"
%var elementMgmtSegmentGeneralRemarkElement = "RM"
%var informationTypeReceiveFrom = "P22"
%var remarkMaxSize = commonConst.remarkMaxSize

%function mapRemarks(remarks)
	flatten remarks map 	
	({
			elementManagementData: {
				segmentName: elementMgmtSegmentGeneralRemarkElement // RM
			},
			miscellaneousRemark: {
				remarks: {
					type: elementMgmtSegmentGeneralRemarkElement, // RM
					freetext: $.Content when (sizeOf $.Content) <= remarkMaxSize otherwise $.Content[0 to remarkMaxSize-4] ++ "..."
				}
			}
		} when $.Content != null otherwise null) 
		when remarks != null otherwise {}

---
{
	PNR_AddMultiElements: {
		pnrActions: {
			optionCode: [11]
		},
		dataElementsMaster: {
			marker1: {},
			dataElementsIndiv: [
			{
				elementManagementData: {
					segmentName: elementMgmtPassengerReceiveFromElement // RF
				},
				freetextData: {
					freetextDetail: {
						subjectQualifier: "3",
						type: informationTypeReceiveFrom // P22
					},
					longFreetext: flowVars.clientAppId
				}
			},
			{
				elementManagementData: {
					segmentName: "FT"
				},
				tourCode:{
					passengerType:"PAX",
					freeFormatTour:{
						indicator:"FF",
						freetext: flowVars.originalOtaAirbookRequest.PriceInfo.ItinTotalFare[0].TourCode
					}
					
				}
			} when flowVars.originalOtaAirbookRequest.PriceInfo.ItinTotalFare[0].TourCode? and flowVars.pnrRetrieve17_1Response != null and not (flowVars.pnrRetrieve17_1Response.PNR_Reply.dataElementsMaster.dataElementsIndiv.elementManagementData.segmentName contains "FP")
				otherwise null
			] 
			++ mapRemarks(flowVars.originalOtaAirbookRequest.TravelerInfo.SpecialReqDetails..Remarks)
		}
	}
}