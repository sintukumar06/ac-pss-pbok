%dw 1.0
%output application/json


%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")



%var pnrAddMultiResponse = flowVars.pnrAddMultiElements17_1SaveResponse when flowVars.pnrAddMultiElements17_1SaveResponse != null otherwise flowVars.pnrRetrieve17_1Response

%var pnrReplyItineraryInfoAir = flatten (pnrAddMultiResponse.PNR_Reply.originDestinationDetails.itineraryInfo)  
								filter $.elementManagementItinerary.segmentName=="AIR"
								when commonLib.isArrayNotEmpty(pnrAddMultiResponse.PNR_Reply.originDestinationDetails) 
								and commonLib.isArrayNotEmpty(pnrAddMultiResponse.PNR_Reply.originDestinationDetails.itineraryInfo) 
								otherwise null

%var origDestMap = flowVars.otaAirbookMaps.fltSegmentMap

%function getFlightRPHTattoos (itineraryInfo)
	itineraryInfo map 
		using (key="FltSegment_" ++ $.travelProduct.boardpointDetail.cityCode ++ "_" ++ commonLib.createOTATS($.travelProduct.product.depDate, $.travelProduct.product.depTime)
		++ "_" ++ $.travelProduct.offpointDetail.cityCode ++ "_" ++ commonLib.createOTATS($.travelProduct.product.arrDate, $.travelProduct.product.arrTime)
		++ "_" ++ $.travelProduct.companyDetail.identification ++ "_" ++ $.travelProduct.productDetails.identification,
		tattoo = $.elementManagementItinerary.reference.number) 
		using (name=origDestMap[key] when origDestMap[key] != null and origDestMap[key] != "" otherwise tattoo as :string){
					
		(name): tattoo	 		
	} when itineraryInfo != null and itineraryInfo[0].elementManagementItinerary.reference.qualifier == "ST" otherwise null
	
%function createFlightRPHTattooMap (flightRPHTattoos)
 (flatten flightRPHTattoos) reduce (obj, acc) -> {
	 (acc + obj)
} when commonLib.isArrayNotEmpty(flightRPHTattoos) otherwise null
	
%var paxRPHMap = flowVars.otaAirbookMaps.paxRPHMap
%function getPaxRPHTattoos()
	pnrAddMultiResponse.PNR_Reply.travellerInfo map (travellerInfo, idxTravellerInfo) -> ( 
		travellerInfo.passengerData map  
			using (key=($.travellerInformation.traveller.surname ++ "_") default ''
				++ ($.travellerInformation.passenger[0].firstName ++ "_") default ''
				++ $.travellerInformation.passenger[0].type default '',
				tattoo= travellerInfo.elementManagementPassenger.reference.number)
				using (name=paxRPHMap[upper key] when paxRPHMap[upper key] != null otherwise tattoo as :string) {
			(name): tattoo
		}	
	) when commonLib.isArrayNotEmpty(pnrAddMultiResponse.PNR_Reply.travellerInfo)  otherwise null

%function getPaxRPHTattooMap(paxRPHTattoos)
 (flatten paxRPHTattoos)  reduce (obj, acc) -> {
 	(acc + obj)
 } when commonLib.isArrayNotEmpty(paxRPHTattoos) otherwise null	
---
{
	
	fltRPHTattooMap: createFlightRPHTattooMap(getFlightRPHTattoos(pnrAddMultiLib.getItineraryInfo(pnrReplyItineraryInfoAir,["HK","SA"]))),
	fltRPHTattooMapHL: (createFlightRPHTattooMap(getFlightRPHTattoos(pnrAddMultiLib.getItineraryInfo(pnrReplyItineraryInfoAir,["HL"])))) when (flowVars.PNRAction != "BookLowerClass") otherwise null,
	paxRPHTattooMap: getPaxRPHTattooMap(getPaxRPHTattoos())
	
} when flowVars.otaRPH2AlteaTattoo == null and (
		flowVars.PNRAction == "SaveAirPax" or 
		flowVars.originalOtaAirbookModifyRequest != null or
		flowVars.PNRAction == "BookLowerClass" or
		flowVars.isPrevFQTVFOPError == true
) otherwise flowVars.otaRPH2AlteaTattoo