%dw 1.0
//import this function as %var pbokLib = readUrl("classpath://p/dw/pbok-common-functions.dwl")
//and refer to it's functions as pbokLib.pnrReplyFilter(...)
//
//NOTE: if you modified this file, you need to run mvn install before it will take effect in your DW preview

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

//Document type to Amadeus Type Map
%var docsRefMap = {
  "Passport": { "SSR": "DOCS", "Type": "P"},
  "USPassport": { "SSR": "DOCS", "Type": "P"},
  "USAlienCard": { "SSR": "DOCS", "Type": "C"},
  "CAResidentCard": { "SSR": "DOCS", "Type": "C"},
  "NA": { "SSR": "DOCS", "Type": ""},
  "PermanentResidentCard": { "SSR": "DOCS", "Type": "C"},
  "Nexus": { "SSR": "DOCS", "Type": "IN"},
  "IdentityCard": { "SSR": "DOCS", "Type": "I"},
  "KnownTraveller": { "SSR": "DOCO", "Type": "K"},
  "Visa": { "SSR": "DOCO", "Type": "V"},  
  "Redress": {   "SSR": "DOCO","Type": "R"},
  
  "ResidenceAddress": { "SSR": "DOCA", "Type": "R"},
  "DestinationAddress": { "SSR": "DOCA", "Type": "D"},
  
  "Infant" : "I", "HeadOfHousehold": "H",   "Male": "M", "Female": "F", "MaleINF": "MI", "FemaleINF": "FI",  "INF": "I"
}

//dataElement - a single dataElementsIndiv or itineraryInfo
//qualifier - "PT","ST"
//returns all the matching references under dataElement
%function findRefByQualifier(dataElement,qualifier)
  dataElement.referenceForDataElement.reference filter $.qualifier == qualifier when dataElement.referenceForDataElement? 
    otherwise dataElement.referenceForSegment.reference filter $.qualifier == qualifier when dataElement.referenceForSegment?
      otherwise []

//dataElement - a single dataElementsIndiv
//qualifier - "PT","ST"
//returns the FIRST matching refNumber of the specific qualifier otherwise null
%function findRefNumberByQualifier(dataElement,qualifier)
  findRefByQualifier(dataElement,qualifier)[0].number when findRefByQualifier(dataElement,qualifier) != [] otherwise null

//Filter elements of request by matching PNR_Reply's traveller and segment 
//Params:
//  elements - list of elements from request
//  data - a single dataElementsIndiv from PNR_Reply
//  matchFunc - a function that matches each element in elements with data for filtering
//returns true/false depending on if it finds a match
%function pnrReplyFilter(elements,data,matchFunc,travellerRPH = "0") using (
                    refST = findRefNumberByQualifier(data,"ST"),
                    refPT = findRefNumberByQualifier(data,"PT")
                    )
(
  true when elements!=null and (sizeOf (elements filter matchFunc($,data)
                    and (travellerRPH == refPT or $.TravelerRefNumberRPHList != null and ($.TravelerRefNumberRPHList contains refPT))
                    and ($.FlightRefNumberRPHList == null or ($.FlightRefNumberRPHList contains refST))
            )
        )   > 0
  otherwise
  false
)
//filterHelper(elements,data,matchFunc,refST,refPT,travellerRPH)

//get the list of reference.number of the pnrElements dataElementsIndiv
%function getRefNumber(pnrElms)
  pnrElms.elementManagementData.reference.number when pnrElms!=null and (sizeOf pnrElms) > 0 otherwise []

%function filterFightSegRPH(flightSegments,segment)
  flightSegments != null and flightSegments !=[] and (flightSegments.*RPH contains segment.RPH)

//////////STs
//flight segments
%function getSegsRefToCancel(otaReq) 
  using (modSegs = flatten otaReq.AirBookModifyRQ.AirItinerary.OriginDestinationOptions..FlightSegment when otaReq.AirBookModifyRQ.AirItinerary.OriginDestinationOptions..FlightSegment? otherwise [])
  (
    using (segs = flatten otaReq.AirReservation.AirItinerary.OriginDestinationOptions..FlightSegment filter not filterFightSegRPH(modSegs,$) when otaReq.AirReservation.AirItinerary.OriginDestinationOptions..FlightSegment? otherwise [])
      (segs map $.RPH as :number when segs !=[] otherwise [])
  )

%function getItineraryElements(pnrReply,qualifier)
  (flatten pnrReply.originDestinationDetails.itineraryInfo) filter pnrReply.originDestinationDetails.itineraryInfo? and $.elementManagementItinerary.reference.qualifier == qualifier
%function getDataElements(pnrReply,qualifier)
  pnrReply.dataElementsMaster.dataElementsIndiv filter $.serviceRequest.ssr.type? and $.elementManagementData.reference.qualifier == qualifier

//svcs
%function matchSVCs(reqData,pnrElm)
pnrElm.travelProduct.productDetails.identification == reqData.SSRCode
%function getSvcsToCancel(modifyRQ,pnrReply)
  using (svcReq = (flatten modifyRQ.TravelerInfo..SpecialServiceRequests) filter $.Operation == "Replace" when modifyRQ.TravelerInfo..SpecialServiceRequests? otherwise []
        ,svcSTElements = getItineraryElements(pnrReply,"ST"))
          ( using (svcsToCancel = svcSTElements filter pnrReplyFilter(svcReq,$,matchSVCs))
            getRefNumber(svcsToCancel)
          )

////////////OTs
//Seats
%function getSeatRefsToCancel(modifyRQ,ssrOTElements)
using (
  prevSeats = (flatten modifyRQ.TravelerInfo..SeatRequests) filter $.AC_PreviousSeatNumber? when modifyRQ.TravelerInfo..SeatRequests? otherwise [],
  seatElements = ssrOTElements filter ($.serviceRequest.ssr.type == "RQST" or $.serviceRequest.ssr.type == "NSST")
)
getRefNumber(seatElements filter pnrReplyFilter(prevSeats,$,matchSeats))
%function matchSeats(reqData,pnrElm)
pnrElm.serviceRequest.ssrb.data contains reqData.AC_PreviousSeatNumber

//SSRs
%function getSsrRefsToCancel(modifyRQ,ssrOTElements)
using (
  ssrReq = (flatten modifyRQ.TravelerInfo..SpecialServiceRequests) filter $.Operation == "Replace" when modifyRQ.TravelerInfo..SpecialServiceRequests? otherwise []
)
getRefNumber(ssrOTElements filter pnrReplyFilter(ssrReq,$,matchSSRs))
%function matchSSRs(reqData,pnrElm)
pnrElm.elementManagementData.reference.number ~= reqData.Number and pnrElm.elementManagementData.reference.qualifier == "OT"

//DOCS DOCO
%function getDocRefsToCancel(travelers,ssrOTElements)
//using (travelers = modifyRQ.TravelerInfo.AirTraveler)
getRefNumber(
  flatten (travelers map (traveler) -> (ssrOTElements filter pnrReplyFilter(traveler.Document,$,matchDocs,traveler.TravelerRefNumber.RPH)))
) when travelers !=null otherwise []
%function matchDocs(reqData,pnrElm)
reqData !=null and reqData.Operation == "Replace" and 
(pnrElm.serviceRequest.ssr.type == "DOCS" or pnrElm.serviceRequest.ssr.type == "DOCO") and
pnrElm.serviceRequest.ssr.freeText? and docsRefMap[reqData.AC_DocType] != null and docsRefMap[reqData.AC_DocType].Type != null and (pnrElm.serviceRequest.ssr.freeText[0][0] == docsRefMap[reqData.AC_DocType].Type[0]
or
pnrElm.serviceRequest.ssr.freeText[0][1] == docsRefMap[reqData.AC_DocType].Type[0])

//DOCA
%function matchAddr(reqData,pnrElm)
pnrElm.serviceRequest.ssr.freeText? and reqData.Operation == "Replace" and reqData.Remark?
and pnrElm.serviceRequest.ssr.freeText[0][0] == reqData.Remark[0]
%function getDocaRefsToCancel(travelers,ssrOTElements)
getRefNumber(
  flatten (travelers map (traveler) -> (ssrOTElements filter pnrReplyFilter(traveler.Address,$,matchAddr,traveler.TravelerRefNumber.RPH)))
) when travelers!=null otherwise []

//PCTC
%function matchPCTC(traveler,pnrElm)
pnrElm.serviceRequest.ssr.type == "PCTC" and traveler.AC_EmergencyContactInfo.Operation == "Replace"
%function getPctcRefsToCancel(travelers,ssrOTElements) 
getRefNumber(
  flatten (travelers map (traveler) -> (ssrOTElements filter pnrReplyFilter(travelers,$,matchPCTC,traveler.TravelerRefNumber.RPH)))
) when travelers!=null otherwise []

//OSI/SK
%function getOtherRPHsToCancel(modifyRQ)
using (otherServices = (flatten modifyRQ.TravelerInfo.SpecialReqDetails..OtherServiceInformation) filter $.Operation == "Replace" when modifyRQ.TravelerInfo.SpecialReqDetails..OtherServiceInformation != null otherwise [])
otherServices map $.RPH! as :number

//Remarks
%function getRemarkRPHsToCancel(modifyRQ)
using (remarks = (flatten modifyRQ.TravelerInfo.SpecialReqDetails..Remarks) filter $.Operation == "Replace" when modifyRQ.TravelerInfo.SpecialReqDetails..Remarks != null otherwise [])
remarks map $.RPH! as :number

//FQTV
%function matchFQTV(royalty,pnrElm)
pnrElm.serviceRequest.ssr.type == "FQTV" and royalty.Operation == "Replace"
%function getFqtvRefsToCancel(travelers,ssrOTElements)
getRefNumber(
  flatten (travelers map (traveler) -> (ssrOTElements filter pnrReplyFilter(traveler.CustLoyalty,$,matchFQTV,traveler.TravelerRefNumber.RPH)))
) when travelers!=null otherwise []

//FOP cancel all FOP
%function getFopRefsToCancel(ssrOTElements)
getRefNumber(ssrOTElements filter $.elementManagementData.segmentName == "FP")      

%function getMiscObjs (airbookRQ)
  using (
    otherSvcInfos = flatten airbookRQ.TravelerInfo.SpecialReqDetails.OtherServiceInformations.OtherServiceInformation
            when commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails) 
              and commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails.OtherServiceInformations.OtherServiceInformation)
            otherwise null,
    specialSvcReqs = flatten airbookRQ.TravelerInfo.SpecialReqDetails.SpecialServiceRequests 
            when commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails) 
              and commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails.SpecialServiceRequests) otherwise null            
  )
 using (
    otherSvcBuckets = otherSvcInfos reduce ((obj, acc={SKs: [], OSs: []}) -> {
      SKs: acc.SKs + obj when obj.Code? otherwise acc.SKs,
      OSs: acc.OSs + obj when commonLib.isEmpty(obj.Code) otherwise acc.OSs 
    }) when otherSvcInfos != null otherwise null,
    anciallaryBuckets = specialSvcReqs reduce  ((obj, acc={SSRs: [], SVCs: []}) -> {
      SSRs: acc.SSRs + obj when (commonLib.isNotEmpty(obj.AC_Type) and obj.AC_Type == "SSR") otherwise acc.SSRs,
      SVCs: acc.SVCs + obj when (commonLib.isNotEmpty(obj.AC_Type) and obj.AC_Type == "SVC") otherwise acc.SVCs
    }) when specialSvcReqs != null otherwise null
  )
    {
      Seats: flatten airbookRQ.TravelerInfo.SpecialReqDetails.SeatRequests when commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails) and commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.SpecialReqDetails.SeatRequests) otherwise null,
      SSRs: anciallaryBuckets.SSRs,
      SVCs: anciallaryBuckets.SVCs,
      SKs: otherSvcBuckets.SKs,
      OSs: otherSvcBuckets.OSs,
      isDocs: commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.AirTraveler.Document),
      isPCTC: commonLib.isNotEmpty(airbookRQ.TravelerInfo.AirTraveler.AC_EmergencyContactInfo[0]),
      isDOCA: commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.AirTraveler.Address) and commonLib.isArrayNotEmpty(airbookRQ.TravelerInfo.AirTraveler.Address.remark)
    }

%function parseTicketObj(longFreeText)
//test PAX 014-2100023570/ETAC/CAD193.30/07JUN18/YULAC0980/00100052
null when commonLib.isEmpty(longFreeText)
otherwise using (ary = (longFreeText splitBy "/"))
{
  ticketNbr: commonLib.extractAllNumbers(ary[0]),
  status: ary[1][0 to 1],
  price: commonLib.extractAllNumbers(ary[2]) as :number when ary[2] != "" otherwise 0,
  date: ary[3],
  officeId: ary[4],
  docNbr:ary[5]
}

---
{
	getRefNumber:getRefNumber,
	pnrReplyFilter:pnrReplyFilter,
	findRefByQualifier:findRefByQualifier,
	docsRefMap:docsRefMap,
	getItineraryElements:getItineraryElements,
	getDataElements:getDataElements,
	//for cancel ST
	getSegsRefToCancel:getSegsRefToCancel,
	getSvcsToCancel:getSvcsToCancel,
	//for cancel OT
	getSeatRefsToCancel:getSeatRefsToCancel,
	getSsrRefsToCancel:getSsrRefsToCancel,
	getDocRefsToCancel:getDocRefsToCancel,
	getDocaRefsToCancel:getDocaRefsToCancel,
	getPctcRefsToCancel:getPctcRefsToCancel,
	getOtherRPHsToCancel:getOtherRPHsToCancel,
	getRemarkRPHsToCancel:getRemarkRPHsToCancel,
	getFqtvRefsToCancel:getFqtvRefsToCancel,
	getFopRefsToCancel:getFopRefsToCancel,
	//map for SSR/SVCs
	getMiscObjs: getMiscObjs,
	parseTicketObj:parseTicketObj
}