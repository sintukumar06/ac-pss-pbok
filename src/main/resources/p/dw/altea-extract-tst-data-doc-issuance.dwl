%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")

%var pnrReply= flowVars.pnrRetrieve17_1Response.PNR_Reply
%var tstData= pnrReply.tstData when commonLib.isArrayNotEmpty(pnrReply.tstData) otherwise []

%function createSegmentBucket(pnrReply)
	pnrReply.dataElementsMaster.dataElementsIndiv reduce (dataElm, acc= {
	FA: [],
	FO: []
	
}) -> {
	FA: acc.FA + dataElm when (dataElm.elementManagementData.segmentName == "FA") otherwise acc.FA,
	FO: acc.FO + dataElm when (dataElm.elementManagementData.segmentName == "FO") otherwise acc.FO
} when commonLib.isNotEmpty(pnrReply.dataElementsMaster) and commonLib.isArrayNotEmpty(pnrReply.dataElementsMaster.dataElementsIndiv) otherwise null

%var dataElementsBySegment= createSegmentBucket(pnrReply)
%var foElements= dataElementsBySegment.FO
%var faElements= dataElementsBySegment.FA
%var hasFASegments = (sizeOf (faElements)) > 0

%function getFASegmentsByTicket(faElements)
faElements groupBy ((($.otherDataFreetext[0].longFreetext splitBy "/")[0])[4..17])

%var matchedFOSegments = 
"foElements" : foElements map (foElement,indexOfFo) -> using (
	faElementsByTicket = getFASegmentsByTicket(faElements)
)
(
	foElement when faElementsByTicket[pnrReplycommonLib.getTicketNumberFromFOSegment(foElement)] != null otherwise []
)
%var matchingFO= matchedFOSegments.foElements
%var foReferenceElements= matchingFO default [] map (foElement,indexFoElem)-> using
(
	elements = pnrReplycommonLib.getRefsByQualifier(foElement,"PT","DataElm") when (foElement != null and foElement != []) otherwise []
)
(
	elements
)
%var foReferenceElementsMap= (foReferenceElements reduce (val,acc={
	elements : {}
}) -> {
	elements : acc.elements ++ val
}).elements groupBy $.number

%function getTstReferenceDataMap(pnrReplyTst)
((pnrReplyTst.referenceForTstData.reference reduce (val,acc={
	elements : {}
})->{
	elements: acc.elements + val
}).elements filter $.qualifier == "PT" map (pt,idx) -> {
	"ptMap" : pt.number
})

%function removeEmptyElements(pnrReplyObject)
pnrReplyObject filter $ != []

%function extractTstDataForReissuance(tstData)
removeEmptyElements(tstData default [] map (pnrElement,indexOfPnr)->using(
	hasElement = (getTstReferenceDataMap(pnrElement) map (h,i)->{
		"hasMap" : h.ptMap when foReferenceElementsMap[h.ptMap] != null otherwise null
	})
)
(
	pnrElement when (hasElement filter $.hasMap != null).hasMap != null otherwise []
))
---
using (
	unissuedTstData=tstData when not hasFASegments otherwise [],
	reissueTstData=extractTstDataForReissuance(tstData)
)
(unissuedTstData when commonLib.isArrayNotEmpty(unissuedTstData) otherwise (reissueTstData when commonLib.isArrayNotEmpty(reissueTstData) otherwise []))
