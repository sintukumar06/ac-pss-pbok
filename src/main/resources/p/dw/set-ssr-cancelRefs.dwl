%dw 1.0
%var pbokLib = readUrl("classpath://p/dw/pbok-common-functions.dwl")

//shorthand
%var modifyRQ = flowVars.airModRQ
%var pnrReply = flowVars.pnrRetrieve17_1Response.PNR_Reply
%var ssrOTElements = pbokLib.getDataElements(pnrReply,"OT")
%var travellers = modifyRQ.TravelerInfo.AirTraveler
//Seats
%var seatsRefToCancel = pbokLib.getSeatRefsToCancel(modifyRQ,ssrOTElements)
//SSRs
%var ssrToCancel = pbokLib.getSsrRefsToCancel(modifyRQ,ssrOTElements)
//DOCS DOCO
%var docToCancel = pbokLib.getDocRefsToCancel(travellers,ssrOTElements)
//DOCA
%var addrToCancel = pbokLib.getDocaRefsToCancel(travellers,ssrOTElements)
//PCTC
%var pctcToCancel = pbokLib.getPctcRefsToCancel(travellers,ssrOTElements)
//OSI/SK
%var otherServicesRPHToCancel = pbokLib.getOtherRPHsToCancel(modifyRQ)
//Remarks
%var remarksRPHToCancel = pbokLib.getRemarkRPHsToCancel(modifyRQ)
//FQTV
%var fqtvToCancel = pbokLib.getFqtvRefsToCancel(travellers,ssrOTElements)
//FOP cancel all FOP
%var fopToCancel = pbokLib.getFopRefsToCancel(ssrOTElements)


//Below is only for debug purpose
%var cancelRefNumbers = {
	  	fopToCancel:fopToCancel,
	  	fqtvToCancel:fqtvToCancel,
	  	remarksRPHToCancel:remarksRPHToCancel,
	  	otherServicesRPHToCancel:otherServicesRPHToCancel,
	  	pctcToCancel: pctcToCancel,
	  	addrToCancel:  addrToCancel,
	  	docToCancel: docToCancel,
	  	seatsRefToCancel: seatsRefToCancel,
	  	ssrToCancel:ssrToCancel}
%var debug = log("OTs to cancel: ",cancelRefNumbers)
%output application/java
---
seatsRefToCancel ++ ssrToCancel ++ docToCancel ++ addrToCancel ++ pctcToCancel ++ otherServicesRPHToCancel ++ remarksRPHToCancel ++ fqtvToCancel ++ fopToCancel
