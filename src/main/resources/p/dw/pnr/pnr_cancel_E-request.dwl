%dw 1.0
%output application/json
//pre:
//	flowVars.cancelRefs & flowVars.cancelQualifier must exist & setup in orchestration flow

%var cancelRefs = flowVars.cancelRefs
---
{
	"PNR_Cancel": {
		"pnrActions": {
			"optionCode":[0]
		},
		"cancelElements": [{
			"entryType": "E",
			"element": cancelRefs map {
				  identifier: flowVars.cancelQualifier,
				  number: $
				}	
		}]
	}
}