%dw 1.0

%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var constantsLib = readUrl("classpath://p/dw/common-constants.dwl")
%var originalOtaReadRequest=flowVars.originalOtaReadRequest
%var departureDate = originalOtaReadRequest.ReadRequests.AirReadRequest[0].DepartureDate
---
PNR_ListPassengersByFlight : {
	flightDateQuery: {
		flightIdentification: {
			carrierDetails: {
				marketingCarrier: originalOtaReadRequest.ReadRequests.AirReadRequest[0].Airline.Code
			},
			"flightDetails": {
				"flightNumber": originalOtaReadRequest.ReadRequests.AirReadRequest[0].FlightNumber as :number
			}
		},
		"dateIdentification" : {
			"businessSemantic": "FLD",
			dateTime: {
				year: commonLib.formatDate(departureDate,constantsLib.otaDateTimeFormat,"yyyy"),
				month: commonLib.formatDate(departureDate,constantsLib.otaDateTimeFormat,"MM"),
				day: commonLib.formatDate(departureDate,constantsLib.otaDateTimeFormat,"dd")
			}
		},
		outputSelectionOption: {
			outputType: {
				selectionDetails: {
					option: "IMG"
				}
			},
			elementType: [{
				segmentName: "SK"
			}]
		},
		searchCriteria: [ {
			primarySearchCriterion: {
				selectionDetails: {
					option: "KWD"
				}
			},
			negativeMode: {
				booleanExpression: {
					codeOperator: "NOP"
				}
			}
		}]
	}
}
