%dw 1.0
%output application/java
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var constantsLib = readUrl("classpath://p/dw/common-constants.dwl")

%var staffDateOfJoining = upper (commonLib.formatDate(flowVars.originalOtaReadRequest.ReadRequests.AirReadRequest[0].AC_StaffDetails.DateOfJoining,constantsLib.otaDateOnlyFormat, constantsLib.alteaDocDateFormat))
%var staffNumber= flowVars.originalOtaReadRequest.ReadRequests.AirReadRequest[0].AC_StaffDetails.StaffNumber
%var pnrViews= payload.PNR_ListPassengersByFlightReply.aggregatedOutput[0].pnrViews when commonLib.isArrayNotEmpty(payload.PNR_ListPassengersByFlightReply.aggregatedOutput) otherwise []

%function getValFromFreeText(freeText,matchingChar)
(
	(freeText splitBy ("/")  filter ($ startsWith (matchingChar)))
	
)


%function matchdojAndSN(dataElements)
(dataElements filter  ($.serviceRequest.ssr.type == "STFS")  map 
	using (doj=getValFromFreeText($.serviceRequest.ssr.freeText[0],'DJ-'),sn=getValFromFreeText($.serviceRequest.ssr.freeText[0],'SN-')) 
	using (sdoj= ((doj[0] splitBy ('-')) [1]) when commonLib.isArrayNotEmpty(doj) otherwise null, ssn= ((sn[0]  splitBy ('-')) [1]) when commonLib.isArrayNotEmpty(sn) otherwise null ) 
	{
	
	( matchingSNandDOJ: true) when (sdoj!=null and sdoj== staffDateOfJoining and ssn!=null and ssn== staffNumber) 
	}  filter ($ != {})
)
---

AirReservation: pnrViews filter (commonLib.isArrayNotEmpty(matchdojAndSN($.pnrView.dataElementsMaster.dataElementsIndiv))) 
 map ((eachPnrView , indexOfEachPnrView) -> 
{
	BookingReferenceID: [{
	ID: eachPnrView.pnrView.pnrHeader.reservationInfo.reservation[0].controlNumber ,
	Type: "PNR"
	
	}]
}

 )

