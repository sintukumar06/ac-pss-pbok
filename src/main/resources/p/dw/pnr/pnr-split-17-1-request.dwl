%dw 1.0
%output application/json 
skipNullOn = "everywhere"
---
{
	PNR_Split: {
		reservationInfo: {
			reservation: {
				controlNumber: flowVars.pnr
			}
		},
		splitDetails: {
			passenger: {
				type: "PT",
				 (flowVars.originalOtaAirbookModifyRequest.AirBookModifyRQ.TravelerInfo.*AirTraveler map tattoo: $.TravelerRefNumber.RPH)
			}
		}
	}
}