// As per IMD 8378070 
%dw 1.0

%output application/json skipNullOn = "everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")

%var pricingOptionKeyPublishedFares = "RP"
%var pricingOptionKeyPrivateFares = "RU"
%var pricingOptionKeyCurrencyOverride = "FCO"
%var pricingOptionKeyPointOfSaleOverride = "POS"
%var pricingOptionKeyPointOfTicketingOverride = "POT"
%var pricingOptionCorporateUnifares = "RW"
%var pricingOptionKeyCorporateNegociatedFares = "RC"
%var pricingOptionKeyPassengerDiscountPTC = "PAX"
%var pricingOptionKeyBoundInput = "BND"
%var pricingOptionFareBasisSimpleOverride = "FBA"
%var pricingOptionKeySelectivePricing = "SEL"
%var discountPenaltyDiscountInformation = "701"
%var passengerSegmentReferenceType = "P"
%var flightSegmentReferenceType = "S"
%var flightSegmentTattoo = "ST"

%var paxRPHTattooMap= flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap

%var otaOriginalAirbookRequest = flowVars.originalOtaAirbookRequest when flowVars.originalOtaAirbookRequest != null otherwise null 
%var POS = otaOriginalAirbookRequest.POS when otaOriginalAirbookRequest != null otherwise null 
%var POSSource=POS.Source[0] when commonLib.isArrayNotEmpty(POS.Source) otherwise null
%var priceInfo = otaOriginalAirbookRequest.PriceInfo when otaOriginalAirbookRequest != null otherwise null 
%var participationLevel = priceInfo.ParticipationLevel when otaOriginalAirbookRequest != null otherwise null 
%var airItin = otaOriginalAirbookRequest.AirItinerary when otaOriginalAirbookRequest != null otherwise null 
%var pricingSourceType = priceInfo.PriceRequestInformation.PricingSource when priceInfo != null otherwise null
%var ticketing=otaOriginalAirbookRequest.Ticketing[0] when (otaOriginalAirbookRequest != null and commonLib.isArrayNotEmpty(otaOriginalAirbookRequest.Ticketing)) otherwise null 
%var pnrAddMultiRS = flowVars.pnrAddMultiElements17_1SaveResponse.PNR_Reply
%var travelerInfo = otaOriginalAirbookRequest.TravelerInfo
%var allFltSegments= flatten (airItin.OriginDestinationOptions.FlightSegment) when airItin != null otherwise null
%var fareBasisCodes=allFltSegments.FareBasisCode
%var acPricingCodes = travelerInfo.AirTraveler.PassengerTypeQuantity.AC_PricingCode

	


%var buildPrivateFarePricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyPrivateFares // RU
		}
	}]) when pricingSourceType == "Both"  or pricingSourceType == "Private" otherwise [{}]

%var buildPublishedFarePricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyPublishedFares // RP
		}
	}]) when pricingSourceType == "Both"  or   pricingSourceType == "Published" otherwise [{}] 


%var buildCorporateNegociatedPricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyCorporateNegociatedFares // RC
		},
		// optionDetail has to come after pricingOptionKey as per XSD
		optionDetail: {
			criteriaDetails: [{
				attributeType: priceInfo.PriceRequestInformation.AC_PromoInformation[0].PromoCode
			}]
			}
			
	}]) when priceInfo.PriceRequestInformation.AC_PromoInformation[0].PromoCode != null and priceInfo.PriceRequestInformation.AC_PromoInformation? otherwise [{}]  //change

// Corporate Fare Pricing. Based on Account Code.
%var buildCorporateFarePricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionCorporateUnifares // RW
		},
		// optionDetail has to come after pricingOptionKey as per XSD
		optionDetail: {
			criteriaDetails: [{
				attributeType: priceInfo.PriceRequestInformation.AccountCode
			}]
		}		
	}]) when priceInfo.PriceRequestInformation.AccountCode? otherwise [{}]

%function groupFlightSegmentsByBoundByFareBasisCode ()
	airItin.OriginDestinationOptions map (
		$.FlightSegment groupBy ($.FareBasisCode when $.FareBasisCode? otherwise "NULL")
	)

	
// Get Reference Number from PNR Reply for the specfic flight segment. 
%function getSegmentReferenceNumber(fltSegment)
	(((flatten (flatten pnrAddMultiRS.*originDestinationDetails).itineraryInfo)
		filter $.elementManagementItinerary.reference.qualifier == flightSegmentTattoo and
			   $.travelProduct.boardpointDetail.cityCode == fltSegment.DepartureAirport.LocationCode and
			   $.travelProduct.offpointDetail.cityCode == fltSegment.ArrivalAirport.LocationCode and
			   $.travelProduct.companyDetail.identification  == fltSegment.MarketingAirline.Code and
			   $.travelProduct.productDetails.identification == fltSegment.FlightNumber and
			   $.travelProduct.product.depDate == commonLib.createAlteaDate(fltSegment.DepartureDateTime, null)
	)[0].elementManagementItinerary.reference.number) 
		when pnrAddMultiRS.*originDestinationDetails? otherwise null
		   	   
// Takes in FLight segments with unique FareBasisCodes
%function buildPricingOptionGroupFareBasis (origDestinations)
	(origDestinations map (origDest, idxOrigDest) -> using (fareBasisCodes=origDest - "NULL" pluck $$) (
		fareBasisCodes map (code, idxCode) -> {
			pricingOptionKey: {
				pricingOptionKey: pricingOptionFareBasisSimpleOverride //FBA
			},
			optionDetail: {
				criteriaDetails: [{
					attributeType: code
				}]
			},
			paxSegTstReference: {
				referenceDetails: (
					(origDest[code] map (fltSegment, indexFltSegment)-> using (fltSegRefNumber=getSegmentReferenceNumber(fltSegment)) {
						type: flightSegmentReferenceType, //S
						value: fltSegRefNumber as :string
								when fltSegRefNumber != null otherwise null
					} when origDest[code] != null otherwise [{}])
				) filter $ != {}
			}
						
		}
	) when commonLib.isArrayNotEmpty(origDestinations) otherwise null)
	

%function buildPaxDetails(travelers)
(
	travelers map ({
		type: "P",
		value: paxRPHTattooMap[$.TravelerRefNumber[0].RPH] as :string 
	} when paxRPHTattooMap[$.TravelerRefNumber[0].RPH] != null otherwise {}) filter $ != {}
)

%function groupTravellersByACPricingCode()
	travelerInfo.AirTraveler groupBy ($.PassengerTypeQuantity.AC_PricingCode when $.PassengerTypeQuantity.AC_PricingCode? otherwise "NULL")
	
%function buildPAXPricingOptionGroup(airTravelerGrps)
using(pricingCodes=(airTravelerGrps - "NULL" pluck $$) when commonLib.isNotEmpty(airTravelerGrps) otherwise null)
(
	pricingCodes map using (airTraveler = airTravelerGrps[$]){
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyPassengerDiscountPTC // PAX
		},
		penDisInformation: { 
			discountPenaltyQualifier: discountPenaltyDiscountInformation, // 701
			discountPenaltyDetails: [{
				rate: $
			}]
		},
		paxSegTstReference: { 
			referenceDetails: buildPaxDetails(airTraveler) 
		}		
	}
)

%function buildPricingOptionGroupFlightPass()
	w: {
		pricingOptionKey: {
			pricingOptionKey: "FLP"
		},
		optionDetail: {
			criteriaDetails: [{
				attributeType: "TYPE",
				attributeDescription: participationLevel
			}]
		}
	} when (participationLevel == "UFP" or participationLevel == "LFP" 
		 or participationLevel == "BP") otherwise {}

%function buildPaxSegTstReferenceNodeForBound(bound)
	bound.FlightSegment map (fltSeg, indexOfSegment) -> {
		type: flightSegmentReferenceType,  //S
		(value: getSegmentReferenceNumber(fltSeg) as :string) when getSegmentReferenceNumber(fltSeg) != null
	} filter ($.value?)
	
// BND pricing for all FlightPass pass types
%function buildPricingOptionGroupBoundPricing()
	 airItin.OriginDestinationOptions map (bound, indexOfBound) -> { //added to get all the elements to circumvent the function issue
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyBoundInput  // BND
		}, 
		optionDetail: {
			criteriaDetails: [{
				attributeType: (indexOfBound + 1) as :string
			}]
		},
		monetaryInformation: {
			monetaryDetails: {
				typeQualifier: "BAO",
				amount: priceInfo.ItinTotalFare[0].TotalFare.Amount as :number when priceInfo.ItinTotalFare[0].TotalFare.Amount? otherwise null,
				currency: priceInfo.ItinTotalFare[0].TotalFare.CurrencyCode
			}
		},
		paxSegTstReference: {
			referenceDetails: buildPaxSegTstReferenceNodeForBound(bound)
		}
	} when (participationLevel == "UFP" or participationLevel == "LFP" 
		 or participationLevel == "BP")  otherwise [{}]

%var buildCurrencyOverridePricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyCurrencyOverride // FCO
		},
		currency: {
			firstCurrencyDetails: {
				currencyQualifier:pricingOptionKeyCurrencyOverride,
				currencyIsoCode:priceInfo.PriceRequestInformation.CurrencyCode
				}
			}
	}]) when priceInfo.PriceRequestInformation.CurrencyCode? otherwise [{}]  

%var buildPointOfSaleOverridePricing =
	([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyPointOfSaleOverride // POS
		},
		locationInformation: {
			locationType:pricingOptionKeyPointOfSaleOverride,// POS
			firstLocationDetails:{
				code:POSSource.PseudoCityCode
		}	
		
		}	
	}]) when POSSource.PseudoCityCode? otherwise [{}] 
	
%var buildPointOfTicketingOverridePricing =([{
		pricingOptionKey: {
			pricingOptionKey: pricingOptionKeyPointOfTicketingOverride // POT
		},
		locationInformation: {
			locationType:pricingOptionKeyPointOfTicketingOverride, // POT
			firstLocationDetails:{
				code:ticketing.PseudoCityCode
		}	
		}		
	}]) when ticketing.PseudoCityCode? otherwise [{}] 

	
// Build selective pricing
%function buildSelectivePricingOption()
	wrapper: {
        pricingOptionKey: {
            pricingOptionKey: pricingOptionKeySelectivePricing  // SEL
            
        },
        paxSegTstReference: {
 
                    referenceDetails: flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap map {
					type: flightSegmentReferenceType,
					value: $ as :string}
            }                           
    } 
    when ( (flowVars.isPnrReplyWaitListedItineraryInfo == true ) and (flowVars.isFlightSegmentsWithACLowerResBookDesigCode == true) ) 
    otherwise {}


---
{
	
	Fare_PricePNRWithBookingClass: {
		pricingOptionGroup: 
			(//staticPricingOptionGroup
			buildPrivateFarePricing
			++
			buildPublishedFarePricing
			++
			buildCorporateNegociatedPricing
			++ 
			buildCorporateFarePricing
			++
			((flatten buildPricingOptionGroupFareBasis(
			 	groupFlightSegmentsByBoundByFareBasisCode ())
			) when commonLib.isArrayNotEmpty(fareBasisCodes) otherwise [{}])
			++
			buildPricingOptionGroupFlightPass()   
			++
			buildPricingOptionGroupBoundPricing()  
			++
			(buildPAXPricingOptionGroup(groupTravellersByACPricingCode()) 
				when commonLib.isArrayNotEmpty(acPricingCodes) otherwise [{}])
			++
			buildCurrencyOverridePricing
			++
			buildPointOfSaleOverridePricing
			++
			buildPointOfTicketingOverridePricing
			++
			buildSelectivePricingOption()
			) filter $ != {}
	}
 }
