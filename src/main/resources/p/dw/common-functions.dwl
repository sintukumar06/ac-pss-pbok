%dw 1.0
//Usage:  %var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
//        use the exported functions as commonLib.theFunction(...)
%function getAlteaDayOfWeek(intDay) (
	null when (intDay < 1 or intDay > 7) otherwise ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'][intDay - 1]
)

%function mapIfExists(elem)
	elem when isNotEmpty(elem) otherwise null

%function isEmpty (element)
  element == null or element == "" or ((sizeOf element) == 0)
%function isNotEmpty (element)
	not (isEmpty(element))
	
%function extractWord(element, position,splitwith)
		((element splitBy splitwith)[position])
					
%function extractAllNumbers(str)
str splitBy "" map ($ when $>=0 and $<=9 otherwise "") joinBy "" when str!=null otherwise null

%function isArrayNotEmpty (elmArray)
	elmArray != null and ((sizeOf elmArray) > 0) and elmArray[0] != "" and elmArray[0] != null
%function isArrayEmpty (elmArray)
  not (isArrayNotEmpty(elmArray))
  	
//Creates OTA time stamp
%function createOTATS (tDate, tTime)
	(tDate ++ tTime) as :localdatetime {format: "ddMMyyHHmm"} as :localdatetime as :string {format: "yyyy-MM-dd'T'HH:mm:ss"} when isNotEmpty(tDate) and isNotEmpty(tTime)
	otherwise
	 (tDate) as :localdatetime {format: "ddMMyyHHmm"} as :localdatetime as :string {format: "yyyy-MM-dd'T'HH:mm:ss"}

//Warning, avoid using 2 digits year, as all 2 digits XX will become 20XX instead of 19XX  
%function convertDateTime(input,inputFormat,outputFormat)
    input as :localdatetime {format: inputFormat} as :string {format: outputFormat}

//Warning, avoid using 2 digits year, as all 2 digits XX will become 20XX instead of 19XX  
%function convertDate(input,inputFormat,outputFormat)
    input as :date {format: inputFormat} as :string {format: outputFormat}
    
%function createAlteaDate (otaTS, otaDate)
	otaTS as :localdatetime {format: "yyyy-MM-dd'T'HH:mm:ss"} as :string {format: "ddMMyy"} when isNotEmpty(otaTS)
	otherwise
		otaDate as :date as :string {format: "ddMMyyyy"} when isNotEmpty(otaDate) otherwise null

	
%function createAlteaTime (otaTS)
	otaTS as :localdatetime {format: "yyyy-MM-dd'T'HH:mm:ss"} as :string {format: "HHmm"}	
	
%function formatAmount(amt)
	amt as :number as :string {format : ".00"}
	
%function formatAmountAsNumber(amt)	
	amt as :number as :string {format : ".00"} as :number
		
%function appendToKeyIfExists (key, appendage, delimiter, appendDelimiterIfNotExists)
using (testKey=key replace delimiter with "")
	(key ++ delimiter ++ appendage when isNotEmpty(appendage) and isNotEmpty(key) and testKey != ""
	otherwise 
		key ++ appendage when isNotEmpty(appendage) and isNotEmpty(key) and testKey == ""
		otherwise
			appendage when key != null and key == "" and isNotEmpty(appendage)
			otherwise 
				key ++ delimiter when appendDelimiterIfNotExists
				otherwise key)
		
%function buildStringWithDelimiter(arrayElms, delimiter, appendDelimiterIfNotExists)
   arrayElms reduce (obj, acc="") -> (
   	appendToKeyIfExists (acc, obj, delimiter, appendDelimiterIfNotExists)
   ) when arrayElms != null and ((sizeOf arrayElms) > 0) otherwise null

%function adjustYr(aDate)
aDate - |P100Y| when (aDate.year - now.year) > 20 
otherwise
aDate 

%function formatDate(inputDate,inputFormat,outputFormat)
(adjustYr(inputDate as :date {format:inputFormat})as :string {format:outputFormat})	
	when (isNotEmpty(inputDate) and isNotEmpty(inputFormat) and isNotEmpty(outputFormat) )
otherwise
	null

//check if srcArray contains all elements of elementAry		
%function arrayContainsAll(srcAry, elementAry)
  (sizeOf elementAry) == sizeOf (elementAry filter (srcAry contains $ )) when srcAry!=null and elementAry !=null otherwise false

// This function will return a string of Zeros for a given number of decimal places. 
// Ex: 1.23 will return "00". 1.2345 will return "0000"
// As per EPM-9600 send back only 0 for amounts w/o a decimal point. Ex: For 1234 return 0.
%function getZeroStringWithDecimalZeros (value)
   ("0." ++ replaceAllNumbersWithZeros((value splitBy ".")[1]))
   	 when value contains "." otherwise "0"

// Regex matches any decimal value: via \d is replaced.  
%function replaceAllNumbersWithZeros(value)
	value replace /(\d)/ with "0"

// This will trim all leading zeros of an integer component in a string
// to a single zero. 
// Ex:    SEL KE LON  000.00 KE SEL 00.00 NUC 0.00 END ROE0000.000000
//     -> SEL KE LON  0.00 KE SEL 0.00 NUC 0.00 END ROE0.000000
// Regex Breakdown:
// 	(?<=): Positive Lookbehind. Matches a group before the main expression 
// 		   without including it in the result.
//  (\d+): Capturing Group 1: \d+ any digit [0-9] match 1 or more (+)
//  (?=\.): (?= Positive lookahead. Matches a group after the main expression without 
//          including it in the result. 
//			\. Escaped character matches a "."
%function singleIntegerPartZeros(value)
	value replace /(?<=)(\d+)(?=\.)/ with "0"

//get decimal places from the amount, default 0
%function getDecimalPlaces(amount)
using (decimals=(amount scan /(\d+)[\.](\d*)/)[0])
(sizeOf decimals[2] when isArrayNotEmpty(decimals) and ((sizeOf decimals) > 2)
  otherwise 0)

%function addWarnings(body,warnings)
{ 
  Warnings: warnings map (warning) -> {
    Code: warning.code,
    Content: warning.message,
    (ShortText: warning.shortText) when warning.shortText?
  } 
} ++ body when isArrayNotEmpty(warnings) and body is :object
  otherwise body

%function errorsToRemark(errors,inputPrefix)
using (defaultPrefix = (inputPrefix ++ "/") when isNotEmpty(inputPrefix) otherwise "")
(errors map 
   using (prefix = ($.prefix ++ "/") when isNotEmpty($.prefix) otherwise defaultPrefix,
          message = $.message replace ("[From: " ++ $.application ++ "] " when $.application != null otherwise  "[From: ]") with "" //trim [From: application] 
   )
   (prefix ++ $.code ++ "/" ++ message)
) when isArrayNotEmpty(errors) otherwise null

---
{
	getAlteaDayOfWeek: getAlteaDayOfWeek,
	isNotEmpty: isNotEmpty,
	isEmpty: isEmpty,
	isArrayNotEmpty: isArrayNotEmpty,
	isArrayEmpty: isArrayEmpty,
	createOTATS: createOTATS,
	createAlteaDate: createAlteaDate,
	createAlteaTime: createAlteaTime,
	formatAmount: formatAmount,
	formatAmountAsNumber: formatAmountAsNumber,
	appendToKeyIfExists: appendToKeyIfExists,
	buildStringWithDelimiter: buildStringWithDelimiter,
	extractWord : extractWord,
	formatDate: formatDate,
	extractAllNumbers: extractAllNumbers,
	arrayContainsAll: arrayContainsAll,
	getZeroStringWithDecimalZeros: getZeroStringWithDecimalZeros,
	replaceAllNumbersWithZeros: replaceAllNumbersWithZeros,
	singleIntegerPartZeros: singleIntegerPartZeros,
	mapIfExists: mapIfExists,
	getDecimalPlaces:getDecimalPlaces,
	addWarnings:addWarnings,
	errorsToRemark:errorsToRemark
}