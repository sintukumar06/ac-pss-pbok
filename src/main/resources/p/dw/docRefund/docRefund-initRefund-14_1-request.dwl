%dw 1.0
%output application/json skipNullOn="everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%function isEmptyTicketNumber (element)
element when commonLib.isNotEmpty(element) otherwise ""
%var hasZeroAmount = flowVars.ticketObj.price == 0
%var refundType = flowVars.originalOtaCancelRequest.UniqueID.RefundType 
%var indicator = "ATC" when not (["ETS","FP"] contains flowVars.clientAppId) 
					otherwise "NUL" when hasZeroAmount
						otherwise "RTF" when refundType == "retain"
							otherwise null
%var actionDetails = {
	statusDetails:{
		indicator: indicator
	} when indicator !=null otherwise null
	
	,otherDetails: {
	  	indicator: "RTF"
	} when indicator == null and refundType == "retain" otherwise null
} 

---
DocRefund_InitRefund:

{
	ticketNumberGroup:{
		documentNumberDetails:{
			documentDetails:{				
				number: isEmptyTicketNumber(flowVars.ticketObj.ticketNbr)
			}
		}
	},
	(actionDetails: actionDetails) when actionDetails.statusDetails != null 
}