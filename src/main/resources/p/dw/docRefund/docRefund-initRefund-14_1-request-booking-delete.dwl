%dw 1.0
%output application/json skipNullOn="everything"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%function isEmptyTicketNumber (element)
element when commonLib.isNotEmpty(element) otherwise ""
---
DocRefund_InitRefund:
{
	ticketNumberGroup:{
		documentNumberDetails:{
			documentDetails:{				
				number: isEmptyTicketNumber(commonLib.extractAllNumbers(commonLib.extractWord(flowVars.longTktNumber,0,"/")))																		 
			}
			}
		},
		(actionDetails:{
			statusDetails:{
				indicator: "ATC"
			}
			 
		}) when flowVars.clientAppId != "ETS" and flowVars.clientAppId != "FP",
		(actionDetails:{
			statusDetails:{
				indicator: "NUL"
			}
			 
		}) when commonLib.extractAllNumbers(commonLib.extractWord(flowVars.longTktNumber,2,"/")) == "000",
		(actionDetails:{
			(otherDetails:{
				indicator: "RTF"
			}) when ((flowVars.clientAppId != "ETS" and flowVars.clientAppId != "FP") or commonLib.extractAllNumbers(commonLib.extractWord(flowVars.longTktNumber,2,"/")) == "000"),
			(statusDetails:{
				indicator: "RTF"
			}) when ((flowVars.clientAppId == "ETS" or flowVars.clientAppId == "FP") or commonLib.extractAllNumbers(commonLib.extractWord(flowVars.longTktNumber,2,"/")) != "000")
			 
		}) when (lower flowVars.pnrRefundType) == "retain"
}