%dw 1.0
%output application/json skipNullOn = "everywhere"
---
{
	
		Ticket_CreateTSTFromPricing:( {
		psaList: flowVars.farePricePNRWithBookingClass18_1Response.Fare_PricePNRWithBookingClassReply.fareList
		map ((fareList , indexOfFareList) -> {
			itemReference: {
				referenceType: fareList.fareReference.referenceType,
				uniqueReference: fareList.fareReference.uniqueReference
			} 
		} when fareList.fareReference.referenceType == "TST"
		otherwise
			null
		)
	}  
	
	when flowVars.farePricePNRWithBookingClass18_1Response.Fare_PricePNRWithBookingClassReply.fareList != null 
	otherwise 
	null)
}