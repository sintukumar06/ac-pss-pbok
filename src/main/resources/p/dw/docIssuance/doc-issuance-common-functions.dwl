%dw 1.0
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%function originDestDetailsByRSVR(pnrRetrieveOriginDestDet)
"PNR_RSVR": pnrRetrieveOriginDestDet map (origin,indexOfOrigin)->{
	(origin.itineraryInfo map (itineraryInfo,indexOfitineraryInfo)->{
		(itineraryInfo default [] filter (itineraryInfo.travelProduct.productDetails.identification == "RSVR"))
	} filter $ != {
	})
} filter $ != {}

%function buildTST(tstData) 
[referenceDetails : tstData map {
	type: "TS",
	value: $.tstGeneralInformation.generalInformation.tstReferenceNumber
}] when tstData != null and tstData != []
	  	otherwise []
	  	
%function buildTSM(emdData) 
[referenceDetails: emdData map ((unissuedEMD,indexOfUnissuedEMD) ->  {
	type: "TMT",
	value: unissuedEMD.tattooAndTypeOfTSM.uniqueReference
})] when emdData  != null and emdData != []
	  	otherwise []
	  	
%function hasRSVRElement(rsvrObject)
 false when rsvrObject.PNR_RSVR == null or rsvrObject.PNR_RSVR == [] otherwise 
 (sizeOf (rsvrObject.PNR_RSVR)) > 0

%function buildSwitch(callingFunction)
statusDetails: {
	indicator: "TRP" when (callingFunction == "C" or callingFunction == "M")
	otherwise "ITR" when callingFunction == "T" otherwise null
}

%var buildEMPRA =
[{
	criteriaDetails: {
		attributeType: "EMPRA"
	}
}]

%function buildSubCompoundOptionsForEmail(acEmail)
[{
	criteriaDetails: {
		attributeType: "EMPR" when commonLib.isNotEmpty(acEmail.Content) otherwise null,
		(attributeDescription: acEmail.Content) when (sizeOf acEmail) > 0 and commonLib.isNotEmpty(acEmail.Content)
	}
}]

%function buildSubCompoundOptions(acEmails)
buildEMPRA when commonLib.isEmpty(acEmails) otherwise
		(acEmails map (acEmail,indexOfAcEmail)->{
			(buildSubCompoundOptionsForEmail(acEmail))
		}) when commonLib.isArrayNotEmpty(acEmails) otherwise null

%function buildOptionGroupForEmail(acEmails,callingFunction) 
[{
	switches: buildSwitch(callingFunction),
	subCompoundOptions: buildSubCompoundOptions(acEmails)
}]

%function buildLanguageAttribute(langID)
[{
	attributeDetails: {
		attributeType: "LA",
		attributeDescription: langID
	}
}] 

%function buildETCAttribute()
[{
	attributeDetails: {
		attributeType: "ETC",
		attributeDescription: "AC"
	}
}]

%function buildOtherCompoundOptions(langID,alteaEmail,officeId)
	(buildLanguageAttribute(langID) when alteaEmail != null and alteaEmail == true and langID != null otherwise []) 
	++ 
	(buildETCAttribute() when (officeId != null and (officeId matches /^.{3}AC2.{3}/)) otherwise [])
---
{
	originDestDetailsByRSVR: originDestDetailsByRSVR,
	hasRSVRElement: hasRSVRElement,
	buildOptionGroupForEmail: buildOptionGroupForEmail,
	buildTST: buildTST,
	buildTSM: buildTSM,
	buildOtherCompoundOptions: buildOtherCompoundOptions
}