%dw 1.0
%output application/json skipNullOn = "everywhere"

%var docIssuanceLib = readUrl("classpath://p/dw/docIssuance/doc-issuance-common-functions.dwl")
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var acEmails=flowVars.originalOtaAirbookRequest.AC_Emails 
when commonLib.isArrayNotEmpty(flowVars.originalOtaAirbookRequest.AC_Emails) otherwise []
%var langID=flowVars.originalOtaAirbookRequest.LangID 
when commonLib.isNotEmpty(flowVars.originalOtaAirbookRequest.LangID) otherwise null
%var alteaEmail=flowVars.alteaEmail when flowVars.alteaEmail? otherwise null
%var officeId=flowVars.office-id
%var pnrRetrieveOriginDestDet = flowVars.pnrRetrieve17_1Response.PNR_Reply.originDestinationDetails
when commonLib.isArrayNotEmpty(flowVars.pnrRetrieve17_1Response.PNR_Reply.originDestinationDetails) otherwise []
%var emdData = flowVars.unissuedEMD
%var tstData = flowVars.tstData when commonLib.isArrayNotEmpty(flowVars.tstData) otherwise []

%var buildSelection=
docIssuanceLib.buildTST(tstData) ++
docIssuanceLib.buildTSM(emdData)

%var buildARV = 
[switches: {
	statusDetails: {
		indicator: "ARV"
	}
}] when docIssuanceLib.hasRSVRElement(docIssuanceLib.originDestDetailsByRSVR(pnrRetrieveOriginDestDet)) otherwise []

%var buildRT = 
[switches: {
	statusDetails: {
		indicator: "RT"
	}
}] when docIssuanceLib.hasRSVRElement(docIssuanceLib.originDestDetailsByRSVR(pnrRetrieveOriginDestDet)) otherwise []

%var buildOptionGroup =
	(docIssuanceLib.buildOptionGroupForEmail(acEmails,"C") when (alteaEmail != null and alteaEmail == true) otherwise []) ++ 
	buildARV ++ 
	buildRT

%var buildOtherCompoundOptions =
docIssuanceLib.buildOtherCompoundOptions(langID,alteaEmail,officeId) when docIssuanceLib.buildOtherCompoundOptions(langID,alteaEmail,officeId) != [] otherwise []
---
{
	DocIssuance_IssueCombined : {
		selection: buildSelection when buildSelection != [] otherwise null,
		optionGroup: buildOptionGroup when buildOptionGroup != [] otherwise null,
		otherCompoundOptions: buildOtherCompoundOptions	when buildOtherCompoundOptions != [] otherwise null
	}
}