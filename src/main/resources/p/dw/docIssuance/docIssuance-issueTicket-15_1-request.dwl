%dw 1.0
%output application/json skipNullOn="everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var docIssuanceLib = readUrl("classpath://p/dw/docIssuance/doc-issuance-common-functions.dwl")
%var acEmails=flowVars.originalOtaAirbookRequest.AC_Emails 
when commonLib.isArrayNotEmpty(flowVars.originalOtaAirbookRequest.AC_Emails) otherwise []
%var langID=flowVars.originalOtaAirbookRequest.LangID
when commonLib.isNotEmpty(flowVars.originalOtaAirbookRequest.LangID) otherwise null
%var alteaEmail=flowVars.alteaEmail when flowVars.alteaEmail? otherwise null
%var officeId=flowVars.office-id
%var tstData = flowVars.tstData when commonLib.isArrayNotEmpty(flowVars.tstData) otherwise []


%var buildSelection =
docIssuanceLib.buildTST(tstData)

%var buildOptionGroup =
(docIssuanceLib.buildOptionGroupForEmail(acEmails,"T") when (alteaEmail != null and alteaEmail == true) otherwise [])

%var buildOtherCompoundOptions =
docIssuanceLib.buildOtherCompoundOptions(langID,alteaEmail,officeId) when docIssuanceLib.buildOtherCompoundOptions(langID,alteaEmail,officeId) != [] otherwise []
---
{
	DocIssuance_IssueTicket: {
	  	selection: buildSelection when buildSelection != [] otherwise null,
		optionGroup: buildOptionGroup when buildOptionGroup != [] otherwise null,
		otherCompoundOptions: buildOtherCompoundOptions when buildOtherCompoundOptions != [] otherwise null
	}
}
