%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%function parseDate(date) date as :string {format: "ddMMyy"}
%var paxRPHTattooMap = flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap when flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap != null otherwise null

%var miscObjectsMap = flowVars.otaAirbookMaps.miscObjectsMap when flowVars.otaAirbookMaps.miscObjectsMap != null otherwise null
%var svcs = miscObjectsMap.SVCs when miscObjectsMap != null and commonLib.isArrayNotEmpty(miscObjectsMap.SVCs) otherwise null

%function getSpecialServiceRequests(travelerInfo)
flatten (travelerInfo.SpecialReqDetails.SpecialServiceRequests  
							when (commonLib.isArrayNotEmpty(travelerInfo.SpecialReqDetails) 
								and commonLib.isArrayNotEmpty(travelerInfo.SpecialReqDetails.SpecialServiceRequests))
							otherwise [])
	
%function mapAuxDetails(svc)
{
    		"manualAuxiliaryType" : {
    			"travelSector" : "SVC"
    		},
    		"travelProduct": {
    			("flightDate": {
    				"departureDate": parseDate(svc.FlightLeg.Date as :date) as :number
    			}) when svc.FlightLeg.Date?,
    			("boardPointDetails": {
    				"trueLocationId": svc.FlightLeg.DepartureAirport.LocationCode
    			}) when svc.FlightLeg.DepartureAirport.LocationCode?,
    			("offpointDetails": {
    				"trueLocationId": svc.FlightLeg.ArrivalAirport.LocationCode
    			}) when svc.FlightLeg.ArrivalAirport.LocationCode?,
    			"companyDetails": {
    				"marketingCompany": svc.Airline.Code when svc.Airline.Code? otherwise svc.Airline.CompanyShortName
    			}
    		},
    		"relatedProduct": {
    			"quantity": 1,
    			"statusCode": "NN"
    		},
    		"service": {
    			"specialRequirementsInfo": {
    				"ssrCode": svc.SSRCode!
    			}
    		},
    		"freetextItinerary": {
    			"freeTextDetails": {
    				"textSubjectQualifier": "3",
    				"source": "M",
    				"encoding": "2"
    			},
    			"freeText" : (svc.Text) when svc.Text? otherwise ""
    		},
    		"passengerAssociation": {
    			"passengerReference": [{
    				"type": "PT",
    				"value": paxRPHTattooMap[svc.TravelerRefNumberRPHList[0]!] as :string when paxRPHTattooMap[svc.TravelerRefNumberRPHList[0]!] != null otherwise null
    			}]
    		}
    	}
---
{
  "PNR_CreateAuxiliarySegment": {
  		"manualAuxDetails": 
  						svcs map ((mapAuxDetails($))) when svcs != null 
  						otherwise 
  						using (ssrs=getSpecialServiceRequests(payload.TravelerInfo))
  						(
  							ssrs map (
    								(mapAuxDetails($)) when $.AC_Type == "SVC" otherwise null
    							) when ssrs != [] otherwise null 
  						) 					
  }
}