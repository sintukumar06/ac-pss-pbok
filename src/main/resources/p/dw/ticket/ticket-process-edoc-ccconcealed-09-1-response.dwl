%dw 1.0
%output application/json skipNullOn = "everywhere"

---
//Requires refactoring for stories of epic EPM-11186
{
TicketDocumentDetails: flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.docGroup map (vDocGroup, kDocGroup) ->//QUICKFIX
	{
		(vDocGroup.docDetailsGroup map (vDocDetailsGroup, kDocDetailsGroup) -> {
			TicketNumber: vDocDetailsGroup.docInfo.documentDetails.number as :number,
			AirReservation: {
				"BookingReferenceID": (vDocGroup.referenceInfo.reservation filter ($.companyId == "AC") map (vReservation, kReservation) -> {
						"ID": vReservation.controlNumber,
						"CompanyName": {
							"Code": vReservation.companyId
						}
				}),
				"Ticketing": {
					"IATANumber": vDocGroup.sysProvider.bookingIataNumber,
					//(vDocGroup.sysProvider.internalIdDetails map {//QUICKFIX
						"PseudoCityCode": vDocGroup.sysProvider.internalIdDetails.inhouseId[0]
					//})
				},
			 "EMD_Info":vDocGroup.numberOfUnits.quantityDetails map (vQuantityDetails, vQuantityDetails) -> {
				"TotalFltSegQty": vDocGroup.numberOfUnit when vQuantityDetails.unitQualifier == "TF" otherwise null,
				(flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.generalDocGroup map {
					"EMD_Type": $.docInfo.documentDetails.type,
					"AgentID": [
						"ID": $.originatorInfo.originator
					]
				} when flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.generalDocGroup != null otherwise []),
				"PaymentDetail": flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.fop.formOfPayment map {
					"PaymentCard": {
						"CardCode": $.vendorCode,
						(vDocGroup.fop.formOfPayment map {
							"CardNumber":{
							 	"PlainText": $.creditCardNumber
							 	//QUICKFIX
							 }
						})
					},
					"PaymentAmount": vDocGroup.fop.formOfPayment map {
						"ApprovalCode": $.approvalCode
					}
				} when flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.fop.formOfPayment != null otherwise [],
				"OriginDestination": {//QUICKFIX
					"OriginCityCode": vDocGroup.docDetailsGroup[0].couponGroup[0].leg[0].boardPointDetails.trueLocationId,
					"DestinationCityCode": vDocGroup.docDetailsGroup[0].couponGroup[-1].leg[-1].offpointDetails.trueLocationId
				},
				(flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.textInfo map {
					"Endorsement": {
						"EndorsementInfo": $.freeText when $.freeTextQualification.informationType == "10" otherwise ""							
					}
				} when flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.textInfo != null otherwise []),
				 (vDocGroup.fareInfo.monetaryDetails map {//QUICKFIX
					("BaseFare": [{
						"Amount": $.amount as :number,//QUICKFIX
						"CurrencyCode": $.currency,
						"DecimalPlaces": 2
					 }]) when $.typeQualifier == "B"
				}),
				(vDocGroup.fareInfo.monetaryDetails map {//QUICKFIX
					("TotalFare": [{
						"Amount": $.amount as :number,//QUICKFIX
						"CurrencyCode": $.currency,
						"DecimalPlaces": 2
					}]) when $.typeQualifier == "T"
				}),
					"Taxes": {
						"Tax": vDocGroup.taxInfo[0].taxDetails map { //QUICKFIX
							//($.taxDetails map {
								//($.type map {
									"TaxCode": $."type"[0],//QUICKFIX
								//}),
								"Amount": $.rate as :number
							//})
						} when vDocGroup.taxInfo != null otherwise []
					},
					"UnstructuredFareCalc": flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.textInfo map (vTextInfo) -> {
						(vTextInfo.freeText map {
							"Info": $ when vTextInfo.freeTextQualification.informationType == "15" otherwise null
						})
					} when flowVars.ticketProcessEDocCCConcealed9_1Response.Ticket_ProcessEDocCCConcealedReply.textInfo != null otherwise [],
					"TicketDocument": [{
						"TicketDocumentNbr": vDocDetailsGroup.docInfo.documentDetails.number,
						"Type": "Ticket" when vDocDetailsGroup.docInfo.documentDetails.type == "T" otherwise 
						        "EMD" when vDocDetailsGroup.docInfo.documentDetails.type == "J" otherwise 
						        "EMD" when vDocDetailsGroup.docInfo.documentDetails.type == "Y" otherwise null, //Change to EMD-A(J) and EMD-S(Y) after RAML updates
						//"DateOfIssue": flowVars.PNR_Retrieve_Response.PNR_Reply.securityInformation.secondRpInformation.creationDate as :date { format: "ddMMyy" } as :string{format: "yyyy-MM-dd"} , //QuickFix
						//"TimeOfIssue": flowVars.PNR_Retrieve_Response.PNR_Reply.securityInformation.secondRpInformation.creationTime as :localtime { format: "HHmm"} as :string {format: "HH:mm:ss"}, //QuickFix
						"CouponInfo": vDocDetailsGroup.couponGroup map (vCouponGroup) -> {
							(vCouponGroup.couponInfo.couponDetails map {//Should contain departure arrival for each coupon
								"Number": $.cpnNumber as :number, //QUICKFIX
								"CouponReference": $.cpnInConnectionWith,
								"FareBasisCode": vCouponGroup.pricingInfo.fareBasisDetails.rateTariffClass, //QUICKFIX
								"Status": $.cpnStatus,
								"CouponItinerarySeqNbr": $.cpnSequenceNumber,
								(vCouponGroup.validityDates.dateAndTimeDetails map {//QUICKFIX
									"DateOfService": $.date as :date {format: "ddMMyy"} as :string {format: "yyyy-MM-dd"} when $.qualifier == "B" otherwise null//QUICKFIX
								} when vCouponGroup.validityDates.dateAndTimeDetails != null otherwise []),//QUICKFIX
								(vCouponGroup.leg map {
									"SoldFlightSegmentRPH": $.itemNumber as :string //QUICKFIX
								} when vCouponGroup.leg != null otherwise []),
								"ValidatingAirline": {
									"Code": vCouponGroup.originatorInfo.deliveringSystem.companyId //Not found
								}
							})
						}
				}],
				"FareComponent": {
					"PriceableUnit": [{
						"Number": "1",
						"FareComponentDetail": vDocGroup.docDetailsGroup.couponGroup..leg map {//QuickFix
							"Number":$$ +1,
							CouponSequence: [OriginCityCode: $.boardPointDetails.trueLocationId[0]]
						}
					}],
					(vDocGroup.fareInfo.monetaryDetails map {
						("TotalConstructionAmount": [{//QUICKFIX
							"Purpose": "Base",
							"Amount": $.amount as :number,//QUICKFIX
							"CurrencyCode": $.currency //QUICKFIX
						}]) when $.typeQualifier == "B"
					})
				  }
				}
			}
		})
	}
}