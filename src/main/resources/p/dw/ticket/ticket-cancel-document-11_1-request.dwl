%dw 1.0
%output application/json 
//%var stockProviderCodeAnyAirline = "ZZZ"
%var pnrReply = flowVars.pnrRetrieve17_1Response.PNR_Reply
%var stockProviderCode = pnrReply.originDestinationDetails[0].itineraryInfo[0].travelProduct.companyDetail.identification
---
{
	Ticket_CancelDocument: {
		documentNumberDetails: {
			documentDetails: {
				// Sample input to parse: PAX 014-2100023570/ETAC/CAD193.30/07JUN18/YULAC0980/00100052
				number: flowVars.ticketObj.ticketNbr //0142100023570
			}
		},
		stockProviderDetails: {
			officeSettingsDetails: {
				stockProviderCode: stockProviderCode
			}
		},
		targetOfficeDetails :{
			originatorDetails: {
				inHouseIdentification2:flowVars.office-id
			}
		}
	}
} 