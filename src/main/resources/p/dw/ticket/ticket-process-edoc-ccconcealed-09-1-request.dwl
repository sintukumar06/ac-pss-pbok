%dw 1.0
%output application/json
%var tickets = payload
---
using (firstTicket = tickets[0])
{
	//Multiple tickets in request and response to be handled : EPM-11518
	Ticket_ProcessEDocCCConcealed: {
		msgActionDetails: {
			messageFunctionDetails: {
				
				messageFunction: "131" when (firstTicket.TicketType == "eTicket") 
						otherwise "791" when (firstTicket.TicketType == "EMD") 
						otherwise firstTicket.TicketType
				
				//additionalMessageFunction: (payload[1 to (sizeOf payload) - 1].TicketType) 
				//map (
					//("131" when ($ == "eTicket") otherwise "791" when ($ == "EMD") otherwise $)
				//)
			}
		},
		infoGroup: tickets map {
			docInfo: {
				documentDetails: {
					number: $.AC_TicketNumber
				}
			}
		}
	}
}
