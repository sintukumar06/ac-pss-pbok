%dw 1.0
%output application/json
skipNullOn="everywhere"
%var inType = inboundProperties."http.query.params".type when inboundProperties."http.query.params".type? 
			otherwise flowVars.retrieveType when flowVars.retrieveType != null otherwise null
---
PNR_Retrieve: {
	retrievalFacts: {
		retrieve: {
			type: inType as :number when inType != null otherwise 2
		},
		reservationOrProfileIdentifier: {
			reservation: [{
				//flowVars.pnr is populated automatically by mulesoft when {pnr} is in the http resource
				controlNumber: flowVars.pnr 
					when flowVars.pnr != null otherwise flowVars.pnrToRetrieve
			}] 
		} when (flowVars.pnr != null or flowVars.pnrToRetrieve != null ) otherwise null,
		(personalFacts: {
	        travellerInformation: {
	          traveller: {
	            surname: inboundProperties."http.query.params".surname
	          }
	        }
	    }) when inboundProperties."http.query.params".surname?
	}
}