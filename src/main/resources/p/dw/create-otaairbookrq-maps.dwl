%dw 1.0
%output application/json

// OTA request has RPH for air and pox. However, Altea creates different tattoos for air and pax segments. So there is a need to correlate these identifiers when adding APIS or SSRs. 
// This code creates a map for air and pax segment RPHs. This will be used to create another map of OTA RPHs to Altea RPHs for air and pax. This new map will be used to add APIS and SSRs and FQTVs.
// This also creates a flight segment to originDestination RPH map to map the flight segments in altea response to respective segments.

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")
%var pbokLib = readUrl("classpath://p/dw/pbok-common-functions.dwl")

%var airbookRQ =  flowVars.originalOtaAirbookRequest

// DepatureAirport + DepartureDT + ArrivalAirport + ArrivalDT + MarketingAirline + FlightNumber
%function createFlightSegmentKey (flightSegment)
using (
	delim="_"
)
commonLib.buildStringWithDelimiter([
	flightSegment.DepartureAirport.LocationCode!, flightSegment.DepartureDateTime!,
	flightSegment.ArrivalAirport.LocationCode!, flightSegment.ArrivalDateTime!,
	flightSegment.MarketingAirline.Code!, flightSegment.FlightNumber!
	],  delim, true)
	
// surname + givenname (in altea form) + ptc
%function createPaxKey (airTraveler)
using (
	delim="_"
)
commonLib.buildStringWithDelimiter([
	airTraveler.PersonName.Surname!, 
	pnrAddMultiLib.buildAlteaGivenName(airTraveler.PersonName.GivenName[0]!, airTraveler.PersonName.NamePrefix[0], airTraveler.PersonName.NameSuffix[0]),
	airTraveler.PassengerTypeCode!
	],  delim, true)

//Altea returns flightSegments within a single origDestDetails. As part of booking process, the response needs separate the flightsegment across bounds.
//Creates an origDestMap with the key and RPH
// key is formed from depAirportCode, depDT, arrAirportCode, arrDT, marketingAirlineCode and FlightNumber
%var flightSegments = airbookRQ.AirItinerary.OriginDestinationOptions map (origDestOption, idxOrigDestOption) -> (
	(origDestOption.FlightSegment map using (key=createFlightSegmentKey($)){
		("OrigDest_" ++ key): origDestOption.RPH!,
		("FltSegment_" ++ key): $.RPH!
	})
) when commonLib.isNotEmpty(airbookRQ.AirItinerary)  and commonLib.isArrayNotEmpty(airbookRQ.AirItinerary.OriginDestinationOptions) otherwise null

%var fltSegmentMap = (flatten flightSegments) reduce (obj, acc) -> {
	 (acc + obj)
} when commonLib.isArrayNotEmpty(flightSegments) otherwise null


%var paxData = airbookRQ.TravelerInfo.AirTraveler map  using (key=createPaxKey($)) {
	(upper key): $.TravelerRefNumber.RPH!
} when airbookRQ.TravelerInfo? otherwise null

%var paxDataMap = (flatten paxData reduce (obj, acc) -> {
	(acc + obj)
}) when paxData != null otherwise null
			

%var miscObjs = pbokLib.getMiscObjs(airbookRQ)
---
{
	fltSegmentMap: fltSegmentMap,
	paxRPHMap: paxDataMap,
	miscObjectsMap: miscObjs
}