%dw 1.0
%output application/json skipNullOn="everywhere"
%var body = flowVars.lastPayload when flowVars.lastPayload != null otherwise payload
%var itinTotalFare = body.AirReservation[0].PriceInfo.ItinTotalFare[0]
%var otaACDiscounts = flowVars.otaACDiscounts
%var newItinTotalFare = itinTotalFare ++ otaACDiscounts when itinTotalFare!=null and otaACDiscounts!=null and otaACDiscounts != {} otherwise null
%var newOriginDest = flowVars.newOriginDest
%function addNewItinTotalFare(res,newItinTotalFare)
{
  AirReservation: [
    res.AirReservation[0] - "PriceInfo" ++
    		PriceInfo:res.AirReservation[0].PriceInfo
    				- "ItinTotalFare" ++
    				ItinTotalFare: [newItinTotalFare]
  ]
} when newItinTotalFare !=null
  otherwise res
%function addNewOriginDest(res,newOriginDest)
{
  AirReservation: [
    res.AirReservation[0] - "AirItinerary" ++
    		"AirItinerary": res.AirReservation[0].AirItinerary
    					- "OriginDestinationOptions" ++
    					"OriginDestinationOptions":newOriginDest
  ]
} when newOriginDest !=null
  otherwise res

---
addNewItinTotalFare(
  addNewOriginDest(body,newOriginDest),
  newItinTotalFare
)