%dw 1.0
%output application/json skipNullOn="everywhere"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%var paymentDetailsNodes= payload.Fulfillment.PaymentDetails when payload.Fulfillment.PaymentDetails? otherwise []
%var splitPayment = true when (commonLib.isArrayNotEmpty(paymentDetailsNodes) and paymentDetailsNodes[0].PaymentTransactionTypeCode == "LAS") otherwise false
%var paymentBuildFOPDetails =[paymentDetailsNodes[0]]  when splitPayment otherwise paymentDetailsNodes //For Non-Limited amount split(LAS)- We need to build Multiple fop elements 
%var paymentBuildMOPDetails = paymentDetailsNodes when splitPayment otherwise [] //For Limited amount split(LAS)- We need to build 1fop with Multiple mop element 

%var isSingleFOP=true when ((sizeOf paymentDetailsNodes)==1) otherwise false
								
%var isBookingModifyRequest=true when flowVars.originalOtaAirbookModifyRequest != null otherwise false

%function isPseudoPayment(paymentDetail)
	paymentDetail.Remark match {
		:null -> false,
		/FLIGHTPASS.*/ -> true,
		/STAFFTRAVEL.*/ -> true,		
		default -> false		
	} 

%var fraudAction = "N" when ( commonLib.isEmpty(payload.IPAddress) or commonLib.isEmpty(payload.DFP))
						otherwise "Y" //This may need to be corrected in future					

%var productPricingQuotationRecordList=flowVars.pnrAddMultiElements17_1SaveResponse.PNR_Reply.pricingRecordGroup.productPricingQuotationRecord when flowVars.pnrAddMultiElements17_1SaveResponse != null 
											otherwise flowVars.pnrRetrieve17_1Response.PNR_Reply.pricingRecordGroup.productPricingQuotationRecord when flowVars.pnrRetrieve17_1Response != null otherwise null

%function getRefValueFromPnrAddMultiElements(pricingRecordGroup)
	((pricingRecordGroup.pricingRecordId.uniqueReference as :string when pricingRecordGroup.pricingRecordId.uniqueReference != null otherwise null)  when pricingRecordGroup.pricingRecordId.referenceType=='TST' otherwise null)	

%var ticketRetriveListOfTSM = flowVars.ticketRetrieveListOfTSMResponse_15_1 when flowVars.ticketRetrieveListOfTSMResponse_15_1 !=null otherwise null
%var ticketRetrieveList=[] when ticketRetriveListOfTSM.Ticket_RetrieveListOfTSMReply == null 
						otherwise ( [] when ticketRetriveListOfTSM.Ticket_RetrieveListOfTSMReply.detailsOfRetrievedTSMs== null 
							otherwise ticketRetriveListOfTSM.Ticket_RetrieveListOfTSMReply.detailsOfRetrievedTSMs
						)
%function getRefValueFromTicketRetrieveListOfTSM(detailsofRetrievedTSM)
	(detailsofRetrievedTSM.tattooAndTypeOfTSM.uniqueReference as :string when detailsofRetrievedTSM.tattooAndTypeOfTSM.uniqueReference != null otherwise null )	
	
%var fareList=flowVars.ticketRepricePNRWithBookingClass16_1Response.Ticket_RepricePNRWithBookingClassReply.ticketGroup.fareList when flowVars.ticketRepricePNRWithBookingClass16_1Response !=null otherwise null

%var ticketReissuePricing = flowVars.ticketReissueConfirmedPricing13_2Response when flowVars.ticketReissueConfirmedPricing13_2Response != null otherwise null
%var createTSTPricing=flowVars.createTSTFromPricing_04_1Response when flowVars.createTSTFromPricing_04_1Response != null otherwise null
%var tstInformationList = ticketReissuePricing.Ticket_ReissueConfirmedPricingReply.tstInformation filter $.tstReference.referenceType == "TST" when  ticketReissuePricing.Ticket_ReissueConfirmedPricingReply.tstInformation !=null otherwise null 
%var createTSTPricingList = createTSTPricing.Ticket_CreateTSTFromPricingReply.tstList when  createTSTPricing.Ticket_CreateTSTFromPricingReply.tstList !=null otherwise null 

%var isValidTicketReissueConfirmedPricing = fareList   map  {
	($.automaticReissueInfo.firstDpiGroup.reissueInfo.otherMonetaryDetails filter ($.amount != 0 and $.typeQualifier=='TAC')  map {
		item:$.amount
	} when $.automaticReissueInfo.firstDpiGroup.reissueInfo.otherMonetaryDetails !=null otherwise [] ) 
} filter ($ != {}) when fareList !=null otherwise []


%function hasUnissuedIndicators(tsmIndicators)
	(false when tsmIndicators == null or tsmIndicators == [] otherwise 
		(sizeOf (tsmIndicators filter $.statusDetails.indicator == "ISD" 
 			and $.statusDetails.action == "0")) > 0
	 )

%function pnrElementAssociationForCreateBookingRequest(paymentDetail)
(
		(productPricingQuotationRecordList map {
						referenceDetails: {
							type: "TST",
							value:getRefValueFromPnrAddMultiElements($)
						}
				} when productPricingQuotationRecordList!=null and paymentDetail.PaymentTransactionTypeCode=="Air"  
	otherwise ticketRetrieveList   map {
					referenceDetails: {
						type: "TSM",
						value:getRefValueFromTicketRetrieveListOfTSM($)
					}
				} when  paymentDetail.PaymentTransactionTypeCode=="Ancillary" 
	otherwise null) when (sizeOf paymentDetailsNodes)>1 otherwise null
)

%function pnrElementAssociationForModifyBookingRequest(paymentDetail)
( 	
flatten {
		tstMapping:(tstInformationList  map {
						referenceDetails: {
							type: "TST",
							value:$.tstReference.uniqueReference as :string when $.tstReference.uniqueReference != null otherwise null
							
							
					}
		} when tstInformationList != null   and (sizeOf isValidTicketReissueConfirmedPricing )>0 
		otherwise (createTSTPricingList map {
			referenceDetails: {
								type: "TST",
								value:$.tstReference.uniqueReference as :string when $.tstReference.uniqueReference !=null otherwise null
			}
		} when createTSTPricingList !=null
		otherwise null))  when (paymentDetail.PaymentTransactionTypeCode == "Air" or  isSingleFOP  ) otherwise null,
		tsmMapping: (ticketRetrieveList  filter hasUnissuedIndicators($.tsmIndicators)  map {
					referenceDetails: {
						type: "TSM",
						value:getRefValueFromTicketRetrieveListOfTSM($)
					}
		  } when ( (paymentDetail.PaymentTransactionTypeCode=="Ancillary" or isSingleFOP)) otherwise null)
	}
)

%function bestEffortFormation()(
	[{
            statusInformation:[{
                indicator: "ALL"
            }]
        }]  when (sizeOf paymentDetailsNodes)>1 and isBookingModifyRequest == false 
	otherwise [{
            statusInformation:[{
                indicator: "ALL"
            }]
        }]  when isBookingModifyRequest
       otherwise null
)
								
---
FOP_CreateFormOfPayment: {
		transactionContext: {
			transactionDetails: {
				code: "DEF"
				}                                                                                                         
		},
		bestEffort:bestEffortFormation(),
    		fopGroup: (paymentBuildFOPDetails map ((paymentFopDetail, indOfPaymentDetail) -> using(mopDetails = paymentBuildMOPDetails  when splitPayment otherwise [paymentFopDetail])
	    	 {
	    	 		fopReference: {},
	    	 		(pnrElementAssociation:pnrElementAssociationForModifyBookingRequest(paymentFopDetail) when isBookingModifyRequest 
											otherwise pnrElementAssociationForCreateBookingRequest(paymentFopDetail)) when  not splitPayment  , 
	 			mopDescription: mopDetails  map ((paymentDetail, indexOfPaymentDetail) ->  using (
			    			cashPayment= true when (paymentDetail.Cash? and paymentDetail.Cash.CashIndicator? and 
			    								(paymentDetail.Cash.CashIndicator == "true" or paymentDetail.Cash.CashIndicator == true)) 
			    								otherwise false,
			    			psuedoPayment = isPseudoPayment(paymentDetail),
			    			cardPayment = paymentDetail.PaymentCard?
			    			
			    			)  {
							fopSequenceNumber: { 
								sequenceDetails: {
									number: "1" 
								}
							},
							
							(mopDetails:{
								fopPNRDetails: {
									fopDetails: [{
										(fopCode: "CASH") when cashPayment,
										(fopStatus: "N") when psuedoPayment
									}]
								},
								(oldFopFreeflow: {
									freeTextDetails: {
										textSubjectQualifier: "ZZZ",
										source: "M", 
										encoding: "ZZZ"
									},
									freeText: paymentDetail.Remark
								}) when psuedoPayment
							}) when cashPayment or psuedoPayment,
							(paymentModule: {
								groupUsage: {
									attributeDetails: [{
										attributeType: "DEF"
									}]
								},
								paymentData: {
									merchantInformation: {
										companyCode: "AC"
									},
									(monetaryInformation:[{
										monetaryDetails:{
											typeQualifier: paymentDetail.PaymentTransactionTypeCode,
											amount: paymentDetail.PaymentAmount.Amount[0],
											currency: paymentDetail.PaymentAmount.CurrencyCode[0]
										}
									}]) when splitPayment ,
									fraudScreeningData: {
										fraudScreening: {
											statusInformation: {
												indicator: "FRA",
												action: fraudAction
											}
										},
										(ipAdress: {
											deviceIdentification: {
												address: payload.IPAddress, //This may need to be corrected in future
												qualifier: "IP"
											}
										}) when fraudAction == "Y",
										(securityCode: [{
											securityType: {
												screeningInstructions: "DFP" 
											},
											securityKey: { //these fields will be replaced by request mapping when LM makes the IMD changes
												dataLength: payload.DFPlength,     //15, 
												dataType: "B", 
												binaryData: payload.DFP  //"MjAxNDA1MDdfNVBDR0g2" 
											}
										}]) when fraudAction == "Y"
									}
									
								},
								mopInformation: {
									fopInformation: {
										formOfPayment: {
											type: "CC"
										}
									},
									dummy: {},
									creditCardData: {
										creditCardDetails: {
											ccInfo: {
												vendorCode: paymentDetail.PaymentCard.CardCode,    
												cardNumber: paymentDetail.PaymentCard.CardNumber.Token,
												securityId: paymentDetail.PaymentCard.SeriesCode.Token,
												expiryDate: paymentDetail.PaymentCard.ExpireDate,
												ccHolderName: paymentDetail.PaymentCard.CardHolderName
											}
										}
									}
								},
								dummy: {}
							}) when cardPayment
						}
					)
				
				}))
				
				}
				