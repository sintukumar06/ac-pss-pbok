%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var fopResponse = payload when payload != null otherwise null
%var mopDescriptions = flatten fopResponse.FOP_CreateFormOfPaymentReply.fopDescription.mopDescription when fopResponse != null and commonLib.isArrayNotEmpty(fopResponse.FOP_CreateFormOfPaymentReply.fopDescription) 
		otherwise null
---
{
	AirReservation: [{
		Fulfillment: { 
			PaymentDetails: mopDescriptions map using (
				ccInfo=$.paymentModule.mopInformation.creditCardData.creditCardDetails.ccInfo,
				monetaryInfo=$.paymentModule.paymentData.monetaryInformation
			){
				PaymentCard: {
					CardCode: ccInfo.vendorCode when ccInfo.vendorCode? otherwise null,
					ExpireDate: ccInfo.expiryDate when ccInfo.expiryDate? otherwise null,
					CardHolderName: ccInfo.ccHolderName when ccInfo.ccHolderName? otherwise null,
					CardNumber: {
						Token: ccInfo.cardNumber
					} when ccInfo.cardNumber? otherwise null,
					SeriesCode: {
						Token: ccInfo.securityId
					} when ccInfo.securityId? otherwise null
				} when ccInfo != null otherwise null,
				PaymentAmount: [{
					Amount: monetaryInfo[0].monetaryDetails.amount when monetaryInfo[0].monetaryDetails.amount? otherwise null,
					CurrencyCode: monetaryInfo[0].monetaryDetails.currency when monetaryInfo[0].monetaryDetails.currency? otherwise null,
					ApprovalCode: $.paymentModule.mopDetailedData.creditCardDetailedData.authorisationInformation.transactionDetails.transmissionControlNumber default null
				}] when commonLib.isArrayNotEmpty(monetaryInfo) otherwise null,
				Remark: $.mopDetails.oldFopFreeflow.freeText when $.mopDetails.oldFopFreeflow.freeText? otherwise null
			} // paymentDetails
		} when commonLib.isArrayNotEmpty(mopDescriptions)  otherwise null // Fulfillment
	}]
}