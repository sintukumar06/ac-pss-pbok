%dw 1.0
%output application/json skipNullOn="everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var travellerCount=sizeOf (flowVars.originalOtaAirbookModifyRequest.AirReservation.TravelerInfo.AirTraveler) when flowVars.originalOtaAirbookModifyRequest.AirReservation.TravelerInfo != null otherwise ((sizeOf flowVars.pnrRetrieve17_1Response.PNR_Reply.travellerInfo) when flowVars.pnrRetrieve17_1Response.travellerInfo != null otherwise 0)
%var existingStatusCode='OX'
%var newStatusCode='NN'

%function GetItinerayMatchWithRPH(rph)
(flowVars.originalOtaAirbookModifyRequest.AirReservation.AirItinerary.OriginDestinationOptions map  {
				(itineryData($,existingStatusCode)  when $.RPH == rph otherwise {})
	}
				
)
				




%function itineryData(itinery,param)(
	
	(itinery.FlightSegment map { 
        	
        	 flightDetails: {
              flightDate: {
                departureDate:commonLib.createAlteaDate($.DepartureDateTime,null),
                departureTime:commonLib.createAlteaTime($.DepartureDateTime) as :number,
                arrivalDate: commonLib.createAlteaDate($.ArrivalDateTime,null),
                arrivalTime: commonLib.createAlteaTime($.ArrivalDateTime) as :number
         
                     
              },
               boardPointDetails: {
                trueLocationId: $.DepartureAirport.LocationCode
              },
              offpointDetails: {
                trueLocationId: $.ArrivalAirport.LocationCode
              },
              companyDetails: {
                marketingCompany: $.MarketingAirline.Code
              },
              flightIdentification: {
                flightNumber: $.FlightNumber,
                bookingClass: $.ResBookDesigCode
              }
              }
              ,
              relatedFlightInfo: {
              quantity: travellerCount default 0,
              statusCode: [
                param
              ]
            }
              
        
        })
) 


---
Air_RebookAirSegment: {


	originDestinationDetails:
	flowVars.originalOtaAirbookModifyRequest.AirBookModifyRQ.AirItinerary.OriginDestinationOptions map {	
	originDestination:{
		origin:$.FlightSegment[0].DepartureAirport.LocationCode,
		destination:$.FlightSegment[-1].ArrivalAirport.LocationCode
	
		
	
	},
	
	itineraryInfo: flatten  {
		existingItinary: GetItinerayMatchWithRPH($.RPH) filter ($ != {}),
		newItinary:itineryData($,newStatusCode)
		
	}
		}
}
				
				
			
		
		
	
			 		
	
      
	
	
	
	
	
	

