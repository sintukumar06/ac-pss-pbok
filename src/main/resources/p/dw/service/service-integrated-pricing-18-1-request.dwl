%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var automatedAncillaries=flowVars.otaAutomatedPricedAncillariesWithSSR
%var pricingOptionKey="SEL"
%var paxRefType="E"
---
{
	"Service_IntegratedPricing": {
		"pricingOption": [{
			"pricingOptionKey": {
				"pricingOptionKey": pricingOptionKey
			},
			"paxSegTstReference": {
				"referenceDetails": automatedAncillaries.AC_Discounts map ((acDiscountAuto,indexOfacDiscountAuto)->{
					"type": paxRefType,
					"value": acDiscountAuto.SSRElement.SegmentTattoo as :string 
					when commonLib.isNotEmpty(acDiscountAuto.SSRElement.SegmentTattoo) otherwise null 
				} when commonLib.isNotEmpty(acDiscountAuto) otherwise null)
			}
		}]
	}
}