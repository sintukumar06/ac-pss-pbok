%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pricingOptionKeyAward = "AWD"
%var pricingOptionKeyServiceGroupFiltering = "GRP"
%var pricingOptionKeyFrequentFlyerOverride = "FTI"
%var criteriaDetailsEUpgradeCredits = "ECR"
%var criteriaDetailsUpgradeServices = "UP"
%var paxSegTstRefPassengerTattoo = "P"
%var pnrReply = flowVars.pnrAddMultiElements17_1CommitResponse.PNR_Reply
%var origOtaAirItin = flowVars.originalOtaAirbookRequest.AirItinerary
%var originalOtaAirBookRQFlightSegments = 
	(flatten origOtaAirItin.OriginDestinationOptions.FlightSegment)
		when  commonLib.isArrayNotEmpty(origOtaAirItin.OriginDestinationOptions.FlightSegment)
		otherwise null

// Unique per Membership ID
%function getUniqueUpgradeEligibilitiesInOtaRQ()
	(originalOtaAirBookRQFlightSegments..AC_UpgradeEligibility 
			distinctBy ($.CustLoyalty.MembershipID)
	) 
		when commonLib.isArrayNotEmpty(originalOtaAirBookRQFlightSegments) and 
			 commonLib.isArrayNotEmpty(originalOtaAirBookRQFlightSegments..AC_UpgradeEligibility)
		otherwise null

// If AC_UpgradeEligibility exists in OTA RQ then source that
%var frequentTravellerDetailsFromOriginalOtaRQ = 
(getUniqueUpgradeEligibilitiesInOtaRQ() map (fltSeg, indexOfFltSeg) -> {
	pricingOptionKey: { // Maintain order
		pricingOptionKey: pricingOptionKeyFrequentFlyerOverride // FTI
	},
	frequentFlyerInformation: {
		frequentTravellerDetails: [{
			carrier: fltSeg.CustLoyalty.ProgramID,
			number: fltSeg.CustLoyalty.MembershipID
		}]
	},
	paxSegTstReference: {
		referenceDetails: [{
			type: paxSegTstRefPassengerTattoo, // P
			// As per IMD: Note - as long as the passenger tattoo is on the PNR, 
			//             Amadeus doesn't care which tattoo is used
			value: pnrReply.travellerInfo[0].elementManagementPassenger.reference.number as :string
		}]
	}
}) when commonLib.isArrayNotEmpty(getUniqueUpgradeEligibilitiesInOtaRQ()) otherwise {}

%var retrievedDataElementsSSRsOfTypeFQTV = 
	pnrReply.dataElementsMaster.dataElementsIndiv filter $.serviceRequest.ssr.type == "FQTV"

// Only use when AC_Upgrade eligibility is not present in Orig Ota RQ.
%var frequentTravellerDetailsFromPNRAddMultiCommitResponse = 
(retrievedDataElementsSSRsOfTypeFQTV map (indivElement, indexOfIndivElement) -> {
	pricingOptionKey: { // Maintain order
		pricingOptionKey: pricingOptionKeyFrequentFlyerOverride // FTI
	},
	frequentFlyerInformation: {
		frequentTravellerDetails: using (infoGroup = indivElement.frequentFlyerInformationGroup) [{
			carrier: infoGroup.frequentTravellerInfo.frequentTraveler.company,
			number: infoGroup.frequentTravellerInfo.frequentTraveler.membershipNumber
		}]
	},
	paxSegTstReference: {
		referenceDetails: [{
			type: paxSegTstRefPassengerTattoo, // P
			value: (indivElement.referenceForDataElement.reference 
						filter $.qualifier == "PT")[0].number
		}]
	}
}) when commonLib.isArrayEmpty(getUniqueUpgradeEligibilitiesInOtaRQ()) otherwise {}

---
// Must maintain certain order. Enforced in the XSD. 
//	"pricingOptionKey" must be at the top of any "pricingOption" group.
{
	Service_IntegratedCatalogue: {
		pricingOption: [{
			pricingOptionKey: { // Maintain order
				pricingOptionKey: pricingOptionKeyAward // AWD
			},
			optionDetail: {
				criteriaDetails: [{
					attributeType: criteriaDetailsEUpgradeCredits // ECR
				}]
			}
		},
		{
			pricingOptionKey: { // Maintain order
				pricingOptionKey: pricingOptionKeyServiceGroupFiltering // GRP
			},
			optionDetail: {
				criteriaDetails: [{
					attributeType: criteriaDetailsUpgradeServices // UP
				}]
			}
		}]
		++
		frequentTravellerDetailsFromOriginalOtaRQ
		++
		frequentTravellerDetailsFromPNRAddMultiCommitResponse
	}
}