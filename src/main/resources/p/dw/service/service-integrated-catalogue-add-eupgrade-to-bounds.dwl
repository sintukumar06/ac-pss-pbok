%dw 1.0
%output application/json skipNullOn="everywhere"

%var passengerGroups = flowVars.serviceIntegratedCatalogue18_1Response.Service_IntegratedCatalogueReply.passengerGroup
%var passengerGroups = passengerGroups filter $.frequentTravellerGroup? when passengerGroups!=null otherwise null
%var serviceGroups = flowVars.serviceIntegratedCatalogue18_1Response.Service_IntegratedCatalogueReply.serviceGroup
%var bounds = flowVars.lastPayload.AirReservation[0].AirItinerary.OriginDestinationOptions
%var upgradeCabin = {
  R : "Business",
  N : "Premium"
}
%function getUpgradeCredit(pricingGroup,ST)
using (couponInfo = (pricingGroup.couponInfoGroup filter $.segmentCouponReference.referenceDetails[0].type=="S" and $.segmentCouponReference.referenceDetails[0].value==ST)[0])  
  (couponInfo.monetaryInfo.otherMonetaryDetails filter $.typeQualifier == "FFE")[0].amount

%function getDesignator(serviceGroup)
	serviceGroup.quotaGroup[0].bookingClassUpgrade[0].bookingClassDetails[0].designator
%function getKey(desinator,ST,PT)
	desinator ++ "-" ++ ST ++ "-" ++ PT
%var upgradeMap = null when serviceGroups == null otherwise flatten (serviceGroups map (serviceGroup) ->
  using (STs=serviceGroup.quotaGroup.segmentReference.referenceDetails..value,
    		pricingGroups = serviceGroup.pricingGroup,
    		desinator = getDesignator(serviceGroup)
  ) (STs map (ST) -> {
    (pricingGroups map (pricingGroup) -> using (PTs=pricingGroup.passengerReference.referenceDetails.value)      	
      	  {
      	    (PTs map {(getKey(desinator,ST,$)) : getUpgradeCredit(pricingGroup,ST)})
      	  }
    )
  })) reduce (acc={},e) -> (e ++ acc)


%function mapUpgradeOptions(serviceGroups,PT,ST)
(serviceGroups filter $.quotaGroup.segmentReference.referenceDetails[0].value[0] == ST) map using (
  designator=getDesignator($),
  key=getKey(getDesignator($),ST,PT)
)
{
  UpgradeCabin: upgradeCabin[designator],
  RequiredCredits: upgradeMap[key]
}
%function mapBookingClassAvail(passengerGroups,flightSeg)
passengerGroups map using (freqDetail=$.frequentTravellerGroup[0].frequentTravellerInfo.frequentTravellerDetails,
    								PT = $.paxReference.passengerReference.value)
{
  AC_UpgradeEligibility: {
    		CustLoyalty: {
    			MembershipID: freqDetail.number,
    			ProgramID:freqDetail.carrier
    		},
    		LoyalLevel:freqDetail.tierLevel,
    		UpgradeOptions:mapUpgradeOptions(serviceGroups,PT,flightSeg.RPH)
    }  
}
---
null when passengerGroups == null or bounds == null
otherwise bounds map ($ - "FlightSegment"
	++ {"FlightSegment" : $.FlightSegment map ($ ++ {
	  		"BookingClassAvails" : [ //only 1
	  		  	{
		  		  	BookingClassAvail: mapBookingClassAvail(passengerGroups,$)
	  		  	}
	  		  ]
	  		
		})
	})