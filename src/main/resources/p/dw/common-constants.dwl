%dw 1.0

%var otaDateTimeFormat="yyyy-MM-dd'T'HH:mm:ss"
%var alteaDocDateFormat="ddMMMYY"
%var otaDateOnlyFormat="yyyy-MM-dd"
%var remarkMaxSize = 127

---
{
	otaDateTimeFormat: otaDateTimeFormat,
	otaDateOnlyFormat: otaDateOnlyFormat,
	alteaDocDateFormat:alteaDocDateFormat,
	remarkMaxSize:remarkMaxSize
}