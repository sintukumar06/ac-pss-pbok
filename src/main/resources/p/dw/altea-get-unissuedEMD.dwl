%dw 1.0
%output application/json
%var res = flowVars.ticketRetrieveListOfTSMResponse_15_1
%var listOfTSM = [] when res.Ticket_RetrieveListOfTSMReply == null otherwise res.Ticket_RetrieveListOfTSMReply.detailsOfRetrievedTSMs
%function hasUnissuedIndicators(tsmIndicators)
	false when tsmIndicators == null or tsmIndicators == [] otherwise
	(sizeOf (tsmIndicators filter $.statusDetails.indicator == "ISD" 
 			and $.statusDetails.action == "0")) > 0
---
listOfTSM filter hasUnissuedIndicators($.tsmIndicators) when listOfTSM.tsmIndicators != null
	otherwise []


 