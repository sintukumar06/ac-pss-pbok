%dw 1.0
%output application/json 

%function stringEmpty(string)
	string == null or string == ""
	
%function fraudDataAvailable(payload, paymentCard)
	(payload.IPAddress? and not stringEmpty(payload.IPAddress)) or
	paymentCard.Email.Content? or
	paymentCard.Telephone.PhoneNumber? or
	paymentCard.Address? or
	paymentCard.CustLoyalty.MembershipID? or
	payload.DFP? and not stringEmpty(payload.DFP) or
	payload.DFPlength?
---
{
	PAY_ValidatePayment: {
		transactionContext: {
			transactionDetails: {
				code: "STDL"
			}
		},
		paymentAndPnrContext: [ using (paymentDetails = payload.Fulfillment.PaymentDetails[0]) {
			paymentUsage: {
				actionRequestCode: "NIN"
			},
			paymentModule: {
				groupUsage: {
					attributeDetails: [{
						attributeType: "PAY"
					}]
				},
				paymentData: {
					merchantInformation: {
						companyCode: "AC"
					},
					monetaryInformation: [{
						monetaryDetails: {
							typeQualifier: "712",
							amount: paymentDetails.PaymentAmount[0].Amount as :number,
							currency: paymentDetails.PaymentAmount[0].CurrencyCode
						}
					}],
					fraudScreeningData: using (paymentCard = paymentDetails.PaymentCard) (
						{
						fraudScreening: {
							statusInformation: {
								indicator: "FRA",
								action: "Y"
							}
						},
						(ipAdress: {
							deviceIdentification: {
								address: payload.IPAddress,
								qualifier: "IP"
							}
						}) when payload.IPAddress != null ,
						payerPhoneOrEmail: (
								(
									paymentCard.Email default [] map ((emailEntry) -> {
							phoneOrEmailType: "E",
							emailAddress: emailEntry.Content
						})
								) ++
								(
									paymentCard.Telephone default [] map ((phoneEntry) -> {
							phoneOrEmailType: "P",
							telephoneNumberDetails: {
								telephoneNumber: phoneEntry.PhoneNumber
							}
						})
								)
							),
						(payerName: {
							ccHolderNameDetails: {
								surname: (paymentCard.CardHolderName splitBy " ")[1] default ""
							},
							otherNameDetails: {
								givenName: (paymentCard.CardHolderName splitBy " ")[0] default ""
							}
						}) when paymentCard.CardHolderName != null,
						(billingAddress: {
							addressDetails: {
								format: "5",
								line1: (paymentCard.Address.AddressLine[0] default "") when paymentCard.Address.AddressLine? and (sizeOf paymentCard.Address.AddressLine) > 0  otherwise "",
								(line2: paymentCard.Address.AddressLine[1] ) when paymentCard.Address.AddressLine[1] != null,
								line6: paymentCard.Address.StateProv.StateCode default ""
							},
							city: paymentCard.Address.CityName default "",
							zipCode: paymentCard.Address.PostalCode default "",
							countryCode: paymentCard.Address.CountryName.Code default "" 
						}) when paymentCard.Address != null,
						(shopperDetails: {
							shopperID: {
								originator: paymentCard.CustLoyalty[0].MembershipID 
							}
						})when paymentCard.CustLoyalty[0].MembershipID != null,
						(securityCode: [{
							securityType: {
								screeningInstructions: "DFP"
							},
							securityKey: {
								(dataLength: payload.DFPlength) when payload.DFPlength != null ,
								(dataType : "B") when payload.DFP != null or payload.DFPlength != null,
								(binaryData: payload.DFP) when payload.DFP != null
							}
						}]) when payload.DFP != null or payload.DFPlength != null
					} when fraudDataAvailable(payload, paymentCard) otherwise {
						fraudScreening: {
							statusInformation: {
								indicator: "FRA",
								action: "N"
							}
						}
					}
						
					)
				},
				mopInformation: {
					fopInformation: {
						formOfPayment: {
							type: "CC"
						}
					},
					dummy: {
					},
					creditCardData: {
						creditCardDetails: {
							ccInfo: {
								vendorCode: paymentDetails.PaymentCard.CardCode,
								cardNumber: paymentDetails.PaymentCard.CardNumber.Token,
								(securityId: paymentDetails.PaymentCard.SeriesCode.Token) when paymentDetails.PaymentCard.SeriesCode.Token != null,
								(expiryDate: paymentDetails.PaymentCard.ExpireDate ) when paymentDetails.PaymentCard.ExpireDate != null
							}
						}
					}
				},
				dummy: {}
			}
		}]
	}
}