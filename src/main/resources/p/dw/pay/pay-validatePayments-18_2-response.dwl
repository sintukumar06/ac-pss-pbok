%dw 1.0
%output application/json skipNullOn = "everywhere"

%var responseStatusMap = {
	"OK": "Success",
	"OKMORE": "Success",
	"KO": "Failure",
	"KOMORE": "Failure"
}
---
using (paymentModule = payload.PAY_ValidatePaymentReply.paymentAndPnrContext[0].paymentModule) {
	Fulfillment: {
		PaymentDetails: [{
			PaymentAmount: [{
				Amount: paymentModule.paymentData.monetaryInformation[0].monetaryDetails.amount,
				ApprovalCode: paymentModule.mopDetailedData.creditCardDetailedData.approvalDetails.approvalCodeData.approvalCode
			}]
		}]
	},
	ResultCode: responseStatusMap[paymentModule.paymentStatus.paymentStatusInformation.statusCode],
	ReferenceCode: paymentModule.paymentData.paymentId map ((payId, payIdIndex) -> {
		Content: payId.uniqueReference,
		Code: {
			PRI: "PRI",
			EXT: "PSP"
		}[payId.referenceType]
	} when ["PRI", "EXT"] contains payId.referenceType otherwise null)
}