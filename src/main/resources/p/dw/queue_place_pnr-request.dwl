%dw 1.0
%output application/json skipNullOn="everywhere"
%var queueOptionIndicator='QEQ'
%var businessSourceInformation=4
%var totalQueuesTobeSent=16
%var subQueueType='C'
%var subQueueItemNumber=2
---

Queue_PlacePNR:{
	placementOption:{
		selectionDetails:{
			option: queueOptionIndicator as :string
	}
},
	targetDetails: [
	{
		targetOffice:{
			sourceType:{
				sourceQualifier1: businessSourceInformation as :string
		},
	originatorDetails:{
		inHouseIdentification1: flowVars.acOfficeID  when flowVars.acOfficeID != "" otherwise ""
		}
	},
	queueNumber:{
		queueDetails:{
			number:totalQueuesTobeSent
		}
	},
	categoryDetails:{
		subQueueInfoDetails:{
			identificationType:subQueueType,
			itemNumber: subQueueItemNumber as :string
		}
	}
	}
	],
	recordLocator:{
		reservation:{
			controlNumber:flowVars.pnr
		}
	}
}

