%dw 1.0
%output application/json
---
{
	"PNR_Cancel": {
		"reservationInfo": {
      "reservation": {
        "controlNumber": flowVars.pnr
         //"P68I9L"
      }
    },				
		"pnrActions": {
			"optionCode":["0"]
		},
		"cancelElements": [{
			"entryType": "I"
		}]
	}
}