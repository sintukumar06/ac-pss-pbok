%dw 1.0
%output application/json skipNullOn = "everywhere"
// This configuration shows all TSTs in the PNR. 
---
{
	Ticket_DisplayTST: {
		displayMode: {
			attributeDetails: {
				attributeType: "ALL"
			}
		}
	}
}