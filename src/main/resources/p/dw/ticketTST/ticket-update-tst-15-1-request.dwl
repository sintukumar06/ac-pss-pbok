%dw 1.0
%output application/json skipNullOn = "everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var fareListTST = payload //single element form flowVars.ticketDisplayTSTFareList 
%var fareComponentDetailsGroup = fareListTST.fareComponentDetailsGroup
%var fareDataMainInformation = fareListTST.fareDataInformation.fareDataMainInformation
%var fareDataSupInformation = fareListTST.fareDataInformation.fareDataSupInformation
%var taxInformation = fareListTST.taxInformation
%var segmentInformation = fareListTST.segmentInformation
%var otherPricingInfo = fareListTST.otherPricingInfo
%var statusInformation = fareListTST.statusInformation
%var originDestination = fareListTST.originDestination
%var fareReference = fareListTST.fareReference
%var actionRequestCode = "FUP"
%var fareRefTypTSTorFareRefNume = "TST"
%var fareDataQualBaseFare = "B"
%var pricingInfoHorizontalFareCalculation = "FCA"
%var fareBasisCode = "FBC"
			
%function getPricingInfoFCA()
	((flatten otherPricingInfo.attributeDetails) 
		filter $.attributeType == pricingInfoHorizontalFareCalculation)
			when otherPricingInfo != null and otherPricingInfo.attributeDetails != null
				otherwise null				
---
{
	
	Ticket_UpdateTST: {
		actionInformation: {
			actionRequestCode: actionRequestCode
		},
		
		fareReference: {
			referenceType: fareRefTypTSTorFareRefNume,
			uniqueReference: fareReference.uniqueReference default null,
			(iDDescription: {
				iDSequenceNumber: fareReference.iDDescription.iDSequenceNumber 
			}) when fareReference.iDDescription != null
		},
		
		(fareDataInformation: {
			(fareDataMainInformation: {
				fareDataQualifier: fareDataMainInformation.fareDataQualifier
			}) when fareDataMainInformation != null,
			(fareDataSupInformation: fareDataSupInformation map { 
      		
				fareDataQualifier: $.fareDataQualifier,
				fareAmount: commonLib.getZeroStringWithDecimalZeros($.fareAmount),
				fareCurrency : $.fareCurrency
			}
			) when fareDataSupInformation != null
		}) when fareListTST.fareDataInformation != null,
		
		(taxInformation : taxInformation map (taxInfo,indexOfTaxInfo)->{
			taxDetails: {
				taxQualifier: taxInfo.taxDetails.taxQualifier,
				taxIdentification: {
					taxIdentifier: taxInfo.taxDetails.taxIdentification.taxIdentifier
				},
				taxType: {
					isoCountry: taxInfo.taxDetails.taxType.isoCountry
				},
				taxNature: taxInfo.taxDetails.taxNature
			},
			amountDetails: {
				fareDataMainInformation: using (fareData = taxInfo.amountDetails.fareDataMainInformation) {
					fareDataQualifier: fareData.fareDataQualifier,
					fareAmount: commonLib.getZeroStringWithDecimalZeros(fareData.fareAmount),
					fareCurrency: fareData.fareCurrency
				}
			}
		}) when taxInformation != null,
		
		(originDestination: {
			cityCode : originDestination.cityCode
		}) when originDestination != null,
		
		(segmentInformation: segmentInformation map (segInfo, indexSegInfo) -> {
			connexInformation: {
				connecDetails: {
					connexType: segInfo.connexInformation.connecDetails.connexType
				}
			},
			fareQualifier: { 
				// As per EPM-9595 use the first fareQualifier returned from DisplayTST
				fareBasisDetails: using (fareBasisDetails = segInfo.fareQualifier[0].fareBasisDetails) {
					primaryCode: fareBasisDetails.primaryCode,
					fareBasisCode: fareBasisDetails.fareBasisCode,
					ticketDesignator: fareBasisDetails.ticketDesignator
				}
			},			
			bagAllowanceInformation: {
				bagAllowanceDetails: using (bagAllowDeets = segInfo.bagAllowanceInformation.bagAllowanceDetails) {
					baggageQuantity: bagAllowDeets.baggageQuantity default null,
					baggageType: bagAllowDeets.baggageType
				}
			},
			sequenceInformation: {
				sequenceSection: {
					sequenceNumber: segInfo.sequenceInformation.sequenceSection.sequenceNumber
				}
			}
		}) when segmentInformation != null,
		
		(otherPricingInfo: {
			// As per 9621 Take where Attribute type == "FCA"
			attributeDetails: getPricingInfoFCA() map (attributeDetail, indexOfattrDeets) -> {
				attributeType: attributeDetail.attributeType,
				attributeDescription: commonLib.replaceAllNumbersWithZeros(attributeDetail.attributeDescription)
			}
		}) when getPricingInfoFCA() != null,

		(statusInformation: {
			firstStatusDetails: {
				tstFlag: statusInformation.firstStatusDetails.tstFlag
			}
		}) when statusInformation != null,
		
		(fareComponentDetailsGroup: fareComponentDetailsGroup map  
				(fareComponentDetailsGroup, indexOffareComponentDetailsGroup) -> {
			fareComponentID: {
				itemNumberDetails: fareComponentDetailsGroup.fareComponentID.itemNumberDetails 
						map (itemNumberDetails, indexOfitemNumberDetails) -> {
					number: itemNumberDetails.number,
					type: itemNumberDetails.type
				}
			},
			monetaryInformation: {
				monetaryDetails: using (monetaryDet = fareComponentDetailsGroup.monetaryInformation.monetaryDetails) {
					typeQualifier: monetaryDet.typeQualifier,
					amount: commonLib.getZeroStringWithDecimalZeros(monetaryDet.amount),
					currency: monetaryDet.currency
				}
			},
			fareComponentServiceAttributes: {
				criteriaDetails: [{
					attributeType: fareBasisCode, // FareBasisCode
					// As per EPM-9598 use the below for source. 
					attributeDescription: fareComponentDetailsGroup.componentClassInfo.fareBasisDetails.rateTariffClass
				} ]//change
			}

		}) when fareComponentDetailsGroup != null
	}
}