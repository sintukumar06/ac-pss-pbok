%dw 1.0
%output application/json skipNullOn="everything"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%function isEmptyTicketNumber (element)
element when commonLib.isNotEmpty(element) otherwise ""

---
{
	Ticket_ReissueConfirmedPricing: {
		ticketInfo: flowVars.pnrRetrieve17_1Response.PNR_Reply.dataElementsMaster.dataElementsIndiv map ((dataElementsIndiv , indexOfdataElementsIndiv) -> {
			paperticketDetailsFirstCoupon : {
				documentDetails : {	
					number:isEmptyTicketNumber(commonLib.extractWord(commonLib.extractWord(commonLib.extractWord(dataElementsIndiv.otherDataFreetext[0].longFreetext,0,"/"),1," "),1,"-")),													
					type: "ET"
				}
			}
			
		} when  dataElementsIndiv.elementManagementData.segmentName == "FA" and (commonLib.extractWord(dataElementsIndiv.otherDataFreetext[0].longFreetext,1,"/") == "ETAC") otherwise {}) 
		filter $ != {} //filter out empty elements
		  		 
		
	}	
	
}	