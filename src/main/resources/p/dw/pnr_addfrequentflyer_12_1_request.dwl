%dw 1.0
%output application/json
skipNullOn = "everywhere"

%var pnrAddMultiLib = readUrl("classpath://p/dw/addMultiElements/pnr-addMultiElements-17_1-common-functions.dwl")
---
{
	PNR_AddFrequentFlyer: {
		paxInfoGroup: {
			paxInfo: {
				paxDetails: {
					surname: flowVars.originalOtaAirbookRequestAirTraveler.PersonName.Surname
				},
				otherPaxDetails: {
					givenName: pnrAddMultiLib.buildAlteaGivenName(flowVars.originalOtaAirbookRequestAirTraveler.PersonName.GivenName[0]!, flowVars.originalOtaAirbookRequestAirTraveler.PersonName.NamePrefix[0], flowVars.originalOtaAirbookRequestAirTraveler.PersonName.NameSuffix[0]),
					uniqueCustomerIdentifier: flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap[flowVars.originalOtaAirbookRequestAirTraveler.TravelerRefNumber.RPH]
				}
			},
			fqtvGroup: flowVars.originalOtaAirbookRequestAirTraveler.CustLoyalty map ((custLoyalty , indexOfCustLoyalty) -> {
				fqtvData: {
					actionRequest: "O",
					companyDetails: {
						marketingCompany: custLoyalty.ProgramID
					},
					accountDetails: {
						frequentTravellerId: custLoyalty.MembershipID
					}
				}
			})
		}
	}
}