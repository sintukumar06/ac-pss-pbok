%dw 1.0
%output application/java
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var otaManuallyPricedAncilliariesWithSSR=flowVars.otaManuallyPricedAncillariesWithSSR
%var otaACDiscounts=otaManuallyPricedAncilliariesWithSSR.AC_Discounts
%var ticketCreateManualReply=flowVars.ticketCreateManualTSMP15_1Response
%var ticketDetailsOfTSM=ticketCreateManualReply.Ticket_CreateManualTSMPReply.detailsOfCreatedTSMs

%function getServiceTattoos(tsm, segmentName)
 tsm.serviceTattoos.referenceDetails default [] filter ($.type == segmentName)

%function GetTSMTattoo (acDiscountElement)
	ticketDetailsOfTSM map (ticketTSM)->
	using (
		svcTattoos=getServiceTattoos(ticketTSM, acDiscountElement.SSRElement.SegmentName)		
	){
		("TSMTattoo": {
			"UniqueReference": ticketTSM.tattooOfTSM.uniqueReference
		}) when svcTattoos != [] and 
				(svcTattoos.value contains (acDiscountElement.SSRElement.SegmentTattoo as :string))
	} when acDiscountElement.SSRElement != null otherwise []
---
"AC_Discounts": otaACDiscounts map (acDiscount)-> {
	(acDiscount + GetTSMTattoo(acDiscount))
}