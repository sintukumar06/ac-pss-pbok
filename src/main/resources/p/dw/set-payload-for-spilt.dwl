%dw 1.0
%output application/json
%var body = flowVars.otaPnrSplit17_1Response
%var newPnrValue = flowVars.pnrAddMultiElements17_1EndTransactResponse.PNR_Reply.pnrHeader[0].reservationInfo.reservation[0].controlNumber
%var bookRefIds = body.AirReservation[0].BookingReferenceID[1 to -1] when body.AirReservation[0].BookingReferenceID[1] != null and body.AirReservation[0].BookingReferenceID[1] != "" otherwise []
%var bookRefId0 = body.AirReservation[0].BookingReferenceID[0] - "ID" ++ "ID": newPnrValue
---
//I want to do this: body.AirReservation[0].BookingReferenceID[0].ID=newPnrValue
//but you can't do this in DW, so:
AirReservation: [body.AirReservation[0] - "BookingReferenceID" ++ "BookingReferenceID" : [bookRefId0] ++ bookRefIds]
