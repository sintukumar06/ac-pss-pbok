%dw 1.0
%output application/json skipNullOn = "everywhere"
%var manuallyPricedSSR = flowVars.otaManuallyPricedAncillariesWithSSR
---
{
	"Ticket_CreateManualTSMP": {
		"serviceTattoos": manuallyPricedSSR.AC_Discounts map ((manualSSR , indexOfmanualSSR) -> {
			"referenceDetails": {
				"type": manualSSR.SSRElement.SegmentName as :string
				when manualSSR.SSRElement.SegmentName? otherwise null,
				"value": manualSSR.SSRElement.SegmentTattoo as :string
				when manualSSR.SSRElement.SegmentTattoo? otherwise null
			}
		} when manualSSR.SSRElement != null otherwise null),
		"validatingCarrier": {
			"companyIdentification": {
				"otherCompany": "AC"
			}
		}
	} default []
}