%dw 1.0
%output application/json skipNullOn = "everywhere" 
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var taxFareIndicator='X'
%var baseFareIndicator='F'
%var totalFareIndicator='T'
---
{
	Ticket_UpdateTSMP : {
		tattooOfTSM : {
			uniqueReference: flowVars.otaAirBookACDiscount.TSMTattoo.UniqueReference 
		},
		monetaryInformation: [{
			monetaryDetails: {
				typeQualifier: baseFareIndicator,
				amount: commonLib.formatAmount(flowVars.otaAirBookACDiscount.DiscountFare.Amount) default '0.00',
				currency: flowVars.otaAirBookACDiscount.DiscountFare.CurrencyCode 
			}
		},
		{
			monetaryDetails: {
				typeQualifier: totalFareIndicator,
				amount: commonLib.formatAmount(flowVars.otaAirBookACDiscount.DiscountFare.Amount as :number + flowVars.otaAirBookACDiscount.DiscountFare.Taxes.Amount as :number) default '0.00',
				currency: flowVars.otaAirBookACDiscount.DiscountFare.CurrencyCode 
			}
		}],
		(taxInformation: flowVars.otaAirBookACDiscount.DiscountFare.Taxes.Tax map {
			taxCategory: taxFareIndicator,
			taxDetails: {
				rate: commonLib.formatAmount($.Amount),
				countryCode: $.TaxCode,
				currencyCode:$.CurrencyCode when $.CurrencyCode? otherwise flowVars.otaAirBookACDiscount.DiscountFare.CurrencyCode,
				type: $.TaxCountry
			}
		}) when commonLib.isArrayNotEmpty(flowVars.otaAirBookACDiscount.DiscountFare.Taxes.Tax),
		validatingCarrier:{
			companyIdentification:{
				otherCompany:flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.validatingCarrier.companyIdentification.otherCompany
			}
			
		},
		couponsDetails:  [{
			serviceTattoo: {
				referenceDetails: {
					type: flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails[0].productTattoo.referenceDetails.type when commonLib.isArrayNotEmpty(flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails) otherwise null,
					value:flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails[0].productTattoo.referenceDetails.value when commonLib.isArrayNotEmpty(flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails) otherwise null
				}
			},
			(validityDates:flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails[0].validityDates map {
				businessSemantic:$.businessSemantic,
					dateTime:{
						year:$.dateTime.year,
						month:$.dateTime.month,
						day:$.dateTime.day
				}
			}) when commonLib.isArrayNotEmpty(flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails) and
						commonLib.isArrayNotEmpty(flowVars.ticketDisplayTSMP13_1Response.Ticket_DisplayTSMPReply.detailsOfTSM.couponsDetails[0].validityDates),
			ambiDummy:{}
		}]
	
	
	}
}