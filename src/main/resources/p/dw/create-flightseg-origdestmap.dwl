%dw 1.0
%output application/java
//Altea returns flightSegments within a single origDestDetails. As part of booking process, the response needs separate the flightsegment across bounds.
//Creates an origDestMap with the key and RPH
// key is formed from depAirportCode, depDT, arrAirportCode, arrDT, marketingAirlineCode and FlightNumber
%var OrigDestOptionsRQ = flowVars.originalOtaAirbookRequest.AirItinerary.OriginDestinationOptions map (origDestOption, idxOrigDestOption) -> (
	(origDestOption.FlightSegment map {
		($.DepartureAirport.LocationCode ++ "_" ++ $.DepartureDateTime ++ "_"
			++ $.ArrivalAirport.LocationCode ++ "_" ++ $.ArrivalDateTime ++ "_"
			++ $.MarketingAirline.Code ++ "_" ++ $.FlightNumber
		): origDestOption.RPH when origDestOption.RPH? otherwise idxOrigDestOption + 1
	})
)
%var origDestMap = (flatten OrigDestOptionsRQ) reduce (obj, acc) -> {
	 (acc + obj)
}
---
origDestMap