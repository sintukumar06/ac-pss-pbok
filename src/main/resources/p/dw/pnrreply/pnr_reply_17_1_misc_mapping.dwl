%dw 1.0
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")

%function createOTATS (tDate, tTime)
	((tDate ++ '0' ++ tTime) as :localdatetime {format: "ddMMyyHHmm"} as :localdatetime as :string {format: "yyyy-MM-dd'T'HH:mm:ss"}) when (commonLib.isNotEmpty(tDate) and commonLib.isNotEmpty(tTime) and (sizeOf (tTime as :string)) == 3)
	otherwise
	((tDate ++ tTime) as :localdatetime {format: "ddMMyyHHmm"} as :localdatetime as :string {format: "yyyy-MM-dd'T'HH:mm:ss"}) when (commonLib.isNotEmpty(tDate) and commonLib.isNotEmpty(tTime)) and (sizeOf (tTime as :string)) == 4
	otherwise ((tDate ++ tTime) as :localdatetime {format: "ddMMyyHH"} as :localdatetime as :string {format: "yyyy-MM-dd'T'HH:mm:ss"}) when (commonLib.isNotEmpty(tDate) and commonLib.isNotEmpty(tTime)) and (sizeOf (tTime as :string)) == 2
	otherwise null

	
%function mapBookingReference(pnrReply)
[{
	ID: pnrReply.pnrHeader[0].reservationInfo.reservation[0].controlNumber,
	Type: "PNR",
	CompanyName: {
		Code: pnrReply.originDestinationDetails[0].itineraryInfo[0].travelProduct.companyDetail.identification,
	    CodeContext: pnrReply.securityInformation.responsibilityInformation.officeId
	} when (pnrReply.originDestinationDetails[0].itineraryInfo[0].travelProduct.companyDetail.identification? or pnrReply.securityInformation.responsibilityInformation.officeId?) otherwise null,
	(ID_Context: pnrReply.securityInformation.responsibilityInformation.iataCode) when pnrReply.securityInformation.responsibilityInformation.iataCode?
}] when pnrReply.pnrHeader[0].reservationInfo.reservation[0].controlNumber? otherwise null


%function mapCreateDateTime(pnrReply)
	createOTATS(pnrReply.securityInformation.secondRpInformation.creationDate,pnrReply.securityInformation.secondRpInformation.creationTime)


//time is defined as an integer and looses the zeros during xml to json conversion
%function mapLastModified(pnrReply)
using (timeVal=pnrReply.pnrHeader[0].reservationInfo.reservation[0].time)
	(createOTATS(pnrReply.pnrHeader[0].reservationInfo.reservation[0].date, timeVal when timeVal != null and ((sizeOf timeVal as :string) == 4)
			otherwise ("0" ++  timeVal as :string) when timeVal != null and ((sizeOf timeVal as :string) == 3) 
			otherwise ("00" ++ timeVal as :string) when timeVal != null and ((sizeOf timeVal as :string) == 2) otherwise null
	))
	

%function mapFulfillment(dataElmByType)
{
	PaymentDetails: flatten dataElmByType.FP.*otherDataFreetext default [] map (freeText) ->
		using (isCC = (freeText.longFreetext contains "CC"),
			isCash = (freeText.longFreetext contains "CASH")
		) {
			PaymentTransactionTypeCode: null, // 206 internally set
			(PaymentCard: using(ccText = (flatten (freeText.longFreetext scan /.*CC(.*)/))[1]) //AXXXXXXXXXXXX0018/1020/CAD3160.75
					using (tokens = ccText splitBy "/")
				 { 
				//scenario1: PAX CASH/CAD100.00+CCAXXXXXXXXXXXX0018/1020/CAD3160.75
				//scenario2: PAX CCAXXXXXXXXXXXX0018/1020/CAD10.00+CASH/CAD3250.75
				CardCode: ccText[0..1], // 1st and 2nd character of first token. VIXXXXXXXXXXXX1237/1020*CV/A38196/S2
				ExpireDate: tokens[1][0..3], //first 4 digits of second token			
				CardNumber: {
					Token: tokens[0][2..-1] // 3rd to end of first token
				}
			}) when isCC,
			(Cash: {
				CashIndicator: true
			}) when isCash,
			(Remark: freeText.longFreetext) when not (isCash or isCC) 
	}
} when dataElmByType != null and commonLib.isArrayNotEmpty(dataElmByType.FP) otherwise null

%function extractTicketType(freeTxtTokens)
	freeTxtTokens[1][0..1] when freeTxtTokens != null and ((sizeOf freeTxtTokens) > 1) otherwise ""
	
%function getTicketType(freeTxtTokens)
using (ticketType = extractTicketType(freeTxtTokens))
("eTicket" when ticketType == "ET" or ticketType == "ER" otherwise 
	("EMD" when ticketType == "DT" or ticketType == "DR" otherwise null)) 

%function getTicketDocumentNbr(ticketToken) 
using (ticketNumber=ticketToken[4..-1] when ticketToken != null otherwise null)
(ticketNumber replace "-" with "" when ticketNumber != null otherwise null)

%function getTicketingTimeLimit(opcElement)
commonLib.createOTATS(opcElement.optionElement.optionElementInfo.date,opcElement.optionElement.optionElementInfo.time) when opcElement.optionElement? otherwise null

%function getTicketNumberFromFASegment(faSegmentFreeText)
(faSegmentFreeText[0])[4..-1] when commonLib.isArrayNotEmpty(faSegmentFreeText) otherwise null

%function faSegmentTicketingStatus(freeTxtTokens, dataElmByType, PTRefs)
using (
	FOExists = commonLib.isArrayNotEmpty(dataElmByType.FO),
	matchingFO = dataElmByType.FO default [] filter (pnrReplycommonLib.getRefsByQualifier($, "PT", "DataElm") == PTRefs),
	FATicketNumber=getTicketNumberFromFASegment(freeTxtTokens),
	ticketType = extractTicketType(freeTxtTokens)
)
using (FOTicketNumber=pnrReplycommonLib.getTicketNumberFromFOSegment(matchingFO[0]))
( "PTK"  when (FOExists == true and FATicketNumber!= null and FOTicketNumber != null and FOTicketNumber == FATicketNumber)
	otherwise 
		"TKD" when ((FOExists == false and (ticketType == "ET" or ticketType == "DT")) 
						or (FOExists == true and FATicketNumber!= null and FOTicketNumber != null and FOTicketNumber != FATicketNumber))
			otherwise
				"REF" when 	(FOExists == false and (ticketType == "ER" or ticketType == "DR"))			
					otherwise "UNK"
)

%function mapTicketingElm(dataElm, freeTxtTokens, PTRefsInput)
using (PTRefs=pnrReplycommonLib.getRefsByQualifier(dataElm, "PT", "DataElm") when PTRefsInput == null otherwise PTRefsInput,
		STRefs=pnrReplycommonLib.getRefsByQualifier(dataElm, "ST", "DataElm"))
	{
		(TravelerRefNumber : PTRefs) when commonLib.isArrayNotEmpty(PTRefs),
		(FlightSegmentRefNumber: STRefs) when commonLib.isArrayNotEmpty(STRefs),
		TicketType: getTicketType(freeTxtTokens),		
		TicketDocumentNbr: getTicketDocumentNbr(freeTxtTokens[0] when commonLib.isArrayNotEmpty(freeTxtTokens) otherwise null),
		RequestedTicketingDate: commonLib.formatDate(freeTxtTokens[3],"ddMMMyy","yyyy-MM-dd")	when commonLib.isArrayNotEmpty(freeTxtTokens) and ((sizeOf freeTxtTokens) > 3) otherwise null	
	}


%function opcTicketMapping(dataElm)
using (freeTxtTokens=pnrReplycommonLib.getFreeTextTokens(dataElm))
  mapTicketingElm(dataElm, freeTxtTokens, null) ++ {
  	 	TicketingStatus: "TTL", 
  	 	TicketTimeLimit: getTicketingTimeLimit(dataElm)
  	 }


%function faTicketMapping(dataElm, dataElmByType)
using (freeTxtTokens=pnrReplycommonLib.getFreeTextTokens(dataElm),
	miscTicketingCode=pnrReplycommonLib.getRefsByQualifier(dataElm, "OT", "DataElm"),
	PTRefs=pnrReplycommonLib.getRefsByQualifier(dataElm, "PT", "DataElm")
)
 (mapTicketingElm(dataElm, freeTxtTokens, PTRefs) ++ {
 	 	TicketingStatus: faSegmentTicketingStatus(freeTxtTokens, dataElmByType, PTRefs), 
 	 	MiscTicketingCode: [miscTicketingCode[0].number] when commonLib.isArrayNotEmpty(miscTicketingCode) otherwise null	
 	 })


%function mapTicketing(dataElmByType)
(
	flatten (dataElmByType.OPC default [] map (ticketingElement) ->
								(opcTicketMapping(ticketingElement)) when dataElmByType != null and commonLib.isArrayNotEmpty(dataElmByType.OPC) otherwise []) +
	(dataElmByType.FA default [] map (ticketingElement) -> 
			  					(faTicketMapping(ticketingElement, dataElmByType)) when dataElmByType != null and commonLib.isArrayNotEmpty(dataElmByType.FA) otherwise [])
			  					
) when dataElmByType != null and (commonLib.isArrayNotEmpty(dataElmByType.OPC) or commonLib.isArrayNotEmpty(dataElmByType.FA)) otherwise null



---
{
	mapTicketing : mapTicketing,
	mapBookingReference : mapBookingReference,
	mapFulfillment: mapFulfillment,
	mapCreateDateTime: mapCreateDateTime,
	mapLastModified: mapLastModified
}