%dw 1.0

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")

%var lookupMap ={
	"freeTextIndexMap" : {
							"DOCS": 
									{
										BirthDate: 4, 
										DocHolderNationality: 3,
										DocID: 2,
										DocIssueCountry:1,
										ExpireDate:6,
										Gender:5,
										HolderType:10,
										AC_DocType:0,
										DocHolderName:7,
										AdditionalPersonName0: 8,
										AdditionalPersonName1: 9
									},
							"DOCO": 
									{
										BirthPlace: 0, 
										DocID: 2,
										DocIssueCountry:5,
										DocIssueLocation:3,
										EffectiveDate:4,
										HolderType:6,
										AC_DocType:1
									},			
							
							"DOCA":{
									"Remark":0,
									"StreetNmbr":2,
									"AddressLine":2,
									"CityName":3,
									"PostalCode":5,
									"StateProv":4,
									"CountryNameCode":1,
									"HolderType":6
								}
						},
							
	"Gender": {"M":"Male","MI":"Male","FI":"Female","F":"Female"},
	"HolderType" :{"I":"Infant", "H":"HeadOfHousehold"},
	"AC_DocType":{"P":"Passport","USP":"USPassport","USC":"USAlienCard","C":"CAResidentCard","IN":"Nexus","V":"Visa","R":"Redress","K":"KnownTraveller","I":"IdentityCard"},
	"Remark":{"R":"ResidenceAddress","D":"DestinationAddress"}
									
}

%function extractWord(o, position)
	(commonLib.extractWord(o, position,'/')) 
		
%function getDocAdditionalPersonNames(name1, name2)
([name1, name2]	 when commonLib.isNotEmpty(name1) and commonLib.isNotEmpty(name2)
	otherwise [name1] when commonLib.isNotEmpty(name1)
	otherwise null)	

%function getAddressDOCA(inputAddress)
using (addresses=inputAddress splitBy (" ") when commonLib.isNotEmpty(inputAddress) otherwise null)
({
	streetNbr: addresses[0],
	addressLine: commonLib.buildStringWithDelimiter(addresses[1 to -1], " ", false)
} when commonLib.isArrayNotEmpty(addresses) and ((sizeOf addresses) > 1)
otherwise {
	streetNbr: null,
	addressLine: inputAddress
} when addresses != null 
otherwise null)

%function getBirthDate(dateOfBirth, ssrDOCS)
dateOfBirth as :date {format: "ddMMyyyy"} as :string {format: "yyyy-MM-dd"} when commonLib.isNotEmpty(dateOfBirth) 
	otherwise commonLib.formatDate(extractWord ((flatten ssrDOCS.serviceRequest.ssr.freeText)[0],lookupMap.freeTextIndexMap[ssrDOCS.serviceRequest.ssr.type[0]].BirthDate),"ddMMMyy","yyyy-MM-dd")
			when commonLib.isNotEmpty((flatten ssrDOCS.serviceRequest.ssr.freeText)[0] )
			otherwise null
	
%function getStructuredAddressObject(structuredAddress)
structuredAddress.address reduce (obj, acc={}) ->
	 		(
	 		 	acc ++ ({POBox: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="PO"
	 		 		otherwise {Address1: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="A1"
	 		 		otherwise {Address2: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="A2"
	 		 		otherwise {City: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="CI" 
	 		 		otherwise {Zip: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="ZP"
	 		 		otherwise {State: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="ST"
	 		 		otherwise {Country: obj.optionText} when commonLib.isNotEmpty(obj.optionText) and obj.option=="CO"
	 		 		otherwise {})
	 		) 

%function getElmFromDataElmBucket (dataElm, paxData)
	(dataElm filter $.referenceForDataElement.reference[0].qualifier == "PT" and $.referenceForDataElement.reference[0].number == paxData.travellerRPH as :string
	) when commonLib.isArrayNotEmpty(dataElm) otherwise null
		
%function getContactDataElms(dataElm, paxData,ptc)
using (PTDataElms = getElmFromDataElmBucket(dataElm, paxData),
	nonPTDataElms = dataElm default [] filter (not (["PT"] contains $.referenceForDataElement.reference[0].qualifier))
)
 (( PTDataElms when PTDataElms != null and ptc != 'INF' otherwise []) ++ nonPTDataElms)
 
 %function parsePhoneTextTokens(freetext)
 using (txtTokens = freetext default [] splitBy " ")
 ({
 	CountryAccessCode: txtTokens[0] when (freetext != null and (sizeOf txtTokens) > 1) otherwise null,
	AreaCityCode: txtTokens[1] when (freetext != null and (sizeOf txtTokens) > 2) otherwise null,
	PhoneNumber: txtTokens[1] when (freetext != null and (sizeOf txtTokens) == 2)
					otherwise commonLib.buildStringWithDelimiter(txtTokens[2 to -1], " ", true) when (freetext != null and (sizeOf txtTokens) > 2)
					otherwise (freetext)
})			
 
%function parsePhoneFreeText(freetext)
{
 PhoneNumber: freetext replace /^(AC\/.\+)/ with "" 
 } when freetext startsWith "AC/"
   otherwise parsePhoneTextTokens(freetext)
   

%function parseEmailFreeText(freetext)
	freetext replace "AC/E+" with "" when freetext startsWith "AC/E+"
	otherwise freetext



%function mapAddresses(paxData, dataElmByType, isoCountryFn,ptc)
using (structAddresses = getContactDataElms(dataElmByType.StructuredAddress, paxData, ptc),
	DOCA = getElmFromDataElmBucket(dataElmByType.DOCA, paxData)
)
using (
	structureAddrObjArray = (structAddresses default [] map {
 										(getStructuredAddressObject($.structuredAddress))
 								}) when commonLib.isArrayNotEmpty(structAddresses) and commonLib.isArrayNotEmpty(structAddresses.structuredAddress.address) otherwise null
)
(
		(
			(DOCA default [] map using (ssrType = $.serviceRequest.ssr.type,
					ssrFreeText = $.serviceRequest.ssr.freeText[0] splitBy "/"
				) 
				using (remark=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].Remark],
					addressLine=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].AddressLine],
					city=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].CityName],
					postalCode=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].PostalCode],
					stateProv=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].StateProv],
					country=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].CountryNameCode],
					holderType=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].HolderType]
				)
				using (address=getAddressDOCA(addressLine))
			({
				Remark: lookupMap.Remark[remark] when commonLib.isNotEmpty(remark) otherwise null,
				StreetNmbr: {
					Content: address.streetNbr
				} when commonLib.isNotEmpty(address) and commonLib.isNotEmpty(address.streetNbr) otherwise null,
				AddressLine: [address.addressLine] when commonLib.isNotEmpty(address) and commonLib.isNotEmpty(address.addressLine) otherwise null,
				CityName: commonLib.mapIfExists(city),
				PostalCode: commonLib.mapIfExists(postalCode),
				StateProv: {
					Content: stateProv
				} when commonLib.isNotEmpty(stateProv) otherwise null,
				CountryName: {
					Code:  isoCountryFn(country)
				} when commonLib.isNotEmpty(country) otherwise null
			} when matchDocs(ssrType, ptc, null, holderType) otherwise {}) when commonLib.isArrayNotEmpty(DOCA.serviceRequest.ssr.freeText) otherwise {})
			++
	 		(structureAddrObjArray default [] map using (addr=$){
				StreetNmbr: {
					PO_Box: addr.POBox
				} when commonLib.isNotEmpty(addr.POBox) otherwise null,
				AddressLine: [addr.Address1 ++  " " ++ addr.Address2] when commonLib.isNotEmpty(addr.Address1) and  commonLib.isNotEmpty(addr.Address2) otherwise 
								[addr.Address1] when commonLib.isNotEmpty(addr.Address1) otherwise null,
				CityName: addr.City when commonLib.isNotEmpty(addr.City) otherwise null,
				PostalCode: addr.Zip when commonLib.isNotEmpty(addr.Zip) otherwise null,
				(StateProv: {
					Content: addr.State
				}) when commonLib.isNotEmpty(addr.State),
				CountryName: {
					Code: isoCountryFn(addr.Country) 	
				} when commonLib.isNotEmpty(addr.Country) otherwise null
			} when commonLib.isArrayNotEmpty(structureAddrObjArray) otherwise {})
	) filter $ != {}
) 

%function determineMatchingElmForDocs(dataElm,ptcType)
using (matchedElms = dataElm filter using (gender=extractWord($.serviceRequest.ssr.freeText[0],5)) (matchDocs("DOCS", ptcType, gender, null)))
(
	matchedElms when commonLib.isArrayNotEmpty(matchedElms) otherwise dataElm

)

%function matchDocs(ssrType, ptc, gender, holderType)
true when ssrType=="DOCS" and ptc == "INF" and (gender == "MI" or gender == "FI")
	otherwise 
		true when ssrType=="DOCS" and ptc != "INF" and (gender == null or gender == "M" or gender == "F")
		otherwise
			true when (ssrType == "DOCO" or ssrType == "DOCA") and ptc == "INF" and holderType != null and holderType == "I"
			otherwise 
				true when (ssrType == "DOCO" or ssrType == "DOCA") and ptc != "INF" and (holderType == null or holderType == "H")
				otherwise false
 
%function mapAirTraveller(paxData,dataElmByType, isoCountryFn)
using (ptc = paxData.travellerInformation.passenger[0].type,
	custLoyalty = getElmFromDataElmBucket(dataElmByType.custLoyalty, paxData),
	ssrDOCS = getElmFromDataElmBucket(dataElmByType.DOCS, paxData),
	ssrDOCO = getElmFromDataElmBucket(dataElmByType.DOCO, paxData),
	PCTC = 	getElmFromDataElmBucket(dataElmByType.PCTC, paxData),
	isAccompanyInfant = true when (paxData.travellerInformation.passenger.*infantIndicator[0] != null 
				and (paxData.travellerInformation.passenger.*infantIndicator[0] == "1" 
					or paxData.travellerInformation.passenger.*infantIndicator[0] == "2" 
					or paxData.travellerInformation.passenger.*infantIndicator[0] == "3"))
							otherwise false,
	firstName = paxData.travellerInformation.passenger.*firstName[0] splitBy " "	
)
using (email = getContactDataElms(dataElmByType.Email, paxData,ptc),
	phone = getContactDataElms(dataElmByType.Phone, paxData,ptc),
	addresses = mapAddresses(paxData, dataElmByType, isoCountryFn,ptc),
	ssrDOCSMatchElem = determineMatchingElmForDocs(ssrDOCS,ptc) when commonLib.isArrayNotEmpty(ssrDOCS)  otherwise []
)
using (	
  	ssrDOCSDOCO = (ssrDOCSMatchElem when commonLib.isArrayNotEmpty(ssrDOCSMatchElem)  otherwise []) 
  						++ (ssrDOCO when commonLib.isArrayNotEmpty(ssrDOCO)  otherwise []),
 	pctcHyphenTokens=PCTC[0].serviceRequest.ssr.freeText[0] splitBy '-' when commonLib.isArrayNotEmpty(PCTC) and PCTC[0].serviceRequest.ssr.freeText? otherwise null
 )
{
	PersonName: {
		NamePrefix: [firstName[2]] when (sizeOf firstName) == 3 
						otherwise 
							[firstName[1]] when (sizeOf firstName) == 2
						otherwise null,
		GivenName: [firstName[0]] when (sizeOf firstName) > 0 otherwise null,
		NameSuffix: [firstName[1]] when (sizeOf firstName) == 3 otherwise null,
		Surname: paxData.travellerInformation.traveller.surname
	},
	
	Email: (email map {
		Content: parseEmailFreeText($.otherDataFreetext.longFreetext[0]) 
	}) when commonLib.isArrayNotEmpty(email)  otherwise null,
	
	Telephone: (phone map 
		using (phoneNumber = parsePhoneFreeText($.otherDataFreetext.longFreetext[0]) when ($.otherDataFreetext.longFreetext?) otherwise null) 
		{ 
			CountryAccessCode: phoneNumber.CountryAccessCode when phoneNumber != null and commonLib.isNotEmpty(phoneNumber.CountryAccessCode) otherwise null,
			AreaCityCode: phoneNumber.AreaCityCode when phoneNumber != null and commonLib.isNotEmpty(phoneNumber.AreaCityCode) otherwise null,
			PhoneNumber: phoneNumber.PhoneNumber when phoneNumber != null and commonLib.isNotEmpty(phoneNumber.PhoneNumber) otherwise null,
			PhoneLocationType: ($.otherDataFreetext.freetextDetail.type)[0]
		}) when commonLib.isArrayNotEmpty(phone)  otherwise null,
	
	(Address: addresses) when commonLib.isArrayNotEmpty(addresses),
	
	TravelerRefNumber: {
		RPH: paxData.travellerRPH as :string
	},
	
	PassengerTypeCode: ptc,
	BirthDate: getBirthDate(paxData.dateOfBirth.dateAndTimeDetails.date,  ssrDOCSMatchElem),
	Gender: lookupMap.Gender[extractWord ((flatten ssrDOCSMatchElem.serviceRequest.ssr.freeText)[0],lookupMap.freeTextIndexMap[ssrDOCSMatchElem.serviceRequest.ssr.type[0]].Gender)],
	AccompaniedByInfantInd: isAccompanyInfant,
	
	CustLoyalty: ([{
		ProgramID: custLoyalty[0].frequentFlyerInformationGroup.frequentTravellerInfo.frequentTraveler.company,
		MembershipID: custLoyalty[0].frequentFlyerInformationGroup.frequentTravellerInfo.frequentTraveler.membershipNumber,
		LoyalLevel: custLoyalty[0].frequentFlyerInformationGroup.frequentTravellerInfo.frequentTraveler.tierLevel
	}]) when commonLib.isArrayNotEmpty(custLoyalty) and ptc != "INF" otherwise null,
	
	Document: (ssrDOCSDOCO map using (ssrType = $.serviceRequest.ssr.type,
				ssrFreeText = $.serviceRequest.ssr.freeText[0] splitBy "/"
			) using (
				birthDate = ssrFreeText[lookupMap.freeTextIndexMap[ssrType].BirthDate],
				docHolderNationality = ssrFreeText[lookupMap.freeTextIndexMap[ssrType].DocHolderNationality],
				docID=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].DocID],
				docIssueCountry=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].DocIssueCountry],
				expireDate=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].ExpireDate],
				gender=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].Gender],
				holderType=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].HolderType],
				acDocType=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].AC_DocType],
				name=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].DocHolderName],
				additionPersonName1=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].AdditionalPersonName0],
				additioPersonName2=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].AdditionalPersonName1],
				birthPlace=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].BirthPlace],
				docIssueLoc=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].DocIssueLocation],
				effectiveDate=ssrFreeText[lookupMap.freeTextIndexMap[ssrType].EffectiveDate]
			 ) using (
			 	isoCntry=isoCountryFn(docIssueCountry) when commonLib.isNotEmpty(docIssueCountry) otherwise null
			 ) ({
			 		BirthDate: commonLib.formatDate(birthDate,"ddMMMyy","yyyy-MM-dd") when commonLib.isNotEmpty(birthDate) otherwise null,
					DocHolderNationality: isoCountryFn(docHolderNationality) when commonLib.isNotEmpty(docHolderNationality) otherwise null,
					DocID: commonLib.mapIfExists(docID),
					DocIssueCountry: isoCntry,
					ExpireDate: commonLib.formatDate(expireDate,"ddMMMyy","yyyy-MM-dd") when commonLib.isNotEmpty(expireDate) otherwise null,
					Gender: lookupMap.Gender[gender] when commonLib.isNotEmpty(gender) otherwise null,
					HolderType: lookupMap.HolderType[holderType] when commonLib.isNotEmpty(holderType) otherwise null,
					AC_DocType:  lookupMap.AC_DocType[extractAcDocTypeWithCondition(acDocType,isoCntry)]  when commonLib.isNotEmpty(acDocType) otherwise null,
					DocHolderName: commonLib.mapIfExists(name),
					AdditionalPersonNames: getDocAdditionalPersonNames(additionPersonName1, additioPersonName2),
					BirthPlace: commonLib.mapIfExists(birthPlace),
					DocIssueLocation: commonLib.mapIfExists(docIssueLoc),
					EffectiveDate: commonLib.formatDate(effectiveDate,"ddMMMyy","yyyy-MM-dd")	 when commonLib.isNotEmpty(effectiveDate) otherwise null
				} when matchDocs(ssrType, ptc, gender,holderType) otherwise null) when commonLib.isArrayNotEmpty(ssrDOCSDOCO.serviceRequest.ssr.freeText) otherwise null),
		
	AC_EmergencyContactInfo: (using (names=pctcHyphenTokens[0] splitBy " " when commonLib.isNotEmpty(pctcHyphenTokens[0]) otherwise []){
		PersonName: { //last token will be surName, and all other names will concatenated to the GivenName. 
			//Lola Ren Bunny-CA9283983 -- GivenName: Lola Ben, SurName: Bunny
			//Taz-US20393922039 -- GivenName: null, SurName: Taz
			GivenName: [names[0]] when (commonLib.isArrayNotEmpty(names) and ((sizeOf names) == 2)) otherwise
							[commonLib.buildStringWithDelimiter(names[0..-2], " ", true)] when (commonLib.isArrayNotEmpty(names) and ((sizeOf names) > 2))
							otherwise null,
			Surname: names[0] when commonLib.isArrayNotEmpty(names) and (sizeOf names) == 1 
						otherwise names[-1] when commonLib.isArrayNotEmpty(names) and (sizeOf names) > 1 otherwise null
		} when (sizeOf pctcHyphenTokens) > 1 otherwise null,
		CountryCode: pctcHyphenTokens[1][0..1] when (sizeOf pctcHyphenTokens) > 1 and (sizeOf pctcHyphenTokens[1]) > 2 otherwise null,
		Telephone: {
			PhoneNumber: pctcHyphenTokens[1][2..-1]
		} when (sizeOf pctcHyphenTokens) > 1 otherwise null
	}) when commonLib.isArrayNotEmpty(pctcHyphenTokens)  and ptc != "INF" otherwise null
}

%function extractAcDocTypeWithCondition(acDocType,docIssueCountry)
( 
	"USP" when acDocType=="P" and docIssueCountry=="US" otherwise  
		"USC" when acDocType=="C" and docIssueCountry=="US" otherwise 
			"C" when acDocType=="C" and docIssueCountry=="CA" otherwise
				acDocType 
)

%function mapAirTravellers(paxDatas, dataElmByType, isoCountryFn)
	flatten paxDatas map (mapAirTraveller($, dataElmByType, isoCountryFn)) when paxDatas != null otherwise null
	
	
%function mapSSRs(dataElmByType)
	dataElmByType.SSRs map 
		using (ssr=$.serviceRequest.ssr) 
		{
			Number: pnrReplycommonLib.getElementManagementReference($, "OT", "DataElm"),
					
			FlightRefNumberRPHList: pnrReplycommonLib.getRefsByQualifier($, "ST", "DataElm"), 			
					
			TravelerRefNumberRPHList: pnrReplycommonLib.getRefsByQualifier($, "PT", "DataElm"),
			
			"AC_Type": "SSR",
					
			SSRCode: ssr.type when ssr.type? otherwise null,
			
			ServiceQuantity: ssr.quantity as :number when ssr.quantity? otherwise null,
			
			Status: ssr.status when ssr.status? and commonLib.isNotEmpty((trim ssr.status)) otherwise null,
			
			Airline: { // TBA question.. request mapping is done from CompanyShortName
				Code: ssr.companyId
			} when ssr.companyId? otherwise null,
      Text: using (freeTextAry = ssr.freeText when ssr.freeText? and ssr.freeText is :array otherwise [ssr.freeText])
                (commonLib.buildStringWithDelimiter(freeTextAry, "/", true) 
                    when commonLib.isArrayNotEmpty(freeTextAry) otherwise null)
		} when dataElmByType != null and commonLib.isArrayNotEmpty(dataElmByType.SSRs) otherwise []	

%function mapSeats(dataElmByType)
	(flatten (dataElmByType.Seats map (Seat) -> (
		Seat.serviceRequest.ssrb map (ssrb)  -> 
		using (
				refNumber=pnrReplycommonLib.getElementManagementReference(Seat, "OT", "DataElm"),
				seatPrefs = ssrb.seatType default [] filter ($ == "A" or $ == "W")
			) {
			(SeatNumber: ssrb.data) when commonLib.isNotEmpty(ssrb.data),
			
			(SeatPreference: seatPrefs) when commonLib.isArrayNotEmpty(seatPrefs),
			
			TravelerRefNumberRPHList: [ssrb.crossRef] when ssrb.crossRef? otherwise null,
			 
			FlightRefNumberRPHList: pnrReplycommonLib.getRefsByQualifier(Seat, "ST", "DataElm"),
			  
			Offers: [{ 
				ID: refNumber as :string	
			}] when commonLib.isNotEmpty(refNumber) otherwise null
		}
	))) when dataElmByType != null and commonLib.isArrayNotEmpty(dataElmByType.Seats) otherwise null
	
%function mapSVCs(svcItins)
	svcItins map {
		Number: pnrReplycommonLib.getElementManagementReference($, "ST", "Itin"),
								
		TravelerRefNumberRPHList: pnrReplycommonLib.getRefsByQualifier($, "PT", "Itin"),
				
		SSRCode: $.travelProduct.productDetails.identification when $.travelProduct.productDetails.identification? otherwise null,
		
		ServiceQuantity: $.relatedProduct.quantity as :number when $.relatedProduct.quantity? otherwise null,
		
		Status: $.relatedProduct.status[0] when $.relatedProduct.status? otherwise null,
		
		"AC_Type": "SVC",
		
		(Airline: { // TBA question.. request mapping is done from CompanyShortName
			Code: $.travelProduct.companyDetail.identification
		}) when $.travelProduct.companyDetail.identification?,
		
		(FlightLeg: {
			(Date: commonLib.formatDate($.travelProduct.product.depDate, "ddMMyy", "yyyy-MM-dd")) when $.travelProduct.product.depDate?,
			(DepartureAirport: {
				LocationCode: $.travelProduct.boardpointDetail.cityCode
			}) when commonLib.isNotEmpty($.travelProduct.boardpointDetail.cityCode),
			(ArrivalAirport: {
				LocationCode: $.travelProduct.offpointDetail.cityCode
		
			}) when commonLib.isNotEmpty($.travelProduct.offpointDetail.cityCode)	
		}) when commonLib.isNotEmpty($.travelProduct.product.depDate) 
					or commonLib.isNotEmpty($.travelProduct.boardpointDetail.cityCode) or commonLib.isNotEmpty($.travelProduct.offpointDetail.cityCode),
					
		(Text: $.itineraryFreetext.longFreetext) when commonLib.isNotEmpty($.itineraryFreetext.longFreetext)
	} when commonLib.isArrayNotEmpty(svcItins) otherwise []
	
%function mapOtherServiceInfos(OtherServiceInfos)
OtherServiceInfos default [] map using (
		PTRefs=pnrReplycommonLib.getRefsByQualifier($, "PT", "DataElm"),
		STRefs=pnrReplycommonLib.getRefsByQualifier($, "ST", "DataElm"),
		rph=pnrReplycommonLib.getElementManagementReference($, "OT", "DataElm"),
		ssrType=$.serviceRequest.ssr.type,
		segmentName=$.elementManagementData.segmentName
	) 
	using (airlineCode=$.serviceRequest.ssr.companyId when ($.serviceRequest.ssr.companyId? and segmentName == "SK") 
					otherwise 
						$.otherDataFreetext[0].freetextDetail.companyId when ($.otherDataFreetext[0].freetextDetail.companyId? and segmentName == "OS")
						otherwise null,
			text=commonLib.buildStringWithDelimiter($.serviceRequest.ssr.freeText, "/", true) when commonLib.isArrayNotEmpty($.serviceRequest.ssr.freeText) 
						 and segmentName == "SK"
					otherwise 
						$.otherDataFreetext[0].longFreetext when $.otherDataFreetext[0].longFreetext? and segmentName == "OS"
						otherwise null
		)
	{
	(RPH: (rph as :string)) when commonLib.isNotEmpty(rph),
		
	(Code: ssrType) when commonLib.isNotEmpty(ssrType),
	
	(TravelerRefNumber: PTRefs map {
		RPH: $ as :string	
	}) when commonLib.isArrayNotEmpty(PTRefs),
	
	(FlightRefNumber: STRefs map {
		RPH: $ as :string
	}) when commonLib.isArrayNotEmpty(STRefs),
	
	(Airline: { 
		Code: airlineCode
	}) when commonLib.isNotEmpty(airlineCode),
	
	(Text: text) when commonLib.isNotEmpty(text)
}

%function mapRemarks(dataElmByType)
	dataElmByType.MiscRemarks map using (rph= pnrReplycommonLib.getElementManagementReference($, "OT", "DataElm")){
		(RPH: (rph as :string)) when commonLib.isNotEmpty(rph),
		(Content: $.miscellaneousRemarks.remarks.freetext) when $.miscellaneousRemarks.remarks.freetext?  
	}
	
%function mapSpecialRemarks(dataElmByType)
	dataElmByType.SpecialRemarks map using (rph= pnrReplycommonLib.getElementManagementReference($, "OT", "DataElm"),
		splRemark=$.miscellaneousRemarks.remarks,
		PTRefs=pnrReplycommonLib.getRefsByQualifier($, "PT", "DataElm"),
		STRefs=pnrReplycommonLib.getRefsByQualifier($, "ST", "DataElm")		
	){
		(RPH: (rph as :string)) when commonLib.isNotEmpty(rph),
		(RemarkType: splRemark.type) when splRemark.type?,
		(TravelerRefNumber: PTRefs map {
			RPH: $ as :string	
		}) when commonLib.isArrayNotEmpty(PTRefs),
	
		(FlightRefNumber: STRefs map {
			RPH: $ as :string
		}) when commonLib.isArrayNotEmpty(STRefs),
				
		(Text: splRemark.freetext) when splRemark.freetext?,
		
		(AuthorizedViewers: $.miscellaneousRemarks.individualSecurity map {
			ViewerCode: $.office
		}) when commonLib.isArrayNotEmpty($.miscellaneousRemarks.individualSecurity)
	}

%function mapSpecialReqDetails(dataElmByType, itinInfo)
using (SVCItins = itinInfo default [] filter $.elementManagementItinerary.segmentName == "IU")
using (SSRs = mapSSRs(dataElmByType),
		SVCs = mapSVCs(SVCItins)
)
([{
		
	SeatRequests: mapSeats(dataElmByType), 
	
	(SpecialServiceRequests: (SSRs ++ SVCs)) when SSRs != [] or SVCs != [],
	
	(OtherServiceInformations: {
		OtherServiceInformation: (mapOtherServiceInfos(dataElmByType.SK) ++ mapOtherServiceInfos(dataElmByType.OS))
		}) when commonLib.isArrayNotEmpty(dataElmByType.SK) or commonLib.isArrayNotEmpty(dataElmByType.OS),
		
	(Remarks: mapRemarks(dataElmByType)) when commonLib.isArrayNotEmpty(dataElmByType.MiscRemarks),
	
	(SpecialRemarks: {
		SpecialRemark: mapSpecialRemarks(dataElmByType)
	}) when commonLib.isArrayNotEmpty(dataElmByType.SpecialRemarks)
		
}] when dataElmByType != null 
		and (commonLib.isArrayNotEmpty(dataElmByType.SSRs) or commonLib.isArrayNotEmpty(dataElmByType.Seats) or commonLib.isArrayNotEmpty(SVCs) 
			or commonLib.isArrayNotEmpty(dataElmByType.SK) or commonLib.isArrayNotEmpty(dataElmByType.OS)
			or commonLib.isArrayNotEmpty(dataElmByType.MiscRemarks) or commonLib.isArrayNotEmpty(dataElmByType.SpecialRemarks)
			) otherwise null)// SpecialReqDetails		

%function mapTravelerInfo(paxDatas, dataElmByType, pnrReply, isoCountryFn)
{
	AirTraveler: mapAirTravellers(paxDatas, dataElmByType, isoCountryFn),

	SpecialReqDetails: mapSpecialReqDetails(dataElmByType, 
								flatten (pnrReply.originDestinationDetails.itineraryInfo) default [])
		
} when commonLib.isArrayNotEmpty(paxDatas) otherwise null // TravellerInfo

---
{
	mapTravelerInfo: mapTravelerInfo
}