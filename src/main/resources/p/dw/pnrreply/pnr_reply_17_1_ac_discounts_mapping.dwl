%dw 1.0
%output application/json skipNullOn="everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")

%var listTSMRes = flowVars.ticketRetrieveListOfTSMResponse_15_1.Ticket_RetrieveListOfTSMReply..serviceTattoos 
%var catalogRes = flowVars.serviceIntegratedCatalogue18_1Response.Service_IntegratedCatalogueReply.serviceGroup
%var pricingRes = flowVars.serviceIntegratedPricing18_1Response.Service_IntegratedPricingReply
%var updateTSMPRes = flowVars.ticketUpdateTSMP14_1Response.Ticket_UpdateTSMPReply
%var pnrReply = flowVars.pnrAddMultiElements17_1CommitResponse.PNR_Reply
%var upgradeCabin = {
  R : "Business",
  N : "Premium"
}
%function findPnrElement(pnrReply,type,number)
pnrReply.dataElementsMaster.dataElementsIndiv filter $.serviceRequest.ssr.type? and $.elementManagementData.reference.number ~= number when (type == "SSR" or type == "SEA")
otherwise  (flatten pnrReply.originDestinationDetails.itineraryInfo) filter pnrReply.originDestinationDetails.itineraryInfo? and $.elementManagementItinerary.reference.number ~= number
%function attrValueMatches(attrDetails,value)
	 [] != (attrDetails filter ($.attributeType == "SRV" and $.attributeDescription ~= value))
%function findMatchPricing(pricingRes,value)
  pricingRes.allFaresInfoGroup filter using(pricingAttrDetails = flatten $..servicePricingAggregation.*attributeDetails)
 		pricingRes != null and pricingAttrDetails != null and attrValueMatches(pricingAttrDetails,value)
  when pricingRes!=null otherwise []
 		
%function findMatchingTSM(updateTSMPRes,value)
	using (refDetail = updateTSMPRes.detailsOfTSM.couponsDetails.productTattoo.referenceDetails)
	(
	  updateTSMPRes.detailsOfTSM when refDetail != null and refDetail.value ~= value
		otherwise null
		
	)
%function mapDiscountFareFromPricing(faresInfos)
using (fareDetail = (flatten faresInfos.monetaryInfo.monetaryDetails filter $.typeQualifier == "B")[0] when faresInfos.monetaryInfo.monetaryDetails != null otherwise null,
  taxDetail = (flatten faresInfos.monetaryInfo.monetaryDetails filter $.typeQualifier == "TX")[0] when faresInfos.monetaryInfo.monetaryDetails != null otherwise null
)
{
 
	Amount: fareDetail.amount as :number
	,CurrencyCode: fareDetail.currency
	,DecimalPlaces: commonLib.getDecimalPlaces(fareDetail.amount)
	,Taxes: {
	  Amount: taxDetail.amount as :number
	  ,Tax: (flatten faresInfos..computedTaxListInfo..taxDetails) map {
	    		Amount: $.rate as :number,
	    		TaxCode: $.countryCode,
	    		TaxCountry: $.type    
	  } when faresInfos..computedTaxListInfo..taxDetails != null otherwise null
	}
}
%function mapDiscountFareFromUpdateTSMP(detailsOfTSM)
using (fareDetail = (detailsOfTSM.monetaryInformation filter $.monetaryDetails.typeQualifier == "F")[0].monetaryDetails,
  totalDetail = (detailsOfTSM.monetaryInformation filter $.monetaryDetails.typeQualifier == "T")[0].monetaryDetails
)
{
	Amount: fareDetail.amount as :number
	,CurrencyCode: fareDetail.currency
	,DecimalPlaces: commonLib.getDecimalPlaces(fareDetail.amount)
	,Taxes: {
	  Amount: totalDetail.amount - fareDetail.amount
	  ,Tax: detailsOfTSM.taxInformation map {
	    		Amount: $.taxDetails.rate as :number,
	    		TaxCode: $.taxDetails.countryCode,
	    		TaxCountry: $.taxDetails.type
	  }
	}
}

%function mapFromListOfTSM(tsms)
(flatten tsms map 
  using (type = $.referenceDetails.type,
    		 value = $.referenceDetails.value
  )
  using (pnrElements=findPnrElement(pnrReply,type,value))
  using (ssrElement = pnrElements[0] when commonLib.isArrayNotEmpty(pnrElements) and type == "SSR" or type == "SEA" otherwise null,
    		 svcElement = pnrElements[0] when commonLib.isArrayNotEmpty(pnrElements) and type == "SVC" otherwise null
  )
  {
  	  ID: $.referenceDetails.value,
  	  Discount_Name:ssrElement.serviceRequest.ssr.type when ssrElement!=null
  	  	otherwise svcElement.travelProduct.productDetails.identification,
  	  TravelerRPH: pnrReplycommonLib.getRefsByQualifier(ssrElement, "PT", "DataElm")[0] when ssrElement!=null
  	  	otherwise pnrReplycommonLib.getRefsByQualifier(svcElement, "PT", "Itin")[0],
  	  FlightSegmentReferences: pnrReplycommonLib.getRefsByQualifier(ssrElement, "ST", "DataElm") map {FlightSegmentReference: {RPH:$}} when ssrElement!=null otherwise null
  	  ,SeatCharacteristics: ssrElement.seatPaxInfo[0].seatPaxDetails.genericDetails.seatCharacteristic map {
  	    		SeatCharacteristic: {
  	    		  	Content: $  	    		  
  	    		  }
  	    } when ssrElement.seatPaxInfo.seatPaxDetails.genericDetails.seatCharacteristic? otherwise null
  	  ,DiscountLocation: {
  	   	Content: svcElement.itineraryInfo.travelProduct.boardpointDetail.cityCode
  	   } when svcElement.itineraryInfo.travelProduct.boardpointDetail.cityCode? otherwise null
  	  ,DiscountFare: mapDiscountFareFromPricing(findMatchPricing(pricingRes,value)) when findMatchPricing(pricingRes,value) != []
  	  	otherwise mapDiscountFareFromUpdateTSMP(findMatchingTSM(updateTSMPRes,value)) when findMatchingTSM(updateTSMPRes,value) != null
  	  		otherwise null
  }) when commonLib.isArrayNotEmpty(tsms)
  		otherwise {}
%function mapDiscountFareFromCatalog(pricingGroup)
using (moneDetails = pricingGroup.computedTaxSubDetails.otherMonetaryDetails)
using (fareDetail = (moneDetails filter $.typeQualifier == "B")[0] when moneDetails!=null otherwise null)
using (taxDetail = (moneDetails filter $.typeQualifier == "TX")[0] when moneDetails!=null otherwise null)
(
  {
	Amount: fareDetail.amount as :number
	,CurrencyCode: fareDetail.currency
	,DecimalPlaces: commonLib.getDecimalPlaces(fareDetail.amount)
	,Taxes: {
	  Amount: taxDetail.amount as :number
//	  ,Tax: detailsOfTSM.taxInformation map {
//	    		Amount: $.taxDetails.rate,
//	    		TaxCode: $.taxDetails.countryCode,
//	    		TaxCountry: $.taxDetails.type
//	  }
	}
  } when fareDetail!=null otherwise null

)
%function mapFromCatalog(serviceGroups)
(flatten (
  serviceGroups map (serviceGroup) -> (serviceGroup.pricingGroup map  
    using (designator = serviceGroup.quotaGroup[0].bookingClassUpgrade[0].bookingClassDetails[0].designator,
      	   segmentList = $.couponInfoGroup..segmentCouponReference..referenceDetails..value,
      	   PT=$.passengerReference.referenceDetails[0].value
    )
    {
  	  Discount_Name: serviceGroup.serviceDetailsGroup[0].serviceDetails.specialRequirementsInfo.ssrCode,
  	  SpecialCondition: serviceGroup.serviceCodes.otherSpecialCondition,
  	  Discount_Type: serviceGroup.serviceDetailsGroup[0].serviceDetails.specialRequirementsInfo.serviceType,
  	  TravelerRPH: PT,
  	  FlightSegmentReferences: (segmentList map {FlightSegmentReference: {RPH:$}}),
  	  
  	  
  	  AC_UpgradeCabin: upgradeCabin[designator]
  	  ,DiscountFare: mapDiscountFareFromCatalog($)
  	}))
) when commonLib.isArrayNotEmpty(serviceGroups)
	otherwise {}
%var discounts = mapFromListOfTSM(listTSMRes) ++  mapFromCatalog(catalogRes)
---
{
  AC_Discounts: discounts
} when discounts != {} otherwise {}


																		