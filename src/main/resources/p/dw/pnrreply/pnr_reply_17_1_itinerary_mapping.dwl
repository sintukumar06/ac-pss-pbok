%dw 1.0

%var arrivalUnknownSegment = "ARNK"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

	
%function getOperatingAirline(itineraryfreeFormTextList)
	using (opFF =(itineraryfreeFormTextList[0].freeText filter ($ contains "OPERATED BY"))
		when commonLib.isArrayNotEmpty(itineraryfreeFormTextList) otherwise null)
	(
		using (opAC = (opFF[0] scan /OPERATED\sBY\s([a-zA-Z]{2})(.*)\s(.*)/ 
							when not (opFF[0] contains "OPERATED BY SUBSIDIARY/FRANCHISE") otherwise ["NULL"]
						)[0] when commonLib.isNotEmpty(opFF) otherwise null,
				opCN = (opFF[1] scan /OPERATED\sBY\s(.*)/
						when opFF[0] contains "OPERATED BY SUBSIDIARY/FRANCHISE" otherwise ["NULL"]
						)[0] when commonLib.isNotEmpty(opFF) otherwise null
					
		)
		(
			using (opACText = opAC[1] when commonLib.isNotEmpty(opAC) and opAC != "NULL" otherwise null,
					opCNText = opCN[1] when commonLib.isNotEmpty(opCN) and opCN != "NULL" otherwise null
			)
			
			{
				Code: (opACText when commonLib.isNotEmpty(opACText) otherwise null) when commonLib.isEmpty(opCNText) otherwise null,
				CompanyShortName: opCNText when commonLib.isNotEmpty(opCNText) otherwise null
			}
		)
	)


%function removeDuplicates(arr)
	arr reduce (obj, acc=[]) -> (
		(acc + obj) when not (acc contains obj)
		otherwise 
		  acc
	)
	
%function getBoundTattoos(allItinTattoos, cnxTattoos)
	allItinTattoos map (itinTattoo)  -> using (cnxTattoo=getCNXTattoo(cnxTattoos,itinTattoo as :string))
	(
		cnxTattoo when cnxTattoo != [] otherwise itinTattoo as :string
	)	

%function getCNXTattoo(cnxTattoos, tattooStr)
	cnxTattoos reduce (obj, acc=[]) -> (
		obj when obj contains tattooStr otherwise acc
	)		

%function mapAirItinerary (pnrReply)
using (itineraryInfos = (flatten pnrReply.originDestinationDetails.itineraryInfo) default [] 
		filter ($.elementManagementItinerary.segmentName == "AIR" and
			    $.travelProduct.productDetails.identification != arrivalUnknownSegment // ARNK
		),
	cnxSegments = pnrReply.segmentGroupingInfo filter $.groupingCode == "CNX" default [],
	fareCompGrps=pnrReply.pricingRecordGroup.productPricingQuotationRecord.documentDetailsGroup.fareComponentDetailsGroup 
		when commonLib.isArrayNotEmpty(pnrReply.pricingRecordGroup.productPricingQuotationRecord.documentDetailsGroup.fareComponentDetailsGroup)
		otherwise []
)
using (allItinTattoos = itineraryInfos.elementManagementItinerary.reference.number default [], 
	cnxTattoos = (cnxSegments..marriageDetail map $.tatooNum) when cnxSegments != [] otherwise [],
	itinByTattoos = itineraryInfos groupBy $.elementManagementItinerary.reference.number when itineraryInfos != [] otherwise null
)
using (boundTattooGroups = removeDuplicates(getBoundTattoos(allItinTattoos, cnxTattoos) when cnxTattoos != [] otherwise allItinTattoos)
//(allItinTattoos -- flatten cnxTattoos) ++ cnxTattoos	
)

({	
	OriginDestinationOptions: boundTattooGroups map (boundTattooGrp, boundTattooGrpIdx) -> {
		RPH: (boundTattooGrpIdx+1) as :string,
		FlightSegment: 
				(boundTattooGrp map (flightTattoo)  -> using (itin=itinByTattoos[flightTattoo as :string]) 
					(mapFlightSegment(itin[0],fareCompGrps))
				) when boundTattooGrp is :array 
					otherwise using (itin=itinByTattoos[boundTattooGrp as :string])
					[
						(mapFlightSegment(itin[0],fareCompGrps))
					]							
	}
} when commonLib.isArrayNotEmpty(boundTattooGroups) otherwise null)

%function mapFlightSegment(itinInfo,fareCompGrps)
using (opAirline= getOperatingAirline(itinInfo.itineraryfreeFormText 
					filter $.freeTextQualification.textSubjectQualifier == "3" 
					when itinInfo.itineraryfreeFormText != null otherwise null),
	 fareCompGrp = (flatten fareCompGrps default [] filter ( ($ != null) and (($.couponDetailsGroup.productId.referenceDetails.type contains "ST")
	 					and ($.couponDetailsGroup.productId.referenceDetails.value contains itinInfo.elementManagementItinerary.reference.number as :string)))
	 					) when commonLib.isArrayNotEmpty(fareCompGrps) otherwise null
	 	)
{
	
	DepartureAirport: {
		LocationCode: itinInfo.travelProduct.boardpointDetail.cityCode,
		(Terminal: itinInfo.flightDetail.departureInformation.departTerminal) when itinInfo.flightDetail.departureInformation.departTerminal?
	},
	ArrivalAirport: {
		LocationCode:itinInfo.travelProduct.offpointDetail.cityCode,
		(Terminal: itinInfo.flightDetail.arrivalStationInfo.terminal) when itinInfo.flightDetail.arrivalStationInfo.terminal?
	},
	Equipment: [{
		AirEquipType: itinInfo.flightDetail.productDetails.equipment
	}],
	DepartureDateTime: commonLib.createOTATS(itinInfo.travelProduct.product.depDate, itinInfo.travelProduct.product.depTime),
	ArrivalDateTime: commonLib.createOTATS(itinInfo.travelProduct.product.arrDate, itinInfo.travelProduct.product.arrTime),
	StopQuantity: itinInfo.flightDetail.productDetails.numOfStops,
	RPH:  itinInfo.elementManagementItinerary.reference.number as :string,
	DepartureDay: commonLib.getAlteaDayOfWeek(itinInfo.flightDetail.productDetails.weekDay),
	(DateChangeNbr: itinInfo.travelProduct.product.dayChangeIndicator as :string) when itinInfo.travelProduct.product.dayChangeIndicator?,
	MarketingAirline: {
		Code: itinInfo.travelProduct.companyDetail.identification
	},
	OperatingAirline: opAirline when opAirline != null and (commonLib.isNotEmpty(opAirline.Code) or commonLib.isNotEmpty(opAirline.CompanyShortName)) otherwise null, 
	NumberInParty: itinInfo.relatedProduct.quantity,					
	FlightNumber: itinInfo.travelProduct.productDetails.identification,
	FareBasisCode: fareCompGrp.componentClassInfo.fareBasisDetails.rateTariffClass[0] when commonLib.isArrayNotEmpty(fareCompGrp.componentClassInfo.fareBasisDetails.rateTariffClass) otherwise null,
	ResBookDesigCode: itinInfo.travelProduct.productDetails.classOfService,
	Status: itinInfo.relatedProduct.status[0]
}

---
{
	mapAirItinerary: mapAirItinerary
}