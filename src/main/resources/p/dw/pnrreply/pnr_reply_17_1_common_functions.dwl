%dw 1.0

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%var nonSSRList = ["RQST", "NSST", "DOCS","DOCO","DOCA","PCTC", "FQTV"]

%function getRefsByQualifier(dataElm, qualifier, type)
  using (refDataElm = (dataElm.referenceForDataElement.reference when type=="DataElm"
  		otherwise 
  			dataElm.referenceForSegment.reference when type=="Itin"
  			otherwise null) default [] filter $.qualifier==qualifier) 
  	(refDataElm.number when commonLib.isArrayNotEmpty(refDataElm) otherwise null)
	
%function getElementManagementReference(dataElm, qualifier, type)
using (refData= dataElm.elementManagementItinerary.reference when type=="Itin" 
						otherwise 
							dataElm.elementManagementData.reference when type=="DataElm" otherwise null
)
	(refData.number when refData.number? 
					and refData.qualifier? and refData.qualifier == qualifier otherwise null)

//Creating paxDatas by adding passenger tattoo from the parent travellerInfo
//traverllerInfo[].passengerData[]
%function getPaxDatas(pnrReply)  
	pnrReply.travellerInfo map (travellerInfo) -> ( 
		travellerInfo.passengerData map (paxData) -> (
			(paxData ++ {
				travellerRPH: travellerInfo.elementManagementPassenger.reference.number
			})
		)
	) when commonLib.isNotEmpty(pnrReply.travellerInfo)  otherwise null


%function createDataElmBuckets(pnrReply)
	pnrReply.dataElementsMaster.dataElementsIndiv reduce (dataElm, acc= {
	Phone: [],
	Email: [],
	custLoyalty: [],
	SK: [],
	OS: [],
	MiscRemarks: [],
	SpecialRemarks: [],
	Seats: [],
	PCTC: [],
	DOCA: [],
	StructuredAddress: [],
	DOCS: [],
	DOCO: [],
	SSRs: [],
	FTs:[],
	FA: [],
	OPC: [],
	FO: [],
	FP: [],
	Endorsements: []
	
}) -> {
	Phone: acc.Phone + dataElm when (commonLib.isArrayNotEmpty(dataElm.otherDataFreetext) and dataElm.elementManagementData.segmentName == "AP" 
				and (dataElm.otherDataFreetext[0].freetextDetail.type == "3" or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "4"  or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "5" or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "6" or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "7" or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "N" or 
					dataElm.otherDataFreetext[0].freetextDetail.type == "P01") 
				and commonLib.isNotEmpty(dataElm.otherDataFreetext[0].longFreetext) and not (dataElm.otherDataFreetext[0].longFreetext startsWith "AC/E+")
				) otherwise acc.Phone,

	Email: acc.Email + dataElm when (commonLib.isArrayNotEmpty(dataElm.otherDataFreetext) and dataElm.elementManagementData.segmentName == "AP" 
						and (dataElm.otherDataFreetext[0].freetextDetail.type == "P02" or 
							(dataElm.otherDataFreetext[0].freetextDetail.type == "5" and commonLib.isNotEmpty(dataElm.otherDataFreetext[0].longFreetext) and 
								(dataElm.otherDataFreetext[0].longFreetext startsWith "AC/E+"))
							)
					) otherwise acc.Email,
				
	SK: acc.SK + dataElm when (dataElm.elementManagementData.segmentName == "SK") otherwise acc.SK,
	
	OS: acc.OS + dataElm when (dataElm.elementManagementData.segmentName == "OS") otherwise acc.OS,
	
	SpecialRemarks: acc.SpecialRemarks + dataElm when (
											dataElm.elementManagementData.segmentName == "RC" or 
											dataElm.elementManagementData.segmentName == "RI" or
											dataElm.elementManagementData.segmentName == "RIF" or
											dataElm.elementManagementData.segmentName == "RX" or
											dataElm.elementManagementData.segmentName == "RQ"
										) otherwise acc.SpecialRemarks,
	
	MiscRemarks: acc.MiscRemarks + dataElm when (dataElm.elementManagementData.segmentName == "RM") otherwise acc.MiscRemarks,

	custLoyalty: acc.custLoyalty + dataElm when (dataElm.elementManagementData.segmentName == "SSR" and dataElm.serviceRequest.ssr.type == "FQTV") otherwise acc.custLoyalty,
		
	DOCS: acc.DOCS + dataElm when (dataElm.elementManagementData.segmentName == "SSR" 
									and dataElm.serviceRequest.ssr.type == 'DOCS'
								) otherwise acc.DOCS,
			
	DOCO: acc.DOCO + dataElm when (dataElm.elementManagementData.segmentName == "SSR" 
									and (dataElm.serviceRequest.ssr.type == "DOCO")
								) otherwise acc.DOCO,
			
	DOCA: acc.DOCA + dataElm when dataElm.serviceRequest.ssr.type == "DOCA" otherwise acc.DOCA,

	PCTC: acc.PCTC + dataElm when (dataElm.elementManagementData.segmentName == "SSR" and dataElm.serviceRequest.ssr.type == "PCTC") otherwise acc.PCTC,
	
	Seats: acc.Seats + dataElm when (dataElm.elementManagementData.segmentName == "SSR" and (dataElm.serviceRequest.ssr.type == "RQST"
										or dataElm.serviceRequest.ssr.type == "NSST")) otherwise acc.Seats,

	SSRs: acc.SSRs + dataElm when (dataElm.elementManagementData.segmentName == "SSR"
							and  not (nonSSRList contains dataElm.serviceRequest.ssr.type) 
			) otherwise acc.SSRs,
			
			
	StructuredAddress: acc.StructuredAddress + dataElm when (dataElm.elementManagementData.segmentName == "AB" or 
		dataElm.elementManagementData.segmentName == "AB/" or 
		dataElm.elementManagementData.segmentName == "AM" or 
		dataElm.elementManagementData.segmentName == "AM/") otherwise acc.StructuredAddress,
		
	
	FA: acc.FA + dataElm when (dataElm.elementManagementData.segmentName == "FA") otherwise acc.FA,
	
	OPC: acc.OPC + dataElm when (dataElm.elementManagementData.segmentName == "OPC") otherwise acc.OPC,
	
	FP: acc.FP + dataElm when (dataElm.elementManagementData.segmentName == "FP") otherwise acc.FP,
	
	FO: acc.FO + dataElm when (dataElm.elementManagementData.segmentName == "FO") otherwise acc.FO,
	
	Endorsements: acc.Endorsements + dataElm when (dataElm.elementManagementData.segmentName == "FE") otherwise acc.Endorsements,
	
	FTs: acc.FTs ++ dataElm.otherDataFreetext when (dataElm.elementManagementData.segmentName == "FT") otherwise acc.FTs
	
} when commonLib.isNotEmpty(pnrReply.dataElementsMaster) and commonLib.isArrayNotEmpty(pnrReply.dataElementsMaster.dataElementsIndiv) otherwise null

//This function extracts longFreeText from one of the objects inside dataElementsIndiv array for specific segment. dataElementsIndiv object is passed as dataElm parameter
%function getFreeTextTokens(dataElm)
	dataElm.otherDataFreetext[0].longFreetext splitBy "/" when dataElm.otherDataFreetext[0].longFreetext? otherwise []

//Extracts ticket number from longFreeText for FO segments
%function getTicketNumberFromFOSegment(foSegment)
using (freeTxtTokens=getFreeTextTokens(foSegment))
using (tktNumber=freeTxtTokens[-1] when commonLib.isArrayNotEmpty(freeTxtTokens) otherwise null)
	(tktNumber[0..13] when tktNumber != null otherwise null)
---
{
	createDataElmBuckets: createDataElmBuckets,
	getPaxDatas: getPaxDatas,
	getElementManagementReference: getElementManagementReference,
	getRefsByQualifier: getRefsByQualifier,
	getFreeTextTokens: getFreeTextTokens,
	getTicketNumberFromFOSegment: getTicketNumberFromFOSegment
}