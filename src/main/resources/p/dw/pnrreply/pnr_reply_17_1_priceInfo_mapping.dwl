%dw 1.0

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")		

%function PTCLookupOnPaxData(paxRefs, passengerData)
using (paxData=(flatten passengerData filter $.travellerRPH == (paxRefs[0].number as :number)) when passengerData != null otherwise null
)
{
	pax: paxData[0].travellerInformation.passenger[0] when commonLib.isArrayNotEmpty(paxData) and ((sizeOf paxData) == 1) 
			otherwise ((flatten paxData.travellerInformation.passenger) filter $.type != "INF")[0] when commonLib.isArrayNotEmpty(paxData) and ((sizeOf paxData) > 1) 
			otherwise null
}

%function getPTCValue(freeText, paxRefs, passengerData)
	freeText.longFreetext when freeText != null and freeText.longFreetext? and freeText.longFreetext == "INF"
	otherwise
		PTCLookupOnPaxData(paxRefs, passengerData).pax.type

//PriceInfo variable is created by parsing through the tstData
//tstData is per PassengerTypeCode. Total Base Fare and Total Fare needs to be calculated for itinTotalFare. 
//To improve performance, a generic structure is created to get aggregate the totals and also, pull the base and total fare only one time per tstData
%function createPriceInfo(pnrReply, passengerData)
(pnrReply.tstData reduce (obj, acc={
	itinTotalFareBase: {
		amount: 0,
		currencyCode: ""
	},
	itinTotalFareEquiv: {
		amount: 0,
		currencyCode: ""
	},	
	itinTotalFareTotal: {
		amount: 0,
		currencyCode: ""
	},
	PTCs: []
}) -> using (
	paxRefs=obj.referenceForTstData.reference filter $.qualifier == "PT" when commonLib.isArrayNotEmpty(obj.referenceForTstData.reference) otherwise null,
	baseFare=(obj.fareData.monetaryInfo filter ($.qualifier == "F" or $.qualifier == "B"))[0],
	totalFare=(obj.fareData.monetaryInfo filter ($.qualifier == "T"))[0],
	equivFare=(obj.fareData.monetaryInfo filter ($.qualifier == "E"))[0],
	paxFreeText=obj.tstFreetext filter $.freetextDetail.subjectQualifier == "3" and $.freetextDetail.type == "41"
					when obj.tstFreetext.freetextDetail? otherwise null,
	feesFreeText=obj.tstFreetext filter $.freetextDetail.subjectQualifier == "3" and $.freetextDetail.type == "37"
					when obj.tstFreetext.freetextDetail? otherwise null
)
using (paxCount=(sizeOf (paxRefs)) when paxRefs != null otherwise 0,
	feesTexts=(feesFreeText..longFreetext filter ($ contains /(.*)(Q\d*\.\d*)+(.*)/)) when commonLib.isArrayNotEmpty(feesFreeText) otherwise null
)
{
	itinTotalFareBase: {
		amount: acc.itinTotalFareBase.amount + (baseFare.amount*paxCount) when baseFare.amount? and paxCount > 0 otherwise acc.itinTotalFareBase.amount,
		currencyCode: baseFare.currencyCode when baseFare.currencyCode? otherwise acc.itinTotalFareBase.currencyCode
	},
	//EPM-10919
	itinTotalFareEquiv: {
		amount: acc.itinTotalFareEquiv.amount + (equivFare.amount*paxCount) when equivFare.amount? and paxCount > 0 otherwise acc.itinTotalFareEquiv.amount,
		currencyCode: equivFare.currencyCode when equivFare.currencyCode? otherwise acc.itinTotalFareEquiv.currencyCode
	},	
	
	itinTotalFareTotal: {
		amount: acc.itinTotalFareTotal.amount + (totalFare.amount*paxCount) when totalFare.amount? and paxCount > 0 otherwise acc.itinTotalFareTotal.amount,
		currencyCode: totalFare.currencyCode when totalFare.currencyCode? otherwise acc.itinTotalFareTotal.currencyCode
	},
	
	PTCs: acc.PTCs + ({
		base: baseFare when baseFare.amount? otherwise null,
		//EPM-10919
		equiv: equivFare when equivFare.amount? otherwise null,
		total: totalFare when totalFare.amount? otherwise null,
		taxes: obj.fareData.taxFields,
		fareBasis: obj.fareBasisInfo,
		QFees: (feesTexts map using (allQs=($ scan /\s((Q\d+[\.]\d*)+)\s/))((allQs default [] map {
			fees: ($[1] splitBy "Q")[1..-1]
		}) when allQs != null otherwise [])) when commonLib.isArrayNotEmpty(feesTexts) otherwise null,
		ptcValue: getPTCValue(paxFreeText[0], paxRefs, passengerData),
		paxCount: paxCount,
		paxReferences: paxRefs map {
			RPH: $.number
		} when paxRefs != null otherwise null,
		paxRefs: paxRefs
	})
}) when commonLib.isArrayNotEmpty(pnrReply.tstData) otherwise null

%function ptcTaxes(taxFields) 
	taxFields reduce (taxField, acc={
			totalTaxes: {
				amount: 0,
				currencyCode: ""
			},
			taxes: []
		}) -> {
			totalTaxes: {
				amount: acc.totalTaxes.amount + (taxField.taxAmount) when taxField.taxAmount? otherwise acc.totalTaxes.amount,
				currencyCode: taxField.taxCurrency when taxField.taxCurrency? otherwise acc.totalTaxes.currencyCode
			},
			taxes: acc.taxes + ({
				CurrencyCode: taxField.taxCurrency,
				DecimalPlaces: commonLib.getDecimalPlaces(taxField.taxAmount),
				Amount: taxField.taxAmount as :number,
				TaxCode: taxField.taxCountryCode,
				TaxCountry: taxField.taxNatureCode
			})
	} when commonLib.isArrayNotEmpty(taxFields) otherwise null
	
%function ptcFees(fees)
	fees reduce (fee, acc={
		totalFees: 0,
		fees: []		
	}) -> {
		totalFees:  acc.totalFees + fee when fee != null otherwise acc.totalFees,
		fees: acc.fees + ({
			DecimalPlaces: commonLib.getDecimalPlaces(fee),
			Amount: fee as :number,
			FeeCode: "Q" // TBD: To be confirmed with TBA
		})		
	} when commonLib.isArrayNotEmpty(fees) otherwise null	

%function getEndorsementElm(dataElmByType, paxRefs)
using (paxRefNumbers=paxRefs.number when commonLib.isArrayNotEmpty(paxRefs) otherwise [])
(dataElmByType.Endorsements filter (pnrReplycommonLib.getRefsByQualifier($, "PT", "DataElm") == paxRefNumbers))

%function mapAmount(fareStructure)
{
	Amount: fareStructure.amount as :number,
	CurrencyCode: fareStructure.currencyCode,
	DecimalPlaces: commonLib.getDecimalPlaces(fareStructure.amount)
}

%function mapPriceInfoWrapper(priceInfo, dataElmByType)
({		
	ItinTotalFare: [{
		BaseFare: mapAmount(priceInfo.itinTotalFareBase) 
					when priceInfo.itinTotalFareBase.amount > -1 and priceInfo.itinTotalFareBase.currencyCode != "" otherwise null,
		//EPM-10919
		EquivFare: [mapAmount(priceInfo.itinTotalFareEquiv)] 
					when priceInfo.itinTotalFareEquiv.amount > -1 and priceInfo.itinTotalFareEquiv.currencyCode != "" otherwise null,	
		
		TotalFare: mapAmount(priceInfo.itinTotalFareTotal) 
					when priceInfo.itinTotalFareTotal.amount > -1 and priceInfo.itinTotalFareTotal.currencyCode != "" otherwise null,
		
		TourCode: (dataElmByType.FTs[0].longFreetext replace /^(PAX )([\*])?(.+)$/ with $[3]) 
						when (dataElmByType.FTs != null and dataElmByType.FTs[0].longFreetext != null) otherwise null
		
	}] when priceInfo.itinTotalFareBase.currency != "" or priceInfo.itinTotalFareTotal.currencyCode != "" otherwise null,
	
	PTC_FareBreakdowns: priceInfo.PTCs map using (
		ptcTaxData = ptcTaxes($.taxes),
		ptcFeesInfo=(flatten $.QFees) reduce (obj, acc={fees:[]})-> {
			(fees: acc.fees ++ obj.fees) when obj.fees != null
		} when $.QFees != null otherwise null,
		endorsementElm = getEndorsementElm(dataElmByType, $.paxRefs) when commonLib.isArrayNotEmpty(dataElmByType.Endorsements) otherwise null
	) using (ptcFeesData=ptcFees(ptcFeesInfo.fees) when ptcFeesInfo != null otherwise null)
	{
		PassengerTypeQuantity: {
			Code: $.ptcValue, // Need a lookup with PT reference. Also need to deal with Infant (TBA question) 
			Quantity: $.paxCount
		},
		
		PassengerFare: [{
			(BaseFare: mapAmount($.base)) when commonLib.isNotEmpty($.base.amount),
			
			//EPM-10919
			(EquivFare: [mapAmount($.equiv)]) when commonLib.isNotEmpty($.equiv.amount),
			
			(TotalFare: mapAmount($.total)) when commonLib.isNotEmpty($.total.amount),
			
			Taxes: {
				Amount: ptcTaxData.totalTaxes.amount as :number,
				Tax: ptcTaxData.taxes
			} when ptcTaxData != null otherwise null,
			
			Fees: { 
				Amount: ptcFeesData.totalFees as :number,
				DecimalPlaces: commonLib.getDecimalPlaces(ptcFeesData.totalFees),
				Fee: ptcFeesData.fees						
			} when ptcFeesData != null otherwise null
			
		}] when $.base.amount? or $.total.amount? otherwise null, // PassengerFare
		
		TravelerRefNumber: $.paxReferences,

		Endorsements: {
			Endorsement: endorsementElm.otherDataFreetext default [] map {
				Content: $.longFreetext[0]
			}
		} when commonLib.isArrayNotEmpty(endorsementElm) otherwise null
		
	} when commonLib.isArrayNotEmpty(priceInfo.PTCs) otherwise null // PTC_FareBreakdowns
}) when priceInfo != null otherwise null

%function mapPriceInfo(pnrReply, dataElmByType, passengerData)
using (priceInfo = createPriceInfo(pnrReply, passengerData))
(mapPriceInfoWrapper(priceInfo, dataElmByType))

---
{
	mapPriceInfo: mapPriceInfo
}