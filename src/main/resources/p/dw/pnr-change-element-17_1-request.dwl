%dw 1.0
%output application/json skipNullOn="everywhere"
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")	
%var endorsement= (payload.endorsement.Endorsements.Endorsement.*Content joinBy "//"
						when commonLib.isArrayNotEmpty(payload.endorsement.Endorsements.Endorsement) and payload.endorsement.Endorsements.Endorsement? otherwise null)
%var splitByPAX=commonLib.extractWord(payload.longFreeText,1,"PAX")
%var longFreeTextWithoutPAX=trim splitByPAX when  splitByPAX!=null otherwise payload.longFreeText

---
PNR_ChangeElement:{
	messageAction:{
		messageFunctionDetails:{
  			messageFunction:"141"
  			}
		
	},
	longTextString:{
		textStringDetails: payload.lineNumber ++ "/" ++ longFreeTextWithoutPAX ++  ( "//"  ++ endorsement when commonLib.isNotEmpty(endorsement) otherwise "")
	   }
}