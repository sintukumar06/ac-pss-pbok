%dw 1.0
%output application/json
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var pnrReplycommonLib = readUrl("classpath://p/dw/pnrreply/pnr_reply_17_1_common_functions.dwl")

%var isAncExists = flowVars.otaAirBookChargeableAncillaries != null
%var isManual = isAncExists and flowVars.otaAirBookChargeableAncillaries.manualPricingAncillaries != null and (sizeOf flowVars.otaAirBookChargeableAncillaries.manualPricingAncillaries) > 0 
%var isAuto = isAncExists and flowVars.otaAirBookChargeableAncillaries.automatedPricingAncillaries != null and (sizeOf flowVars.otaAirBookChargeableAncillaries.automatedPricingAncillaries) > 0

%var otaAutomatedPricedAncilliaries=flowVars.otaAirBookChargeableAncillaries.automatedPricingAncillaries when isAuto otherwise null
%var otaManuallyPricedAncilliaries=flowVars.otaAirBookChargeableAncillaries.manualPricingAncillaries when isManual otherwise null

%var pnrReply=flowVars.pnrAddMultiElements17_1SaveResponse when flowVars.pnrAddMultiElements17_1SaveResponse != null and (isManual or isAuto) otherwise null
%var dataElementsPNRReply= pnrReply.PNR_Reply.dataElementsMaster.dataElementsIndiv when (pnrReply != null and commonLib.isArrayNotEmpty(pnrReply.PNR_Reply.dataElementsMaster.dataElementsIndiv)) otherwise null
%var pnrReplyBySSRCode= dataElementsPNRReply groupBy ($.serviceRequest.ssr.type when commonLib.isNotEmpty($.serviceRequest) and $.serviceRequest.ssr.type? otherwise "") when dataElementsPNRReply != null otherwise null

%var pnrCreateAuxSegReply=flowVars.pnrCreateAuxiliarySegment09_1Response when flowVars.pnrCreateAuxiliarySegment09_1Response != null and (isManual or isAuto) otherwise null
%var manualAuxiliaryDetails= pnrCreateAuxSegReply.PNR_CreateAuxiliarySegmentReply.manualAuxiliaryDetails when (pnrCreateAuxSegReply != null and commonLib.isArrayNotEmpty(pnrCreateAuxSegReply.PNR_CreateAuxiliarySegmentReply.manualAuxiliaryDetails)) otherwise null
%var pnrCreateAuxSegReplyBySSRCode= manualAuxiliaryDetails groupBy ($.serviceDetails.specialRequirementsInfo.ssrCode when $.serviceDetails.specialRequirementsInfo.ssrCode? otherwise "") when manualAuxiliaryDetails != null otherwise null

%var paxRPHTattooMap = flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap when flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.paxRPHTattooMap != null otherwise null
%var fltRPHTattooMap = flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap when flowVars.otaRPH2AlteaTattoo != null and flowVars.otaRPH2AlteaTattoo.fltRPHTattooMap != null otherwise null

//SSRs -- Not all SSRs have flightRefs
%function MatchACDiscountsWithPNRReply(acDiscountElement)
	(pnrReplyBySSRCode[acDiscountElement.Discount_Name] map (pnrSSR)-> 
		using (
			fltRefs=pnrReplycommonLib.getRefsByQualifier(pnrSSR, "ST", "DataElm"),
			paxRefs=pnrReplycommonLib.getRefsByQualifier(pnrSSR, "PT", "DataElm"),
			travelerTatoo=paxRPHTattooMap[acDiscountElement.TravelerRPH] when paxRPHTattooMap != null 
													and commonLib.isNotEmpty(acDiscountElement.TravelerRPH) and commonLib.isNotEmpty(paxRPHTattooMap[acDiscountElement.TravelerRPH]) otherwise null,
			fltTatoo=fltRPHTattooMap[acDiscountElement.FlightSegmentReferences[0].FlightSegmentReference.RPH] when fltRPHTattooMap != null 
							and commonLib.isNotEmpty(acDiscountElement.FlightSegmentReferences[0].FlightSegmentReference.RPH)
							and commonLib.isNotEmpty(fltRPHTattooMap[acDiscountElement.FlightSegmentReferences[0].FlightSegmentReference.RPH]) otherwise null
			){			
				("SSRElement": {
					"SegmentName": pnrSSR.elementManagementData.segmentName,
					"SegmentTattoo": pnrSSR.elementManagementData.reference.number as :string,
					"CompanyShortName": pnrSSR.serviceRequest.ssr.companyId
				}) when (
							(acDiscountElement.Discount_Name == pnrSSR.serviceRequest.ssr.type)
							and commonLib.isArrayNotEmpty(paxRefs) and travelerTatoo != null and (paxRefs contains travelerTatoo as :string)
							and (
								(commonLib.isArrayEmpty(fltRefs)) or fltTatoo == null or (commonLib.isArrayNotEmpty(fltRefs) and fltTatoo != null and (fltRefs contains fltTatoo as :string))
							)
						)
			}) when commonLib.isArrayNotEmpty(pnrReplyBySSRCode[acDiscountElement.Discount_Name]) otherwise []

//SVCs
%function MatchACDiscountsWithPNRAuxReply(acDiscountElement)
	(pnrCreateAuxSegReplyBySSRCode[acDiscountElement.Discount_Name] map (auxSSR)->
		using (
			paxRefs=auxSSR.passengerAssociation.passengerReference.value,
			travelerTatoo=paxRPHTattooMap[acDiscountElement.TravelerRPH] when paxRPHTattooMap != null 
											and commonLib.isNotEmpty(paxRPHTattooMap[acDiscountElement.TravelerRPH]) otherwise null
			){	
				("SSRElement": {
					"SegmentName": "SVC",
					"SegmentTattoo": auxSSR.elementManagementSegment.reference.number,
					"CompanyShortName": auxSSR.travelProduct.companyDetails.marketingCompany
				}) when (
							(acDiscountElement.Discount_Name == auxSSR.serviceDetails.specialRequirementsInfo.ssrCode)
							and commonLib.isArrayNotEmpty(paxRefs) and travelerTatoo != null and (paxRefs contains (travelerTatoo as :string))
							and (
									commonLib.isEmpty(acDiscountElement.DiscountLocation.Content) 
									or (acDiscountElement.DiscountLocation.Content == auxSSR.travelProduct.boardPointDetails.trueLocationId as :string)
								)
						) 
			}) when pnrCreateAuxSegReplyBySSRCode[acDiscountElement.Discount_Name] != null otherwise []

---
{
	"manualPricingAncillaries": (otaManuallyPricedAncilliaries map (acDiscount) -> 
																		using(
																			matchACDiscountsPNRReply=MatchACDiscountsWithPNRReply(acDiscount) filter $ != {},
																			matchACDiscountsAuxReply=MatchACDiscountsWithPNRAuxReply(acDiscount) filter $ != {}
																		) {
																		(
																			acDiscount + matchACDiscountsPNRReply when matchACDiscountsPNRReply != []
        																			otherwise acDiscount + matchACDiscountsAuxReply when matchACDiscountsAuxReply != [] otherwise []
        																	)
																	} filter $ != {} ) when commonLib.isArrayNotEmpty(otaManuallyPricedAncilliaries) otherwise null,
																
	"automatedPricingAncillaries": (otaAutomatedPricedAncilliaries map (acDiscount) -> 
																		using(
																			matchACDiscountsPNRReply=MatchACDiscountsWithPNRReply(acDiscount) filter $ != {},
																			matchACDiscountsAuxReply=MatchACDiscountsWithPNRAuxReply(acDiscount) filter $ != {}
																		) {
																		(
																			acDiscount + matchACDiscountsPNRReply when matchACDiscountsPNRReply != []
        																			otherwise acDiscount + matchACDiscountsAuxReply when matchACDiscountsAuxReply != [] otherwise []
        																	)
																	} filter $ != {}) when commonLib.isArrayNotEmpty(otaAutomatedPricedAncilliaries) otherwise null
}