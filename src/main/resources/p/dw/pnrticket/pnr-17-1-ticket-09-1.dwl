%dw 1.0
%output application/json skipNullOn="everything"
---
//Requires refactoring for stories of epic EPM-11186
{
"TicketDocumentDetails":
[{
	TicketNumber: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].TicketNumber,
	AirReservation: {
		
		AirItinerary: flowVars.otaPnrRetrieve17_1Response.AirReservation.AirItinerary[0],//Need correlation
		
		TravelerInfo: flowVars.otaPnrRetrieve17_1Response.AirReservation.TravelerInfo[0],//Need correlation
		
		Ticketing: [{
			//TicketType: flowVars.otaPnrRetrieve17_1Response.AirReservation[0].Ticketing[0].TicketType,//Need correlation
			IATANumber: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].AirReservation.Ticketing.IATANumber,
			PseudoCityCode: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].AirReservation.Ticketing.PseudoCityCode
		}],
		
		BookingReferenceID: [{
			ID: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].AirReservation.BookingReferenceID[0].ID, //Need correlation
			CompanyName: {
				Code: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].AirReservation.BookingReferenceID[0].CompanyName.Code,
				CodeContext: flowVars.otaPnrRetrieve17_1Response.AirReservation.BookingReferenceID[0].CompanyName.CodeContext[0]
				//Need correlation
			}
		}],
		
		CreateDateTime: flowVars.otaPnrRetrieve17_1Response.AirReservation.CreateDateTime[0],
		//Combination of #50 and #51 (PNR_Reply/securityInformation/secondRpInformation/creation) To be validated
		
		EMD_Info: flowVars.otaTicketProcessEDocCCConcealed9_1Response.TicketDocumentDetails[0].AirReservation.EMD_Info
	}
}]
}