%dw 1.0
%output application/json
%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")
%var otaAirBookRQ = flowVars.originalOtaAirbookRequest
%var acDiscounts = otaAirBookRQ.PriceInfo.ItinTotalFare[0].AC_Discounts default [] 

%var acDiscountsPerFlight = (flatten (acDiscounts map ((acDiscount) ->	
	(acDiscount.FlightSegmentReferences map 
		(acDiscount - "FlightSegmentReferences" ++ {
			FlightSegmentReferences: [
				($)
			]
		})
	) when commonLib.isArrayNotEmpty(acDiscount.FlightSegmentReferences) otherwise acDiscount)
)) when commonLib.isArrayNotEmpty(acDiscounts) otherwise null

%var ancillaries = flatten otaAirBookRQ.TravelerInfo.SpecialReqDetails.SpecialServiceRequests 
				when commonLib.isArrayNotEmpty(otaAirBookRQ.TravelerInfo.SpecialReqDetails) 
					and commonLib.isArrayNotEmpty(otaAirBookRQ.TravelerInfo.SpecialReqDetails.SpecialServiceRequests) otherwise null
%var ancillariesByNumber = (ancillaries filter ($.Number != null)) groupBy $.Number when (ancillaries != null) otherwise ""

%var seats = flatten otaAirBookRQ.TravelerInfo.SpecialReqDetails.SeatRequests 
				when commonLib.isArrayNotEmpty(otaAirBookRQ.TravelerInfo.SpecialReqDetails) 
					and commonLib.isArrayNotEmpty(otaAirBookRQ.TravelerInfo.SpecialReqDetails.SeatRequests) otherwise null
					
%var seatsByID = (seats filter ($.Offers[0].ID != null)) groupBy $.Offers[0].ID when (seats != null) otherwise ""

---
//Manual priced ancillaries
//Extract all AC_Discounts containing non empty DiscountFare element
//Automated Priced Ancillaries
//Extract all  AC_Discounts containing empty DiscountFare element
//An additional validation to make sure these ancillaries exists in the SpecialServiceRequests
// SSRCode matches DiscountName and TravelerRefNumberRPHList contains TravelerRPH

acDiscountsPerFlight reduce (obj, acc={
	manualPricingAncillaries: [],
	automatedPricingAncillaries: []
}) -> using (ancillary = (ancillariesByNumber[obj.ID] filter ($.Number contains obj.ID))
								when ancillariesByNumber[obj.ID] != null 
								otherwise (seatsByID[obj.ID] filter ($.Offers[0].ID contains obj.ID))
											when seatsByID[obj.ID] != null otherwise null
) {
	manualPricingAncillaries: acc.manualPricingAncillaries + obj
					when (commonLib.isNotEmpty(obj.DiscountFare) and commonLib.isArrayNotEmpty(ancillary)) otherwise acc.manualPricingAncillaries,
	automatedPricingAncillaries: acc.automatedPricingAncillaries + obj 
					when ((not (commonLib.isNotEmpty(obj.DiscountFare))) and commonLib.isArrayNotEmpty(ancillary)) otherwise acc.automatedPricingAncillaries
} when commonLib.isNotEmpty(otaAirBookRQ.PriceInfo.ItinTotalFare[0]) 
	and commonLib.isArrayNotEmpty(acDiscountsPerFlight) 
	and (commonLib.isNotEmpty(ancillaries) or commonLib.isNotEmpty(seats))
	and (commonLib.isNotEmpty(ancillariesByNumber) or commonLib.isNotEmpty(seatsByID))
otherwise null