%dw 1.0
%output application/json skipNullOn="everything"

%var commonLib = readUrl("classpath://p/dw/common-functions.dwl")

%function isETACSegment(element)
	element.elementManagementData.segmentName == "FA" and (commonLib.extractWord(element.otherDataFreetext[0].longFreetext,1,"/") == "ETAC")
%var dataElements = flowVars.pnrRetrieve17_1Response.PNR_Reply.dataElementsMaster.dataElementsIndiv
%var ticketElements = dataElements filter isETACSegment($)

---
{
	Ticket_RepricePNRWithBookingClass: {
		exchangeInformationGroup: ticketElements map {
			transactionIdentifier : {
				itemNumberDetails : [{	
					number : ($$+1) as :string								
				}]
			}, 
			documentInfoGroup: [{
				paperticketDetailsLastCoupon: {
					documentDetails: {
						number:commonLib.extractAllNumbers(commonLib.extractWord($.otherDataFreetext[0].longFreetext,0,"/")),
						type: "ET"
					}
				}
			}]
		},
		pricingOption: ticketElements map {
			pricingOptionKey:{
				pricingOptionKey: "SEL"
			},							
			paxSegTstReference :{
				referenceDetails:[{
					type: "P",
					value: ($.referenceForDataElement.reference filter $.qualifier=="PT")[0].number when ($.referenceForDataElement.reference filter $.qualifier=="PT") != [] otherwise null
				}, 
				{
					type: "E",
					value: ($$+1) as :string	
				}]
			}
		}		  		 
		
	}	
	
}	