%dw 1.0
%output application/java
%var prefix = flowVars.errorPrefix
%var errors = flowVars.EIPContext.errors
%function addPrefix(errors)
(errors map ($ ++ prefix: prefix)) when prefix != null otherwise
errors 
---
flowVars.flowErrors when flowVars.flowErrors? and (errors == null or errors == [])
otherwise (flowVars.flowErrors ++ addPrefix(errors)) when flowVars.flowErrors? and errors != null and errors !=[]
otherwise addPrefix(errors) when errors != null and errors !=[]
otherwise []